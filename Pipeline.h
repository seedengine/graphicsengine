#include <vulkan\vulkan.h>

#include <map>

#include "CoreEngine\StaticCoreEngine.h"
#include "UtilStruct.h"
#include "CoreEngine/IHandle.h"
#include "PipelineLayout.h"
#include "RenderPass.h"
#include "Memory.h"
#include "IPipeline.h"
#include "ShaderModule.h"

class Buffer;
class Device;
class SwapChainImage;

#pragma once
class Pipeline : 
	public IPipeline,
	public EventHandler,
	public IHandle<VkPipeline>
{
public:
	Pipeline(ComponentInterface*);
	Pipeline(ComponentInterface*, std::string name);

	void construct(Device& dev, VkPipelineCache& cache, VkGraphicsPipelineCreateInfo pipeline, PipelineLayout&&);
	void construct(Device& dev, IRenderSystem*, uint32_t renderStepID, Surface*, PipelineLayout&&, std::function<void(PipelineFactory&)> = std::function<void(PipelineFactory&)>([](PipelineFactory&) {}));

	const VkPipeline& getHandle() const;
	const PipelineLayout& getLayout() { return layout; }

	bool hasSet(uint32_t id) { return layout.hasSet(id); }

	void bind(IRenderContext*);

	void operator()(ComponentInterface*, const EventBase*);


	~Pipeline();
private:
	void load(ComponentInterface*);
	VkShaderStageFlagBits getTypeFromName(std::string);
	std::string file;
	Compressor::PipelineData data;

	VkPipeline pipeline;

	std::map<std::string, ShaderModule*> modules;

	PipelineLayout layout;
};

#include <vulkan/vulkan.h>
#include <vector>
#include <functional>

#include "UtilStruct.h"
#include "CoreEngine/IHandle.h"
#include "Image.h"
#include "CoreEngine/ISwappable.h"
#include "RenderPass.h"

class Surface;
class Semaphore;
class Device;

#pragma once
class SwapChain : public IHandle<VkSwapchainKHR>, public ISwappable<Image>, public FramebufferAttachmentProvider
{
	friend class SwapChainImage;
public:
	SwapChain();

	void construct(Device& dev, Surface& window);
	void rebuildSwapChain(Device& dev, Surface& window);

	virtual Attachment getFramebufferAttachment(uint32_t framebufferCnt) const;

	const VkSwapchainKHR& getHandle() const;

	uint32_t getNextImage(Device& dev, Semaphore& semaphore);

	void destroy(Device&);

	~SwapChain();
private:
	VkExtent2D swapChainExtent;
	VkFormat swapChainFormat;
	VkPresentModeKHR presentationMode;
	VkSwapchainKHR swapChain;
};

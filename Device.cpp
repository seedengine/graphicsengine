#include "Device.h"
#include "SwapChain.h"
#include "GraphicsExceptions.h"

#include "UtilStruct.h"
#include <algorithm>
#include "CoreEngine\StaticCoreEngine.h"
#include "CoreEngine/Event.hpp"
#include "GraphicsEvents.h"
#include "Instance.h"
#include "VulkanGraphicsEvents.h"
#include "GraphicsEngine.h"

const VkPipelineStageFlags Device::waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };

Device::Device(ComponentInterface* core) : 
	dataHandler( 1024),
	waitSemaphore(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT),
	frameDoneSemaphore(VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT),
	descriptorManager(core)
{
}

void Device::construct(ComponentInterface* core, PhysicalDevice* dev, VkDeviceCreateInfo & info, int presentationQueueIndex, int computeQueueIndex, int transferQueueIndex)
{
	physical = dev;
	//Tries top build Device according to specifications. Throws Failure on Exception
	if (vkCreateDevice(physical->getHandle(), &info, VK_NULL_HANDLE, &device) != VK_SUCCESS)
		throw VulkanConstructionError("failed to create logical device!");

	//Builds logical queues from provided queue family indices.
	graphicsQueue.construct(*this, presentationQueueIndex);
	computeQueue.construct(*this, computeQueueIndex);
	transferQueue.construct(*this, transferQueueIndex);

	waitSemaphore.construct(*this);
	frameDoneSemaphore.construct(*this);

	((BufferMemoryInterface*)dataHandler.getInterface())->constructInterface(core, VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);


	GraphicsMemoryReadyEvent deviceEvt;
	deviceEvt.setDeviceHandle(this);
	core->getEventBus()->dispatchEvent(core, deviceEvt, { VulkanDeviceConstructionEvent::getClassID() });
}

void Device::present(GraphicsEngine* engine)
{
	uint32_t idx = engine->getSwapChain().getNextImage(*this, waitSemaphore);

	transferQueue.flush(nullptr, nullptr);
	engine->submitBuffer();
	graphicsQueue.flush(&waitSemaphore, &frameDoneSemaphore);
	computeQueue.flush(nullptr, nullptr);

	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

	//Specifies Semaphores to wait on
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = &frameDoneSemaphore.getHandle();
	//Specified Swapchains to present
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = &engine->getSwapChain().getHandle();
	//Specifies Image Indices for each swap chain to present
	presentInfo.pImageIndices = &idx;
	
	//execute presentation
	vkQueuePresentKHR(graphicsQueue.getQueueHandle(), &presentInfo);
}

void Device::waitUntilIdle()
{
	//makes thread wait for the logical Device
	vkDeviceWaitIdle(device);
}

void Device::destroy()
{
	((BufferMemoryInterface*)dataHandler.getInterface())->destroyInterface(*this);

	graphicsQueue.flush(nullptr, nullptr);
	graphicsQueue.destroy(*this);
	computeQueue.destroy(*this);
	transferQueue.destroy(*this);

	waitSemaphore.destroy(*this);
	frameDoneSemaphore.destroy(*this);

	vkDestroyDevice(device, VK_NULL_HANDLE);
	device = VK_NULL_HANDLE;
}

Device::~Device()
{
}

#include "Image.h"
#include "RenderContext.h"

class Attachment;

#pragma once
class DepthImage :
	public IRenderComponent,
	public EventHandler
{
	class DepthImageObject : public Image
	{
	public:
		void construct(Device& dev, uint32_t width, uint32_t height, VkFormat format) override { construct(dev, width, height); } //force ignore format
		void construct(Device& dev, uint32_t width, uint32_t height); //force ignore format
		void init(Device&) override;
		void destroy(Device&) override;

		void reset(ComponentInterface* core);
		void reset(VkCommandBuffer& record);
	protected:
		void createImage(Device &dev) override;
		void createImageView(Device &dev) override;
	private:
		VkFormat getFormatFromDevice(const PhysicalDevice&, const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features);
		VkCommandBuffer buf;
	};
public:
	DepthImage(ComponentInterface* core, uint32_t width, uint32_t height);
	~DepthImage();

	virtual void operator()(ComponentInterface* core, const EventBase*);

	uint32_t getImageReference() const { return reference; }

	const DepthImageObject& getImageObject() const { return img; }

	inline void reset();
	void reset(CommandBufferRecord&);

	virtual void* getComponentData();
private:
	ComponentInterface* core;
	DepthImageObject img;
	Attachment* data;
	uint32_t reference;

	uint32_t width;
	uint32_t height;

};


extern "C" __declspec(dllexport) IRenderComponent* __cdecl createDepthImage(ComponentInterface* core);
extern "C" __declspec(dllexport) void __cdecl destroyDepthImage(IRenderComponent*, ComponentInterface* core);

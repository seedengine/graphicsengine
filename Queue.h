#include <vulkan\vulkan.h>
#include <vector>
#include <mutex>
#include "Semaphore.h"

class Device;

#pragma once
class Queue
{
public:
	Queue();

	Queue(const Queue&);

	void construct(Device& dev, uint32_t queueFamilyIndex);

	VkQueue& getQueueHandle();
	VkCommandPool& getCommandPoolHandle();
	uint32_t getFamilyID();

	void submitCommandBuffer(const VkCommandBuffer&);
	void flush(Semaphore* waitSemaphore, Semaphore* signalSemaphore);
	void wait();

	void destroy(Device & dev);

	~Queue();
private:
	VkQueue queue;
	uint32_t queueFamilyIndex;
	VkCommandPool pool;
	std::mutex queueLock;

	std::vector<VkCommandBuffer> buffers;
};


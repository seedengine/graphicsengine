#include <vector>

#include "CoreEngine\IEngine.h"
#include "CoreEngine\IObjectComponent.h"
#include "CoreEngine\Event.h"
#include "Instance.h"
#include "Surface.h"
#include "Device.h"
#include "RenderPass.h"
#include "Semaphore.h"
#include "SwapChain.h"
#include "GraphicsEvents.h"
#include "VulkanGraphicsEvents.h"
#include "RenderContext.h"
#include "GraphicsModuleAction.h"
#include "LightRenderer.h"
#include "DepthImage.h"

class BaseLight;

//copy budgetting
//lazy swapping, only use dedicated memory whenever the swap memory is full -->gradually swap, either force clear the swap buffer or push a quarter of it per tick
//memory handler need to perperly handle arrays
//Implement Vulkan allocator

#pragma once
class GraphicsEngine : 
	public IEngineModule,
	public EventHandler
{
public:
	GraphicsEngine(ComponentInterface*);
	~GraphicsEngine();

	virtual void construct(ComponentInterface* component);
	virtual void destroy();

	virtual uint32_t getState();

	virtual void operator() (ComponentInterface*, const EventBase*);
	virtual void __cdecl doAction(uint32_t, void*);

	Instance& getInstance() { return instance; }
	Surface& getSurface() { return surface; }
	Device& getDevice() { return device; } //public but not available in interface. This requires the Type to access ==> package private
	SwapChain& getSwapChain() { return swapchain; }

	DepthImage* getDepthMapAttachment() { return &depthMap; }

	virtual const std::string getID() const { return GRAPHICS_MODULE_NAME; }

	void run();
	void submitBuffer();

private:
	void prepareCommandBuffers();
	void updateCommandBuffers();


	bool setupComplete;
	Task frameRender;

	uint32_t state;

	Version version;
	Instance instance;
	Device device;

	Surface surface;

	SwapChain swapchain;
	DepthImage depthMap;
	Attachment colorAttachment;
	IRenderSystem::ComponentRef colorRef;
	

	std::vector<CommandBuffer*> buffers;

	ComponentInterface* core;

	std::set<uint64_t> subEvents;

	//Make runtime dependant?
	//or make evaluation more constant and quicker
	static const char* instanceExtensions[];
	static const char* deviceExtensions[];
	static const char* validationLayers[];
	std::vector<const char*> getExtensionList(ComponentInterface * component);
};

extern "C" __declspec(dllexport) IEngineModule* __cdecl createModule(ComponentInterface* core, std::string);
extern "C" __declspec(dllexport) void __cdecl destroyModule(IEngineModule*, ComponentInterface* core, std::string);

extern "C" __declspec(dllexport) IObjectComponent* __cdecl createComponent(ComponentInterface* core, std::string, void*);
extern "C" __declspec(dllexport) void __cdecl destroyComponent(ComponentInterface* core, IObjectComponent*, std::string);

#include "DescriptorSetFactory.h"
#include "CoreEngine\OrderedList.hpp"
#include "CoreEngine\IEngine.h"
#include "GraphicsEvents.h"
#include "CoreEngine\Event.hpp"
#include "VulkanGraphicsEvents.h"
#include "GraphicsExceptions.h"
#include "Device.h"

DEFINE_EVENT(DescriptorPoolEvent)
public:
	void setDeviceHandle(Device* dev) { device = dev; }
	Device* getDeviceHandle() const { return device; }
private:
	Device* device;
};

DescriptorSetFactory::DescriptorSetFactory(ComponentInterface* core) : EventHandler(core), typeCounts(), uniformAlignment(0)
{
	core->getEventBus()->registerHandler(this, VulkanDeviceConstructionEvent::getClassID());
	core->getEventBus()->registerHandler(this, DescriptorPoolEvent::getClassID());
	core->getEventBus()->registerHandler(this, FinalizationPreparationEvent::getClassID());
}

void DescriptorSetFactory::operator()(ComponentInterface* core, const EventBase* evt)
{
	if (evt->is<VulkanDeviceConstructionEvent>())
	{
		VulkanDeviceConstructionEvent* data = (VulkanDeviceConstructionEvent*)evt;

		//Retreive minimum alignment
		uniformAlignment = data->getDeviceHandle()->getPhysicalDevice()->getDeviceLimits().minUniformBufferOffsetAlignment;


		DescriptorRegistrationEvent info0;
		info0.setDescriptorFactory(this);
		core->getEventBus()->dispatchEvent(core, info0);
		DescriptorPoolEvent info;
		info.setDeviceHandle(data->getDeviceHandle());
		core->getEventBus()->dispatchEvent(core, info, { DescriptorRegistrationEvent::getClassID() });
		DescriptorAllocationEvent info1;
		info1.setDescriptorFactory(this);
		core->getEventBus()->dispatchEvent(core, info1, { DescriptorPoolEvent::getClassID() });
	}
	else if (evt->is< FinalizationPreparationEvent>())
	{
		FinalizationPreparationEvent* data = (FinalizationPreparationEvent*)evt;
		data->addDependentEvent(DescriptorAllocationEvent::getClassID());
	}
	else if (evt->is<DescriptorPoolEvent>())
	{
		VulkanDeviceConstructionEvent* data = (VulkanDeviceConstructionEvent*)evt;
		VkDescriptorPoolCreateInfo poolInfo = {};

		for (auto set : sets)
			set->buildLayout(data->getDeviceHandle());

		uint32_t setCnt = sets.size() * 4; //should be dynamic in some way. But that would need some smarter registering

		std::vector<VkDescriptorPoolSize> sizes;

		for (auto p : typeCounts)
			sizes.push_back(VkDescriptorPoolSize{p.first, p.second});

		poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
		poolInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
		poolInfo.poolSizeCount = sizes.size();
		poolInfo.pPoolSizes = sizes.data();
		poolInfo.maxSets = setCnt;

		if (vkCreateDescriptorPool(*data->getDeviceHandle(), &poolInfo, VK_NULL_HANDLE, &pool) != VK_SUCCESS)
			throw VulkanConstructionError("failed to create logical device!");
	}

}

//bindings must be associated with set Object apparently
void DescriptorSetFactory::registerBinding(ComponentInterface* core, uint8_t setPosition, VkDescriptorType type, uint32_t binding, uint32_t size)
{
	DescriptorSet* set = getDescriptorSet(core, setPosition);

	set->registerBinding(type, binding, size);

	if (typeCounts.find(type) == typeCounts.end())
		typeCounts.insert(std::pair<VkDescriptorType, uint32_t>(type, 0));

	typeCounts[type] += 1;
}

DescriptorSet* DescriptorSetFactory::getDescriptorSet(ComponentInterface* core, uint32_t setPosition)
{
	if (sets.size() == 0)
	{
		sets.insert(new DescriptorSet(core, &pool, setPosition, uniformAlignment));
		return sets[0];
	}
	DescriptorSet* set = nullptr;
	try {
		set = sets.find((const uint32_t)setPosition);
	}
	catch (const FrameworkException&)
	{
		//not found
	}

	if (set == nullptr || set->getID() != setPosition)
		return sets[sets.insert(new DescriptorSet( core, &pool, setPosition, uniformAlignment))];
	else 
		return set;
}

void DescriptorSetFactory::update()
{
	for (auto set : sets)
		set->update();
}

#include "Camera.h"

#include "GraphicsEngine.h"


#include "CoreEngine\IEngine.h"
#include "GraphicsModuleAction.h"

#include <glm\gtc\matrix_transform.hpp>
#include "DescriptorSetFactory.h"
#include "DescriptorSet.h"
#include "CommandBuffer.h"
#include "Pipeline.h"



Camera::Camera(ComponentInterface* engine) :
	ICamera(engine),
	forward(0.0f, 0.0f, -1.0f),
	up(0.0f, 1.0f, 0.0f),
	right(1.0f, 0.0f, 0.0f),
	_pos(0.0f, 0.0f, 1.0f),
	lastX(0),
	lastY(0),
	initialized(false)
{
	XAngle = -90.0f;
	YAngle = 0;
	mvp.ProjMat = glm::perspective(45.0f, 1.3333f, 0.1f, 100.0f);
	mvp.Eye = _pos;
	updateVectorsFromRotation();
}

void Camera::translate(glm::vec3 vec)
{
	_pos += vec;

	mvp.TransMat = glm::lookAt(_pos, _pos + forward, up);
	mvp.Eye = _pos;
	if (initialized)
	{
		mvpDescriptor->set(mvpID, &mvp);
	}
}

void Camera::rotate(double x, double y)
{
	double dX = lastX - x;
	double dY = lastY - y;
	dX *= 20;
	dY *= 20;
	lastX = x;
	lastY = y;

	if (!lastX == 0 || !lastY == 0)
	{
		XAngle += dX;
		YAngle += dY;
	}
	
	updateVectorsFromRotation();
}

 void Camera::setPos(glm::vec3 newPos)
{
	 _pos = newPos;
	mvp.TransMat = glm::lookAt(_pos, _pos + forward, up);
	mvp.Eye = _pos;
	if (initialized)
	{
		mvpDescriptor->set(mvpID, &mvp, 0, sizeof(MVP));
	}
}

void Camera::setForward(glm::vec3 f)
{
	forward = f;
	mvp.Eye = _pos;
	updateVectorsFromRotation();
}

void Camera::operator()(ComponentInterface* core, const EventBase* evt)
{
	if (evt->is<RecordEvent>())
	{
		RecordEvent* handle = (RecordEvent*)evt;
		
		mvpDescriptor->bind((CommandBufferRecord*)handle->getRenderContext()->getContext(), mvpID);
	}
	else if (evt->is<DescriptorAllocationEvent>())
	{
		mvpID = mvpDescriptor->allocate(sizeof(MVP));
		mvpDescriptor->set(mvpID, &mvp);
		initialized = true;
	}
	else if (evt->is<DescriptorRegistrationEvent>())
	{
		DescriptorRegistrationEvent* handle = (DescriptorRegistrationEvent*)evt;

		handle->getDescriptorFactory()->registerBinding(core, 0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 0, sizeof(MVP));
		mvpDescriptor = handle->getDescriptorFactory()->getDescriptorSet(core, 0);
	}
}

Camera::~Camera()
{
}

inline void Camera::listRequirements(std::set<uint64_t>& req)
{
	req.insert(DescriptorRegistrationEvent::getClassID());
	req.insert(DescriptorAllocationEvent::getClassID());
	req.insert(RecordEvent::getClassID());
}

void Camera::updateVectorsFromRotation()
{
	if (YAngle > 89.0f)
		YAngle = 89.0f;
	if (YAngle < -89.0f)
		YAngle = -89.0f;

	static const glm::vec3 UP = glm::vec3(0.0f, 1.0f, 0.0f);

	forward = glm::vec3(
		cos(glm::radians(XAngle)) * cos(glm::radians(YAngle)),
		sin(glm::radians(YAngle)),
		sin(glm::radians(XAngle)) * cos(glm::radians(YAngle))
	);

	right = glm::normalize(glm::cross(forward, UP));
	up = glm::normalize(glm::cross(forward, right));

	mvp.TransMat = glm::lookAt(_pos, _pos + forward, up);
	if (initialized)
	{
		mvpDescriptor->set(mvpID, &mvp, 0, sizeof(MVP));
	}
}

#include "CoreEngine\Identifier.h"

#include "RenderContext.h"


#pragma once
class IPipeline : public Identifier
{
public:
	IPipeline() {}
	virtual void bind(IRenderContext*) = 0;

private:
};

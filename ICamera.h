#include "CoreEngine\IObjectComponent.h"
#include "CoreEngine\Event.h"
#include <glm\glm.hpp>


#pragma once
class ICamera : public IObjectComponent
{
public:
	ICamera(ComponentInterface* core) : IObjectComponent(core) {}

	virtual void translate(glm::vec3) = 0;
	virtual void rotate(double x, double y) = 0;

	virtual glm::vec3 getPos() = 0;
	virtual glm::vec3 getForward() = 0;
	virtual glm::vec3 getRight() = 0;
	virtual glm::vec3 getUp() = 0;

	virtual void setPos(glm::vec3) = 0;
	virtual void setForward(glm::vec3) = 0;

};
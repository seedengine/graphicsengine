using namespace std;
#include <glm\glm.hpp>
#include <glm\common.hpp>

#include "ILight.h"
#include "CoreEngine\MemoryAssignment.h"

class ComponentInterface;
class DescriptorSet;

#pragma once
class BaseLight : public ILight
{
public:
	BaseLight(ComponentInterface*, ILightRenderer*, glm::vec3 c, float i);

	float getIntensity() { return _intensity; }
	glm::vec3 getColor() { return _color; }

	virtual void updateData(GraphicsModuleAction::BaseLightData* data) { _updateData(data); ILight::updateData(data); }

	static const constexpr uint32_t getLightSize() { return 16; }
	virtual uint32_t getDataSize() { return getLightSize(); }
	const void writeData(DescriptorSet* setMgr, DescriptorSet* shadowSet);
protected:
	virtual void _updateData(GraphicsModuleAction::BaseLightData*);
	virtual void _writeData(DescriptorSet* setMgr, DescriptorSet* shadowSet, uint32_t offset = 0);
private:
	glm::vec3 _color;
	float _intensity;
};

#pragma once
class DirectionalLight : public BaseLight
{
public:
	DirectionalLight(ComponentInterface*, ILightRenderer*, glm::vec3 c, float i, glm::vec3 dir);

	void setNormal(glm::vec3 n)
	{
		_normal = n;
	}

	glm::vec3 getNormal()
	{
		return _normal;
	}

	virtual void updateData(GraphicsModuleAction::DirectionalLightData* data) { _updateData(data); ILight::updateData(data); }

	~DirectionalLight()
	{
	}

	static const constexpr uint32_t getLightSize() { return BaseLight::getLightSize() + 16; }
	virtual uint32_t getDataSize() { return getLightSize(); }
protected:
	virtual void _updateData(GraphicsModuleAction::DirectionalLightData*);
	virtual void _writeData(DescriptorSet* setMgr, DescriptorSet* shadowSet, uint32_t offset = 0);
private:
	glm::vec3 _normal;
};

#pragma once
class PointLight : public BaseLight
{
public:
	PointLight(ComponentInterface*, ILightRenderer*, glm::vec3 c, float i, float ex, float lin, float con, glm::vec3 p);

	void setPosition(glm::vec3 b)
	{
		_pos = b;
	}
	glm::vec3 getPosition()
	{
		return _pos;
	}

	virtual void updateData(GraphicsModuleAction::PointLightData* data) { _updateData(data); ILight::updateData(data); }

	static const constexpr uint32_t getLightSize() { return DirectionalLight::getLightSize() + 32; }
	virtual uint32_t getDataSize() { return getLightSize(); }
protected:
	virtual void _updateData(GraphicsModuleAction::PointLightData*);
	virtual void _writeData(DescriptorSet* setMgr, DescriptorSet* shadowSet, uint32_t offset = 0);
private:
	void calcRange()
	{
		_range = abs((-_linear + sqrtf(_linear * _linear - 4 * _exponent * _constant * getIntensity() * glm::length(getColor()))) / (2 * _exponent));
		if (isnan(_range))
			_range = 100.0f;
	}

	float _constant, _linear, _exponent;
	glm::vec3 _pos; //technically nor really a light property, needs to be applied with transformations :/
	float _range;
};

#pragma once
class SpotLight : public PointLight
{
public:
	SpotLight(ComponentInterface*, ILightRenderer*, glm::vec3 c, float i, float ex, float lin, float con, glm::vec3 p, glm::vec3 dir, float cut);

	glm::vec3 getDirection() { return _direction; }
	float getCutoff() { return _cutoff; }

	virtual void updateData(GraphicsModuleAction::SpotLightData* data) { _updateData(data); ILight::updateData(data); }

	~SpotLight() {};

	static const constexpr uint32_t getLightSize() { return PointLight::getLightSize() + 68; }
	virtual uint32_t getDataSize() { return getLightSize(); }

protected:
	virtual void _updateData(GraphicsModuleAction::SpotLightData*);
	virtual void _writeData(DescriptorSet* setMgr, DescriptorSet* shadowSet, uint32_t offset = 0);
private:
	glm::vec3 _direction;
	float _cutoff;
};
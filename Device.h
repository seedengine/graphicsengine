#include <vulkan\vulkan.h>

#include "VulkanDeviceFactory.h"
#include "Queue.h"
#include "BufferMemoryInterface.h"
#include "Semaphore.h"
#include "CoreEngine\BlockMemoryManager.h"
#include "DescriptorSetFactory.h"

class SwapChain;
class SwapChainImage;
class PhysicalDevice;
class GraphicsEngine;

//Wrapper class for Vulkan Devices
#pragma once
class Device : public IHandle<VkDevice>
{
	friend void VulkanDeviceFactory::buildDevice(Device&);
public:
	Device(ComponentInterface*);

	void construct(ComponentInterface* core, PhysicalDevice* dev, VkDeviceCreateInfo& info, int presentationQueue, int computeQueue, int transferQueue);

	//Gets Physical device the Logical device is operating on
	const PhysicalDevice* getPhysicalDevice() const { return physical; }
	//Get the correspondant Queues
	///TODO WARNING NOT ALL CARDS SUPPORT EVERYTHING
	Queue& getGraphicsQueue() { return graphicsQueue; }
	Queue& getComputationQueue() { return computeQueue; }
	Queue& getTransferQueue() { return transferQueue; }

	const VkDevice& getHandle() const { return device; }
	BlockMemoryManager<BufferMemoryInterface>* getDataMemoryHandler() { return &dataHandler; }
	DescriptorSetFactory& getDescriptorFactory() { return descriptorManager; }
	VkDescriptorPool& getDescriptorPool() { return descPool; }

	//Submit a Render Command to the Device
	//Present a rendered Image to the Window
	void present(GraphicsEngine*);
	//Pause until Device is operational
	void waitUntilIdle();

	void destroy();

	~Device();
private:
	VkDevice device;

	PhysicalDevice* physical;

	Queue graphicsQueue;
	Queue computeQueue;
	Queue transferQueue;

	Semaphore waitSemaphore, frameDoneSemaphore;

	BlockMemoryManager<BufferMemoryInterface> dataHandler;
	DescriptorSetFactory descriptorManager;
	VkDescriptorPool descPool;

	static const VkPipelineStageFlags waitStages[];
};

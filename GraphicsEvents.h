#include "CoreEngine\CoreEvents.h"

class IRenderSystem;
class Surface;
class Device;
class IRenderContext;
class DescriptorSetFactory;
class CommandBufferRecord;
class CommandBuffer;

#pragma once
DEFINE_EVENT(FinalizationPreparationEvent)
public:
	FinalizationPreparationEvent(std::set<uint64_t>* ids) : ids(ids) {}
	void addDependentEvent(uint64_t id) { ids->insert(id); }
private:
	std::set<uint64_t>* ids;
};
DEFINE_EVENT(RecordEvent) //depends on setup of API
public:
	void setRenderContext(IRenderContext* renderContext) { context = renderContext; }
	IRenderContext* getRenderContext() const { return context; }
private:
	IRenderContext* context;
};
DEFINE_EVENT(PipelineBuildEvent)
public:
	void setRenderSystem(IRenderSystem* renderSystem) { system = renderSystem; }
	IRenderSystem* getRenderSystem() { return system; }
	void setSurface(Surface* surface) { this->surface = surface; }
	Surface* getSurface() const { return surface; }
private:
	IRenderSystem* system;
	Surface* surface; //abstract Surface to window context
};
DEFINE_EVENT(RenderSystemSetupEvent)
public:
	void setSurface(Surface* surface) { this->surface = surface; }
	Surface* getSurface() const { return surface; }
private:
	Surface* surface; //abstract Surface to window context
};
DEFINE_EVENT(RenderSystemInitEvent)
};

DEFINE_EVENT(DescriptorRegistrationEvent)
public:
	void setDescriptorFactory(DescriptorSetFactory* layoutBuilder) { builder = layoutBuilder; }
	DescriptorSetFactory* getDescriptorFactory() const { return builder; }
private:
	DescriptorSetFactory* builder;
};
DEFINE_EVENT(DescriptorAllocationEvent)
public:
	void setDescriptorFactory(DescriptorSetFactory* layoutBuilder) { builder = layoutBuilder; }
	DescriptorSetFactory* getDescriptorFactory() const { return builder; }
private:
	DescriptorSetFactory* builder;
};

DEFINE_EVENT(GraphicsMemoryReadyEvent)
public:
	void setDeviceHandle(Device* dev) { device = dev; }
	Device* getDeviceHandle() const { return device; }
private:
	Device* device;
};

DEFINE_EVENT(ExecuteRenderPass)
public:
	void setCommandBuffer(CommandBufferRecord* buf) { buffer = buf; }
	CommandBufferRecord* getCommandBuffer() const { return buffer; }
	void setBufferID(uint32_t id) { bufferID = id; }
	uint32_t getBufferID() const { return bufferID; }
private:
	CommandBufferRecord* buffer;
	uint32_t bufferID;
};

DEFINE_EVENT(RecordSubpassEvent)
public:
	void setCommandBuffer(CommandBuffer* buf) { buffer = buf; }
	CommandBuffer* getCommandBuffer() const { return buffer; }
	void setBufferID(uint32_t id) { bufferID = id; }
	uint32_t getBufferID() const { return bufferID; }
private:
	CommandBuffer* buffer;
	uint32_t bufferID;
};


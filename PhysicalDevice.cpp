#include "PhysicalDevice.h"
#include "Surface.h"
#include "UtilStruct.h"
#include "GraphicsExceptions.h"
#include "Instance.h"

#include <algorithm>

std::vector<PhysicalDevice> PhysicalDevice::devList;



PhysicalDevice::PhysicalDevice(PhysicalDevice && other)
{
	handle = std::move(other.handle);
	score = std::move(other.score);
	deviceQueues = std::move(other.deviceQueues);
	properties = std::move(other.properties);
	features = std::move(other.features);
}

PhysicalDevice & PhysicalDevice::operator=(PhysicalDevice && other)
{
	handle = std::move(other.handle);
	score = std::move(other.score);
	deviceQueues = std::move(other.deviceQueues);
	properties = std::move(other.properties);
	features = std::move(other.features);

	return *this;
}

void PhysicalDevice::construct(VkPhysicalDevice&& dev)
{
	handle = std::move(dev);
	vkGetPhysicalDeviceProperties(dev, &properties);
	vkGetPhysicalDeviceFeatures(handle, &features);
}

void PhysicalDevice::evaluateDevice(Surface& surface)
{
	score = 0;
	VkPhysicalDeviceProperties deviceProperties;
	VkPhysicalDeviceFeatures deviceFeatures;
	vkGetPhysicalDeviceProperties(handle, &deviceProperties);
	vkGetPhysicalDeviceFeatures(handle, &deviceFeatures);

	// Discrete GPUs have a significant performance advantage
	if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
		score += 100000;
	}

	// Maximum possible size of textures affects graphics quality
	score += deviceProperties.limits.maxImageDimension2D;

	for (QueueFamily fam : deviceQueues)
	{
		score += fam.getSize() * (fam.isGraphicQueueFamily() ? 1000 : 0); //more graphics families are better
		score += fam.getSize() * (fam.isTransferQueueFamily() ? 100 : 0); //transfer queues are useful for better memory handling
		score += fam.getSize() * (fam.isComputeQueueFamily() ? 10 : 0);   //computation queues can be useful for certain renderings
		if (fam.isPresentationQueue())
			score += 10000;
	}

	// Application can't function without geometry shaders
	if (!deviceFeatures.geometryShader || !deviceFeatures.logicOp)
		score *= -1;
}

bool PhysicalDevice::isDeviceSuitable(PhysicalDevice& dev, Surface& surface) //check whether surface is valid before
{
	//SurfacePresentationDetails properties = surface.getSurfaceProperties(); //gets extended surface data for evaluation

	VkPhysicalDeviceFeatures supportedFeatures;
	vkGetPhysicalDeviceFeatures(dev.getHandle(), &supportedFeatures);

	return supportedFeatures.samplerAnisotropy;
}
//consider standard Queue Structures from Vendor (Nvidia dedicated transport queues for example)
void PhysicalDevice::readQueueFamilies(Surface& surface)
{
	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(handle, &queueFamilyCount, VK_NULL_HANDLE); //gets amount of properties

	std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount); //allocates avecotr of that size
	vkGetPhysicalDeviceQueueFamilyProperties(handle, &queueFamilyCount, queueFamilies.data()); //pulls actual data

	deviceQueues.reserve(queueFamilyCount); //allocates area to avoid reallocation while pushing Queues
	unsigned it = 0;
	VkBool32 presentSupport = false;
	bool canPresentToSurface = false;
	for (VkQueueFamilyProperties& queueFam : queueFamilies)
	{
		vkGetPhysicalDeviceSurfaceSupportKHR(handle, it, surface.getHandle(), &presentSupport); //checks if queue can present on surface
		deviceQueues.push_back(std::move(QueueFamily(it, presentSupport, queueFam))); //stores queue family data in array
		if (presentSupport)
			canPresentToSurface = true;
		it++;
	}
	
	if (canPresentToSurface)
		surface.readSurfaceProperties(this); //if this device can present on the surface configure surface accordingly
}


PhysicalDevice::~PhysicalDevice()
{
}

PhysicalDevice::QueueFamily PhysicalDevice::findFirstQueueFamily(int type)
{
	QueueFamily fallback;
	for (unsigned i = 0; i < deviceQueues.size(); i++)
		if (deviceQueues[i].isType(type) && (fallback.getType() > deviceQueues[i].getType() || !fallback.getType()))
		{
			fallback = deviceQueues[i];
			break;
		}

	if (fallback.getID() != -1)
		return fallback;
	else
		throw VulkanApplicationError("Requested Queue not found");
}

PhysicalDevice::QueueFamily PhysicalDevice::getPresentationQueue()
{
	VkBool32 presentSupport = false;
	for (unsigned i = 0; i < deviceQueues.size(); i++)
		if (deviceQueues[i].isType(VK_QUEUE_GRAPHICS_BIT) && deviceQueues[i].isPresentationQueue())
			return deviceQueues[i];
	throw VulkanApplicationError("Requested Queue not found");
}


void PhysicalDevice::scanDevices(Instance& instance, Surface& surface)
{
	uint32_t deviceCount(0);
	vkEnumeratePhysicalDevices(instance.getHandle(), &deviceCount, VK_NULL_HANDLE); //get all physical devices in System
	if (deviceCount == 0)
		throw VulkanNotSupportedError("failed to find GPUs with Vulkan support!");

	std::vector<VkPhysicalDevice> devices(deviceCount);
	vkEnumeratePhysicalDevices(instance.getHandle(), &deviceCount, devices.data()); //write device objects in array

	devList.clear();
	devList.reserve(deviceCount);
	for (unsigned i = 0; i < devices.size(); i++)
	{
		PhysicalDevice dev;
		dev.construct(std::move(devices[i])); //build instances of PhysicalDevice from the vkPhysicalDevice object
		if (isDeviceSuitable(dev, surface))
		{
			dev.evaluateDevice(surface); //score the devices to pick the optimal one
			uint32_t pos = insert(std::move(dev));
			devList[pos].readQueueFamilies(surface); //finds presentation Queue
		}
	}
}

PhysicalDevice* PhysicalDevice::getBestFreeDevice()
{
	return &devList.back();
}

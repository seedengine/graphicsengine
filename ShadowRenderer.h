#pragma once
#include "RenderContext.h"
#include "CoreEngine\Event.h"
#include "Subpass.h"
#include "DepthImage.h"
#include "ILight.h"

//JUST TO REMEMBER
//STEP 1
//WE TRANSFORM THE WORLD SPACE INTO THE LIGHT SPACE AND RENDER A DEPTH MAP
//STEP 2
//WE RENDER THE LIGHT PASS BY TRANSFORMING THE POSITIONS **BOTH INTO LIGHT AND CAMERA SPACE**
//WE ADD A STEP IN THE FRAMEBUFFER THAT DOES A DEPTH CHECK AGAINST THE INPUT ATTACHMENT.


class ShadowRenderer :
	public Subpass,
	public EventHandler
{
public:
	
	ShadowRenderer(ComponentInterface* core);

	void construct(DepthImage* depthImage, ILightRenderer* lightProvider);

	virtual void operator()(ComponentInterface*, const EventBase*);
	virtual void render(IRenderContext*);

private:
	DescriptorSet* inputAttachment;
	DescriptorSet* depthMatrixSet;
	MemoryRange depthMatrixAllocation;
	glm::mat4 depthMat;
};



#include <iostream>

#include "Instance.h"
#include "GraphicsExceptions.h"
#include "VulkanEXT.h"
#include "CoreEngine/Log.h"

Log* Instance::logger = nullptr;

Instance::Instance() : debugCallback(VK_NULL_HANDLE)
{
}

void Instance::construct(VkInstanceCreateInfo &createInfo)
{
	if (vkCreateInstance(&createInfo, VK_NULL_HANDLE, &instance) != VK_SUCCESS) //Tries to build Instance according to Specifications throws exception of Failure.
		throw VulkanConstructionError("Failed to build Vulkan Instance");
}

void Instance::destroy()
{
	if (debugCallback != VK_NULL_HANDLE)
		DestroyDebugReportCallback(instance, debugCallback, VK_NULL_HANDLE);
	vkDestroyInstance(instance, VK_NULL_HANDLE);
}

const VkInstance& Instance::getHandle() const
{
	return instance;
}


Instance::~Instance()
{
}


void Instance::buildDebugCallback(Log* logger)
{
	if (!this->logger)
		this->logger = logger;
	else
		logger->writeError("Tried to modify Vulkan Logger Reference.");

	VkDebugReportCallbackCreateInfoEXT debugInfo = {};
	debugInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;

	debugInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT; //Debug Callback should be called on Errors and Warnings
	debugInfo.pfnCallback = VKDebugCallback; //Function Pointer to Debug Callback

	if (CreateDebugReportCallback(instance, &debugInfo, VK_NULL_HANDLE, &debugCallback) != VK_SUCCESS)
		throw VulkanConstructionError("failed to set up debug callback!");
}


VKAPI_ATTR VkBool32 VKAPI_CALL Instance::VKDebugCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType, uint64_t obj, size_t location, int32_t code, const char * layerPrefix, const char * msg, void * userData)
{
	if (logger)
		logger->writeError((std::string("[Vulkan Error] ") + msg).c_str());

	return VK_FALSE;
}

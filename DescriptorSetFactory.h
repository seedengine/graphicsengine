#include <vulkan\vulkan.h>
#include "CoreEngine\Event.h"
#include "DescriptorSet.h"
#include "CoreEngine\OrderedList.h"

class ComponentInterface;

#pragma once
//Globals over all Descriptor Sets
//All Types of descriptor sets are stored here and only referenced elsewhere
class DescriptorSetFactory : EventHandler
{
public:
	DescriptorSetFactory(ComponentInterface*);

	virtual void operator()(ComponentInterface*, const EventBase*);

	void registerBinding(ComponentInterface*, uint8_t set, VkDescriptorType type, uint32_t binding, uint32_t size /*size == 0 -> sampler*/);
	DescriptorSet* getDescriptorSet(ComponentInterface* core, uint32_t setPosition);

	void update();

private:

	VkDescriptorPool pool;

	static bool CompDescSet(DescriptorSet*const & left, DescriptorSet*const & right) { return left->getID() >= right->getID(); }

	OrderedList<DescriptorSet*, CompDescSet> sets;
	
	std::map<VkDescriptorType, uint32_t> typeCounts;

	VkDeviceSize uniformAlignment;
};


#include "VulkanEvent.h"
#include "Device.h"
#include "GraphicsExceptions.h"
VulkanEvent::VulkanEvent()
{}

void VulkanEvent::construct(Device& dev)
{
	VkEventCreateInfo info{};
	info.sType = VK_STRUCTURE_TYPE_EVENT_CREATE_INFO;

	if (vkCreateEvent(dev.getHandle(), &info, VK_NULL_HANDLE, &evtObj) != VK_SUCCESS)
		throw VulkanConstructionError("failed to create semaphores!");
}
const VkEvent& VulkanEvent::getHandle() const { return evtObj; }

void VulkanEvent::destroy(Device& dev)
{
	vkDestroyEvent( dev, evtObj, VK_NULL_HANDLE);
}

VulkanEvent::~VulkanEvent()
{

}

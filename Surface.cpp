#include "Surface.h"
#include "GraphicsExceptions.h"
#include "CoreEngine\IEngine.h"

#include "UserInterface/UserInterfaceModuleAction.h"

Surface::Surface()
{
}

void Surface::construct(Instance& instance, ComponentInterface* core)
{
	//construct a surface for the current GLFW context
	UserInterfaceModuleAction::GetDrawContextVulkan data;
	data.context = &instance.getHandle();
	data.returnData = (void*)&surface;

	core->getModule(USER_INTERFACE_MODULE_NAME)->doAction(UserInterfaceModuleAction::EngineAction::GET_DRAW_CONTEXT_VULKAN, &data);

	if (surface == VK_NULL_HANDLE)
		throw ContextError("GLFW failed to properly construct Vulkan Context");
}


const VkSurfaceKHR& Surface::getHandle() const
{
	return surface;
}

SurfacePresentationDetails Surface::getSurfaceProperties()
{
	return surfaceDetails; //get current surface data
}

VkSurfaceFormatKHR Surface::getImageViewFormat()
{
	return imageFormat;
}

VkExtent2D Surface::getExtend()
{
	return surfaceExtent;
}

void Surface::destroy(Instance& instance)
{
	//destroy surface
	vkDestroySurfaceKHR(instance.getHandle(), surface, VK_NULL_HANDLE);
}

Surface::~Surface()
{
}

void Surface::readSurfaceProperties(const PhysicalDevice *dev)
{
	const VkPhysicalDevice& device = dev->getHandle();
	surfaceDetails = SurfacePresentationDetails();
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, &surfaceDetails.capabilities); //read surface Capabilities

	uint32_t formatCount;
	vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, VK_NULL_HANDLE); //Read all formats supported by the surface on the PhysicalDevice

	//If there are any formats found read their data
	if (formatCount != 0) {
		surfaceDetails.formats.resize(formatCount);
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, surfaceDetails.formats.data());
	}

	//do the same thing from formats with presentation modes
	uint32_t presentModeCount;
	vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, VK_NULL_HANDLE);

	if (presentModeCount != 0) {
		surfaceDetails.presentModes.resize(presentModeCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, surfaceDetails.presentModes.data());
	}

	if (surfaceDetails.formats.size() == 1 && surfaceDetails.formats[0].format == VK_FORMAT_UNDEFINED) {
		imageFormat = VkSurfaceFormatKHR{ VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };
	}

	for (const auto& availableFormat : surfaceDetails.formats) {
		if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
			imageFormat = availableFormat;
			break;
		}
	}

	if (surfaceDetails.capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max())
		surfaceExtent = surfaceDetails.capabilities.currentExtent;
	else
		throw FrameworkException("Invalid Surface Details.");
}


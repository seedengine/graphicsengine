#include <vulkan\vulkan.h>

#include "CoreEngine\IHandle.h"


#pragma once

class Device;

class VulkanEvent : public IHandle<VkEvent>
{
public:
	VulkanEvent();

	void construct(Device&);
	const VkEvent& getHandle() const;
	void destroy(Device&);

	~VulkanEvent();

private:
	VkEvent evtObj;

};

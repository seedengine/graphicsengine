#include "CoreEngine/Exceptions.h"

#pragma once
class ContextError : FrameworkException
{
public:
	ContextError(std::string& msg) : FrameworkException(msg) {}
	ContextError(const char* str) : FrameworkException(str) {}
};

class VulkanError : FrameworkException
{
public:
	VulkanError(std::string& msg) : FrameworkException(msg) {}
	VulkanError(const char* str) : FrameworkException(str) {}
};
class VulkanConstructionError : FrameworkException
{
public:
	VulkanConstructionError(std::string& msg) : FrameworkException(msg) {}
	VulkanConstructionError(const char* str) : FrameworkException(str) {}
};
class VulkanDestructionError : FrameworkException
{
public:
	VulkanDestructionError(std::string& msg) : FrameworkException(msg) {}
	VulkanDestructionError(const char* str) : FrameworkException(str) {}
};
class VulkanValidationError : FrameworkException
{
public:
	VulkanValidationError(std::string& msg) : FrameworkException(msg) {}
	VulkanValidationError(const char* str) : FrameworkException(str) {}
};
class VulkanNotSupportedError : FrameworkException
{
public:
	VulkanNotSupportedError(std::string& msg) : FrameworkException(msg) {}
	VulkanNotSupportedError(const char* str) : FrameworkException(str) {}
};
class VulkanApplicationError : FrameworkException
{
public:
	VulkanApplicationError(std::string& msg) : FrameworkException(msg) {}
	VulkanApplicationError(const char* str) : FrameworkException(str) {}
};

class AlignmentError : FrameworkException
{
public:
	AlignmentError() : FrameworkException("Input Provided does not match Alignment specified.") {}
};
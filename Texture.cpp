#include "Texture.h"
#include "Device.h"

#include <cmath>
#include <algorithm>
#include <fstream>
#include <exception>

#include "GraphicsEngine.h"

//std::mutex Texture::mut;
//VkCommandBuffer Texture::transferBuffer{ VK_NULL_HANDLE };
//VkDeviceMemory Texture::stageMem{ VK_NULL_HANDLE };
//VkBuffer Texture::stageBuffer{ VK_NULL_HANDLE };
VulkanBuffer* Texture::stageBuffer{ nullptr };

void Texture::load()
{
	ImageType _type = getFileExtension();

	std::ifstream file(path, std::ios::in | std::ios::binary);

	if (_type != getFileImageType(file))
	{
		printf("Magic Number mismatch with extension");
		file.close();
		return;
	}

	file.seekg(0);

	switch (_type)
	{
		case BMP:
			{
				file.seekg(10);//goes the the end of the Header

				unsigned int offset(0); //reads Data Offset
				file.read((char*)&offset, 4);

				file.seekg(18);
				file.read((char*)&_width, 4);
				file.read((char*)&_height, 4);


				file.seekg(28);
				file.read((char*)&_depth, 2);

				file.read((char*)&_compression, 4);


				uint32_t _bitMasks[3];
				if (_compression == 3)
				{
					file.read((char*)_bitMasks, 12);
				}
				else
					file.seekg(46);

				char* _colorTable;
				uint32_t _tableSize;

				file.read((char*)&_tableSize, 4);
				if (_tableSize == 1 || _tableSize == 4 || _tableSize == 8)
					_colorTable = (char*)malloc(_tableSize);
				else
					_colorTable = nullptr;

				if (_colorTable) //read color Table
					for (size_t i = 0; i < _tableSize; i++)
						file.read(&_colorTable[i], 1);

				unsigned int dataSize = (unsigned int)std::ceil((_width * _height * _depth) / 8);
				_data = (uint8_t*)malloc(dataSize);

				_structure = PixelStructure(_depth / 8);

				file.seekg(offset);
				unsigned int last = dataSize % 0xffffffff;
				unsigned int stepSize = (dataSize - last) / 0xffffffff; //if bigger than 32bit separate
				for (size_t i = 0; i < (dataSize - last); i += stepSize)
					file.read((char*)&_data[i], stepSize);
				file.read((char*)&_data[dataSize - last], last);

				file.close();


				if (_compression == 3 && (_depth & 48))
					throw std::runtime_error("Wrong compression or Depth. It doesn't work together");
				if (_depth == 1)
				{
					uint8_t* newData = new uint8_t[_width * _height * 3];
					for (uint32_t i = 0; i < (_width * _height) / 8; i++)
					{
						uint8_t temp = _data[i];
						for (unsigned int j = 0; j < 8; j++)
						{
							if (temp & (1 << j))
								*((int*)(&newData[(i * 8 + j) * 3])) = 0x00FFFFFF;
						}
					}
					free(_data);
					_data = newData;
				}
				else if (_depth == 24)
				{
					for (uint32_t i = 0; i < _width * _height; i++) //flip from BGR to RGB
					{
						char temp = _data[(i * 3) + 2];
						_data[(i * 3) + 2] = _data[(i * 3)];
						_data[(i * 3)] = temp;
					}
				}

				break;
			}
		case PNG:
			{

				png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
				if (!png) abort(); //better error handling
				png_infop info = png_create_info_struct(png);
				if (!info) abort(); //better error handling

				if (setjmp(png_jmpbuf(png))) abort(); //will jump to this when crashing? intersting

				png_set_read_fn(png, (png_voidp)&file, userReadData);
				png_read_info(png, info);

				_width = png_get_image_width(png, info);
				_height = png_get_image_height(png, info);
				uint8_t color_type = png_get_color_type(png, info);
				uint8_t bit_depth = png_get_bit_depth(png, info);

				// Read any color_type into 8bit depth, RGBA format.
				// See http://www.libpng.org/pub/png/libpng-manual.txt

				if (bit_depth == 16)
					png_set_strip_16(png);

				if (color_type == PNG_COLOR_TYPE_PALETTE)
					png_set_palette_to_rgb(png);

				// PNG_COLOR_TYPE_GRAY_ALPHA is always 8 or 16bit depth.
				if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
					png_set_expand_gray_1_2_4_to_8(png);

				if (png_get_valid(png, info, PNG_INFO_tRNS))
					png_set_tRNS_to_alpha(png);

				// These color_type don't have an alpha channel then fill it with 0xff.
				if (color_type == PNG_COLOR_TYPE_RGB || color_type == PNG_COLOR_TYPE_GRAY || color_type == PNG_COLOR_TYPE_PALETTE)
					png_set_filler(png, 0xFF, PNG_FILLER_AFTER);

				if (color_type == PNG_COLOR_TYPE_GRAY || color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
					png_set_gray_to_rgb(png);

				png_read_update_info(png, info);

				png_bytep* row_pointers = (png_bytep*)malloc(sizeof(png_bytep) * _height);

				_data = (uint8_t*)malloc(_height * png_get_rowbytes(png, info));

				for (int32_t y = _height - 1; y >= 0; y--)
					row_pointers[(_height - 1) - y] = _data + y * png_get_rowbytes(png, info);

				png_read_image(png, row_pointers);

				free(row_pointers);
				png_destroy_read_struct(&png, &info, nullptr);

				_structure = RGBA;

				file.close();
				break;
			}
		default:
			file.close();
	}
}

Texture::~Texture()
{
}

void Texture::operator()(ComponentInterface* core, const EventBase* evt)
{
	if (evt->is<RecordEvent>())
	{
		RecordEvent* data = (RecordEvent*)evt;
		set->bind((CommandBufferRecord*)data->getRenderContext()->getContext(), descriptorID);
	}
	else if (evt->is<DescriptorRegistrationEvent>())
	{
		DescriptorRegistrationEvent* data = (DescriptorRegistrationEvent*)evt;
		data->getDescriptorFactory()->registerBinding(core, 1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 0, 0);
		set = data->getDescriptorFactory()->getDescriptorSet(core, 1);
	}
	else if (evt->is<DescriptorAllocationEvent>())
	{
		DescriptorAllocationEvent* data = (DescriptorAllocationEvent*)evt;
		descriptorID = set->allocate(0);
		ImageSampler d{ (Image*)this , nullptr};
		set->set(descriptorID, &d);
	}
	else if (evt->is<VulkanDeviceConstructionEvent>())
	{
		VulkanDeviceConstructionEvent* data = (VulkanDeviceConstructionEvent*)evt;

		Image::construct(*data->getDeviceHandle(), _width, _height, getFormatFromStructure());

		Image::init(*data->getDeviceHandle());

		stageBuffer->operator()(core, evt);

		stageBuffer->set(_data, _width * _height * _structure);

		stageBuffer->copy(VkExtent3D{ _width, _height, 1 }, VkOffset3D{ 0, 0, 0 }, *this);

		VkSamplerCreateInfo samplerInfo = {};
		samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
		samplerInfo.magFilter = VK_FILTER_LINEAR;
		samplerInfo.minFilter = VK_FILTER_LINEAR;
		samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
		samplerInfo.anisotropyEnable = VK_FALSE;
		samplerInfo.maxAnisotropy = 1;
		samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
		samplerInfo.unnormalizedCoordinates = VK_FALSE;
		samplerInfo.compareEnable = VK_FALSE;
		samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
		samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
		samplerInfo.mipLodBias = 0.0f;
		samplerInfo.minLod = 0.0f;
		samplerInfo.maxLod = 0.0f;
		if (vkCreateSampler(*data->getDeviceHandle(), &samplerInfo, nullptr, &sampler) != VK_SUCCESS) {
			throw std::runtime_error("failed to create texture sampler!");
		}
	}
	else if (evt->is<VulkanDeviceDestructionEvent>())
	{
		VulkanDeviceDestructionEvent* data = (VulkanDeviceDestructionEvent*)evt;
		vkDestroySampler(*data->getDeviceHandle(), sampler, nullptr);
		Image::destroy(*data->getDeviceHandle());

		delete stageBuffer; //is it risky to do that? after this point all actions should be completed on the buffer and destruction should be safe. Or am i forgetting something
		stageBuffer = nullptr;
	}
	else if (evt->is<LoadEvent>())
	{
		this->load();

		if (stageBuffer == nullptr)
			stageBuffer = new VulkanBuffer(core, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, 0x400000);

	}
}

Texture::ImageType Texture::getFileExtension()
{
	if (path.substr(path.find_last_of('.')) == ".bmp")
		return ImageType::BMP;
	else if (path.substr(path.find_last_of('.')) == ".png")
		return ImageType::PNG;
	else
		return NIL;
}

Texture::ImageType Texture::getFileImageType(std::ifstream& file)
{
	file.seekg(0);

	char Sig[8];
	file.read(Sig, 8);
	const static char bmpMagNbr[] = { (char)0x42, (char)0x4D };
	const static char pngMagNbr[] = { (char)0x89, (char)0x50, (char)0x4E, (char)0x47, (char)0x0D, (char)0x0A, (char)0x1A, (char)0x0A };
	
	if (memcmp(Sig, bmpMagNbr, 2) == 0)
		return BMP;
	else if (memcmp(Sig, pngMagNbr, 8) == 0)
		return PNG;
	else
		return NIL;
}

inline void Texture::listRequirements(std::set<uint64_t>& req)
{
	req.insert(LoadEvent::getClassID());
	req.insert(VulkanInstanceConstructionEvent::getClassID());
	req.insert(VulkanDeviceConstructionEvent::getClassID());
	req.insert(DescriptorAllocationEvent::getClassID());
	req.insert(DescriptorRegistrationEvent::getClassID());
	req.insert(RecordEvent::getClassID());
	req.insert(VulkanDeviceDestructionEvent::getClassID());
}

void Texture::userReadData(png_structp pngPtr, png_bytep data, png_size_t length)
{
	png_voidp a = png_get_io_ptr(pngPtr);
	((std::istream*)a)->read((char*)data, length);
}

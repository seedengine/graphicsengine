#include "DescriptorSet.h"
#include "CommandBuffer.h"
#include "GraphicsEngine.h"
#include "Pipeline.h"
#include "Device.h"

DescriptorSetInterface::DescriptorSetInterface(ComponentInterface* core, VkDescriptorPool* pool, DescriptorSet* layout, uint32_t setID, VkDeviceSize alignment) :
	IMemoryInterface(0),
	core(core),
	pool(pool),
	layout(layout),
	setID(setID),
	alignment(alignment)
{
}
//we need staging here
MemoryRange DescriptorSetInterface::allocate(size_t size)
{
	VkDescriptorSetAllocateInfo info{};
	info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	info.pNext = nullptr;
	info.descriptorPool = *pool;
	info.descriptorSetCount = 1;
	info.pSetLayouts = &layout->getHandle();

	std::unique_lock<std::mutex> lock;

	MemoryRange range;
	range.memIndex = allocations.size();
	range.offset = 0;
	range.size = size;

	VkDescriptorSet set;

	auto res = vkAllocateDescriptorSets(((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice(), &info, &set);

	if (set == VK_NULL_HANDLE)
		throw VulkanValidationError("Set could not be allocated.");

	if (size > 0)
	{
		uint32_t dataSize = size;
		if (dataSize % alignment)
			dataSize = (uint32_t)(dataSize - (dataSize % alignment) + alignment);

		MemoryRange memData = core->getMemoryHandler()->allocate(dataSize);
		allocations.push_back(Descriptor{ new VulkanBuffer(core, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, dataSize), set });
	}
	else
		allocations.push_back(Descriptor{ nullptr, set });
	return range;
}

void DescriptorSetInterface::freeRange(MemoryRange memory)
{
	vkFreeDescriptorSets(((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice(), *pool, 1, &allocations[memory.memIndex].descriptor);
	delete allocations[memory.memIndex].buffer;
	//free set, free buffer, shrink memory somehow
}

void DescriptorSetInterface::set(MemoryRange range, void* data)
{
	std::unique_lock<std::mutex> guard(lock);
	if (range.size > 0)
	{
		allocations[range.memIndex].buffer->set(data, range.size);
		writeUniformBuffer(range);
	}
	else
	{
		ImageSampler* d = (ImageSampler*)data;
		writeImageBuffer(range, d->img, d->sampler, d->layout);
	}
}

void DescriptorSetInterface::set(MemoryRange range, void* data, size_t offset, size_t size)
{
	std::unique_lock<std::mutex> guard(lock);
	if (size > 0)
	{
		allocations[range.memIndex].buffer->set(data, size, offset);
	}
	else
	{
		throw FrameworkException("trying to write offset for a texture.");
	}
}

MemoryRange DescriptorSetInterface::copy(MemoryRange src, MemoryRange dst)
{
	std::unique_lock<std::mutex> guard(lock);
	pendingCopies.push_back(VkCopyDescriptorSet{
		VK_STRUCTURE_TYPE_COPY_DESCRIPTOR_SET,
		VK_NULL_HANDLE,
		allocations[src.offset].descriptor,
		(uint32_t)src.size,
		0,
		allocations[dst.offset].descriptor,
		(uint32_t)dst.size,
		0,
		1
		});
	update();
	return dst;
}

void DescriptorSetInterface::swap(MemoryRange src, MemoryRange dst)
{
	throw FrameworkException("Not implemented yet");
}

void DescriptorSetInterface::destroy()
{
	for (auto allocation : allocations)
	{
		vkFreeDescriptorSets(((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice(), *pool, 1, &allocation.descriptor);
		delete allocation.buffer;
	}
	allocations.clear();
}

void DescriptorSetInterface::update()
{
	if (!pendingWrites.empty() || !pendingCopies.empty())
	{
		//vkDeviceWaitIdle(((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice());
		Device& dev = ((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice();
		vkUpdateDescriptorSets(dev, pendingWrites.size(), pendingWrites.data(), pendingCopies.size(), pendingCopies.data());
		//vkDeviceWaitIdle(((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice());
		pendingWrites.clear();
		pendingCopies.clear();
		setData.clear();
	}
}


void DescriptorSetInterface::writeUniformBuffer(MemoryRange range)
{
	DescriptorData info;
	info.uniformInfo.buffer = allocations[range.memIndex].buffer->getBufferHandle();
	info.uniformInfo.offset = 0;
	info.uniformInfo.range = range.size;
	setData.push_back(info);
	
	pendingWrites.push_back(VkWriteDescriptorSet{
			VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
			VK_NULL_HANDLE,
			allocations[range.memIndex].descriptor,
			(uint32_t)range.offset,
			0, //potentially needs Resolution
			1,
			VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
			VK_NULL_HANDLE,
			&setData[setData.size() - 1].uniformInfo,
			VK_NULL_HANDLE
		});
	update();
}

void DescriptorSetInterface::writeImageBuffer(MemoryRange range, Image* img, VkSampler* sampler, VkImageLayout layout)
{
	DescriptorData info;
	info.imageInfo.imageLayout = layout;
	info.imageInfo.imageView = img->getImageView();
	if (sampler == nullptr)
		info.imageInfo.sampler = img->getSampler();
	else
		info.imageInfo.sampler = *sampler;
	setData.push_back(info);
	pendingWrites.push_back(VkWriteDescriptorSet{
			VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
			VK_NULL_HANDLE,
			allocations[range.memIndex].descriptor,
			(uint32_t)range.offset, //needs Resolution
			0, //potentially needs resolution
			1,
			VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
			&setData[setData.size() - 1].imageInfo,
			VK_NULL_HANDLE,
			VK_NULL_HANDLE
		});
	update();
}

void DescriptorSet::buildLayout(Device* dev)
{
	std::vector<VkDescriptorSetLayoutBinding> bindings;

	for (auto& b : allocationPools)
	{
		switch (b.type)
		{
		case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER:
			bindings.push_back(
				VkDescriptorSetLayoutBinding{
					b.binding,
					VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
					1, //for arrays
					VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
					nullptr
				});
			break;
		case VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER:
			bindings.push_back(
				VkDescriptorSetLayoutBinding{
					b.binding,
					VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
					1, //for arrays
					VK_SHADER_STAGE_FRAGMENT_BIT,
					nullptr
				});
			break;
		case VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT:
			bindings.push_back(
				VkDescriptorSetLayoutBinding{
					b.binding,
					VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT,
					1, //for arrays
					VK_SHADER_STAGE_FRAGMENT_BIT,
					nullptr
				});
			break;
		}
	}

	std::vector<VkDescriptorBindingFlagsEXT> bindingFlags(bindings.size(), VK_DESCRIPTOR_BINDING_UPDATE_UNUSED_WHILE_PENDING_BIT_EXT);
	VkDescriptorSetLayoutBindingFlagsCreateInfoEXT extInfo;
	extInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO_EXT;
	extInfo.pNext = nullptr;
	extInfo.bindingCount = bindingFlags.size();
	extInfo.pBindingFlags = bindingFlags.data();

	VkDescriptorSetLayoutCreateInfo layoutInfo{};
	layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutInfo.pNext = &extInfo;
	layoutInfo.bindingCount = bindings.size();
	layoutInfo.pBindings = bindings.data();

	if (vkCreateDescriptorSetLayout(*dev, &layoutInfo, nullptr, &layout) != VK_SUCCESS) {
		throw VulkanConstructionError("failed to create descriptor set layout!");
	}
}

void DescriptorSet::registerBinding(VkDescriptorType type, uint32_t binding, uint32_t size)
{	
	if (allocationPools.size() > binding && allocationPools[binding].binding != binding)
	{
		allocationPools.insert(allocationPools.begin() + binding, Binding{ (uint32_t)type, binding, size });
		this->size += size;
	}
	else if (allocationPools.size() <= binding)
	{
		allocationPools.push_back(Binding{ (uint32_t)type, binding, size });
		this->size += size;
	}

}

void DescriptorSet::bind(CommandBufferRecord* record, MemoryRange range)
{
	if (record->getCurrentPipeline()->hasSet(getID()) && !record->isSetBound(getID()))
	{
		record->bindSet(getID(), range.memIndex);
		vkCmdBindDescriptorSets(record->getCommandBuffer(), VK_PIPELINE_BIND_POINT_GRAPHICS, record->getCurrentPipeline()->getLayout(), getID(), 1, &memoryInterface.getAllocation(range.memIndex)->descriptor, 0, VK_NULL_HANDLE);
	}
}

void DescriptorSet::update()
{
	memoryInterface.update();
}

void DescriptorSet::set(MemoryRange range, void* data)
{
	IMemoryManager<DescriptorSetInterface, Binding>::set(range, data);
}

void DescriptorSet::set(MemoryRange range, void * data, size_t offset, size_t size)
{
	MemoryRange clone;
	clone.memIndex = range.memIndex;
	clone.offset = getBindingFromOffset(offset);
	clone.size = range.size; //insecure?
	IMemoryManager<DescriptorSetInterface, Binding>::set(clone, data, offset, size);
}

void DescriptorSet::writeUniformBuffer(MemoryRange range)
{
	MemoryRange range1 = range;
	range1.offset = getBindingFromOffset(range.size);

	memoryInterface.writeUniformBuffer(range1);
}

void DescriptorSet::writeImageBuffer(MemoryRange range, Image * img, VkSampler* sampler, VkImageLayout layout)
{
	memoryInterface.writeImageBuffer(range, img, sampler, layout);
}

size_t DescriptorSet::getBindingFromOffset(size_t offset)
{
	//int32_t internalOffset = offset;

	if (offset == 0)
		return 0;
	std::sort(allocationPools.begin(), allocationPools.end());
	for (auto binding : allocationPools)
	{
		//internalOffset -= binding.size;
		//if (internalOffset < 0)
		if (offset == binding.size)
			return binding.binding;
	}
	return -1;
}

#include "Queue.h"
#include "Device.h"
#include "GraphicsExceptions.h"

Queue::Queue()
{
}

Queue::Queue(const Queue &other) :
	queue(other.queue),
	queueFamilyIndex(other.queueFamilyIndex),
	pool(other.pool)
{
}


void Queue::construct(Device & dev, uint32_t queueFamilyIndex)
{
	//todo if queueFamilyIndex == -1 initialize dummy Queue

	vkGetDeviceQueue(dev.getHandle(), queueFamilyIndex, 0, &queue);

	this->queueFamilyIndex = queueFamilyIndex;

	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = queueFamilyIndex;
	poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
	if (vkCreateCommandPool(dev.getHandle(), &poolInfo, VK_NULL_HANDLE, &pool) != VK_SUCCESS) {
		throw VulkanConstructionError("failed to create command pool!");
	}
}

VkQueue& Queue::getQueueHandle()
{
	return queue;
}

VkCommandPool& Queue::getCommandPoolHandle()
{
	return pool;
}

uint32_t Queue::getFamilyID()
{
	return queueFamilyIndex;
}

void Queue::submitCommandBuffer(const VkCommandBuffer& submitInfo)
{
	queueLock.lock();
	buffers.push_back(submitInfo);
	queueLock.unlock(); 
}

void Queue::flush(Semaphore* waitSemaphore, Semaphore * signalSemaphore)
{
	queueLock.lock();
	if (buffers.empty())
	{
		queueLock.unlock();
		return;
	}
	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	//Specifies Semaphores to wait on
	submitInfo.waitSemaphoreCount = waitSemaphore != nullptr ? 1 : 0;
	submitInfo.pWaitSemaphores = waitSemaphore != nullptr ? &waitSemaphore->getHandle() : nullptr;
	//Specifies where to wait
	submitInfo.pWaitDstStageMask = &waitSemaphore->getStageMask();
	//Specifies command Buffers to submit
	submitInfo.commandBufferCount = buffers.size();
	submitInfo.pCommandBuffers = buffers.data();
	//Specifies Semaphores that should signal once submission is completed
	submitInfo.signalSemaphoreCount = signalSemaphore != nullptr ? 1 : 0;
	submitInfo.pSignalSemaphores = signalSemaphore != nullptr ? &signalSemaphore->getHandle() : nullptr;


	if (vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE) != VK_SUCCESS) {
		throw VulkanApplicationError("failed to submit draw command buffer!");
	}
	vkQueueWaitIdle(queue);

	buffers.clear();
	queueLock.unlock();
}

void Queue::wait()
{
	vkQueueWaitIdle(queue);
}

void Queue::destroy(Device & dev)
{
	queueLock.lock();
	wait();
	vkDestroyCommandPool(dev.getHandle(), pool, VK_NULL_HANDLE);
	queueLock.unlock();
}

Queue::~Queue()
{
}

#include <vulkan\vulkan.h>
#include "CoreEngine/DataStructures.h"
#include "CoreEngine\MemoryAssignment.h"
#include "CoreEngine/IMemoryInterface.h"
#include "CoreEngine/FreeListMemoryManager.h"
#include "VulkanBuffer.h"

#include <mutex>

class Device;

#pragma once
class BufferMemoryInterface : public IMemoryInterface<VulkanBuffer*> //would be reasonable to create a Buffer class to streamline all staging code
{
	friend class VulkanBuffer;
public:
	BufferMemoryInterface(size_t size);
	~BufferMemoryInterface();
	
	void constructInterface(ComponentInterface*, VkBufferUsageFlags usage);
	void destroyInterface(Device&);

	void resetBuffer(size_t, VkBufferUsageFlags);
	virtual size_t getSize() { return DATA_SIZE; }

	MemoryRange allocate(size_t);
	void freeRange( MemoryRange memory);

	void set(MemoryRange, void*);
	void set(MemoryRange, void*, size_t, size_t);
	MemoryRange copy( MemoryRange src, MemoryRange dst);
	void swap( MemoryRange src, MemoryRange dst);

	const VkDeviceMemory& getMemoryHandle(MemoryRange range) const;
	const VkBuffer& getBufferHandle(MemoryRange range) const;

	static VkDeviceMemory allocateVisible(Device& dev, VkMemoryRequirements req);
	static VkDeviceMemory allocate(Device& dev, VkMemoryRequirements req);
	static VkMemoryRequirements getRequirements(Device&, VkBuffer*);
	static uint32_t getMemoryType(Device&, uint32_t, VkMemoryPropertyFlags);
	
protected:
	ComponentInterface* core;

	size_t DATA_SIZE;


	VkBufferUsageFlags usage;
	VulkanBuffer* stageBuffer;

	VkCommandBuffer transferBuffer;
	std::mutex transferLock;
	std::mutex allocationLock;

	VkBuffer construct(Device& dev);

	void deallocate(Device& dev, VkDeviceMemory);
};


/*
typedef MemHdle& MemoryHandle -->opque reference to value thgat is being maintained by but Manager. Would allow Section locking. --> locking would be inefficient. Rather use Separate Tick to reallocate and manmage pool states. BUILD THREADED (SUPERSCALAR) PIPELINE
*/

#include "CoreEngine\DataStructures.h"
#include "CoreEngine\MemoryAssignment.h"

#include <atomic>
#include <string>
#include <vulkan\vulkan.h>
#include <vector>
#include "CoreEngine/IHandle.h"
#include "CoreEngine/Util.h"

#include "Compressor\DataStructures.h"
#pragma once


struct SurfacePresentationDetails {
	VkSurfaceCapabilitiesKHR capabilities;
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> presentModes;
};

struct VertexStructure {
	std::vector<VkVertexInputAttributeDescription> attributeArr;
	std::vector<VkVertexInputBindingDescription> bindingArr;
	VkPipelineVertexInputStateCreateInfo getVertexStructure()
	{
		return VkPipelineVertexInputStateCreateInfo
		{
			VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
			VK_NULL_HANDLE,
			0,
			uint32_t(bindingArr.size()),
			bindingArr.data(),
			uint32_t(attributeArr.size()),
			attributeArr.data()
		};
	}
};

struct AssemblyStructure {
	VkPrimitiveTopology topology;
	VkBool32 primitiveRestart;
	VkPipelineInputAssemblyStateCreateInfo getInputAssemblyState() //sets up vertex grouping
	{
		return VkPipelineInputAssemblyStateCreateInfo
		{
			VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
			VK_NULL_HANDLE,
			0,
			topology,
			primitiveRestart
		};
	}
};

struct DepthBias {
	float biasConstant;
	float biasSlope;
};

struct BlendAttachmentConverter {
	VkPipelineColorBlendAttachmentState operator()(const Compressor::ColorBlendAttachment& structure) {
		VkPipelineColorBlendAttachmentState tmp;

		tmp.colorWriteMask = structure.bitmask;

		tmp.srcAlphaBlendFactor = structure.alphaValA;
		tmp.alphaBlendOp = structure.alphaBlendOp;
		tmp.dstAlphaBlendFactor = structure.alphaValB;

		tmp.srcColorBlendFactor = structure.colorValA;
		tmp.colorBlendOp = structure.colorBlendOp;
		tmp.dstColorBlendFactor = structure.colorValB;

		tmp.blendEnable = true;

		return tmp;
	}
};





typedef uint32_t AttachmentRef;
typedef uint32_t SubpassRef;
const static SubpassRef externalSubpass = VK_SUBPASS_EXTERNAL;
struct Dependency
{
	SubpassRef pass;
	VkPipelineStageFlags stage;
	VkAccessFlags access;
};



struct MemoryBinding
{
	uint32_t set;
	uint32_t binding;
	MemoryRange allocation;
	VkDescriptorBufferInfo buffer;
	VkWriteDescriptorSet* writeRequest;
};



enum RenderState
{
	AMBIENT_PASS,
	FWD_RENDER,
	POST_RENDER
};
#include "Subpass.h"
#include "RenderPass.h"
#include "CoreEngine\OrderedList.hpp"

Subpass::Subpass(VkPipelineBindPoint bindPt, uint32_t commandBufferCount) : bindPt(bindPt), commands(commandBufferCount), id(-1)
{
	stencilReference.attachment = -1;
}

void Subpass::construct(Device& dev, uint32_t id)
{
	commands.init<Device&>(dev, false);
	this->id = id;
}

void * Subpass::getStepData()
{
	desc = buildInfo();
	return &desc;
}

void Subpass::executeStep(IRenderContext* context)
{
	const VkCommandBuffer& cmdBuf = getHandle();
	vkCmdExecuteCommands(((CommandBufferRecord*)context->getContext())->getCommandBuffer(), 1, &cmdBuf);
}

void Subpass::updateLatestBuffer(RenderPass& pass)
{
	{
		CommandBufferRecord rec;
		commands.getNext().record(&rec, VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT | VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT, VK_NULL_HANDLE, pass.getHandle(), id);

		RenderContext context(&rec, getID(), -1);
		render(&context);
	}
	commands.next();
}

void Subpass::updateAll(RenderPass& pass)
{
	commands.execute([this, &pass](CommandBuffer& buf, uint32_t pos) {
		CommandBufferRecord rec;
		buf.record( &rec, VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT | VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT, VK_NULL_HANDLE, pass.getHandle(), id);

		RenderContext context(&rec, getID(), -1);
		render(&context);
	});
}

void Subpass::destroy(Device& dev)
{
	commands.execute([this, &dev](CommandBuffer& buf, size_t id) {
		buf.destroy(dev);
	});
}

Subpass::~Subpass()
{
}

void Subpass::update() {
	if (id != -1)
	{
		updateLatestBuffer(*(RenderPass*)getParent());
	}
}

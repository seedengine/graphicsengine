#include "VulkanBuffer.h"
#include "GraphicsExceptions.h"
#include "Device.h"
#include "Instance.h"
#include "Image.h"
#include "VulkanGraphicsEvents.h"
#include "GraphicsModuleAction.h"
#include "GraphicsEngine.h"

VulkanBuffer::VulkanBuffer(ComponentInterface* core, VkBufferUsageFlags flags, size_t size) : EventHandler(core), core(core), dev(nullptr), flags(flags), state(UNDEFINED), size(size)
{
	if (core->getModule(GRAPHICS_MODULE_NAME)->getState() & GraphicsModuleAction::EngineState::DEVICE_CREATED)
	{
		dev = &((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice();
		allocate();
	}

	core->getEventBus()->registerHandler(this, VulkanDeviceConstructionEvent::getClassID());
	core->getEventBus()->registerHandler(this, VulkanDeviceDestructionEvent::getClassID());
}

VulkanBuffer::VulkanBuffer(const VulkanBuffer& other) :
	EventHandler(other.core),
	flags(other.flags),
	state(UNDEFINED),
	size(other.size),
	core(other.core),
	dev(other.dev)
{
	core->getEventBus()->removeHandler(this->getHandlerID());


	if (core->getModule(GRAPHICS_MODULE_NAME)->getState() & GraphicsModuleAction::EngineState::DEVICE_CREATED)
	{
		dev = &((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice();
		allocate();
		copy(size, 0, *((VulkanBuffer*)&other), 0);
	}
	core->getEventBus()->registerHandler(this, VulkanDeviceConstructionEvent::getClassID());
	core->getEventBus()->registerHandler(this, VulkanDeviceDestructionEvent::getClassID());
}

const VulkanBuffer & VulkanBuffer::operator=(const VulkanBuffer& other)
{
	core = other.core;
	dev = other.dev;
	flags = other.flags;
	state = other.state;
	size = other.size;

	if (core->getModule(GRAPHICS_MODULE_NAME)->getState() & GraphicsModuleAction::EngineState::DEVICE_CREATED)
	{
		dev = &((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice();
		allocate();
		copy(size, 0, *((VulkanBuffer*)&other), 0);
	}
	core->getEventBus()->removeHandler(this->getHandlerID());
	core->getEventBus()->registerHandler(this, VulkanDeviceConstructionEvent::getClassID());
	core->getEventBus()->registerHandler(this, VulkanDeviceDestructionEvent::getClassID());

	return *this;
}

void VulkanBuffer::operator()(ComponentInterface * core, const EventBase* evt)
{
	if (evt->is<VulkanDeviceConstructionEvent>())
	{
		dev = ((VulkanDeviceConstructionEvent*)evt)->getDeviceHandle();
		if (state == UNDEFINED)
			allocate();
	}
	else if (evt->is<VulkanDeviceDestructionEvent>())
	{
		if (mem != VK_NULL_HANDLE || buf != VK_NULL_HANDLE)
		{
			core->getLog()->writeWarning("Device is being destroyed before the buffer was destroyed. Attempting to delete buffer while still possible.");
			clear();
		}
		dev = nullptr;
	}
}

void VulkanBuffer::set(void * data, size_t size, size_t offset)
{
	std::lock_guard<std::mutex> lock(mut);

	if (dev == nullptr || mem == VK_NULL_HANDLE || buf == VK_NULL_HANDLE)
		throw VulkanApplicationError("Buffer not yet allocated");

	if(extData == nullptr)
		map();

	char* dataPtr = (char*)extData;

	dataPtr += offset;

	memcpy(dataPtr, data, size);
}

void VulkanBuffer::copy(size_t size, size_t offset, VulkanBuffer & other, size_t other_offset)
{
	mut.lock();
	if (other.flags & VK_BUFFER_USAGE_TRANSFER_SRC_BIT)
	{
		//could I generalize it and do it per tick
		//Does one Buffer needs regional updates, how how could i stack buffer updates efficiently
		//also where does the CmdBuf go
		vkResetCommandBuffer(cmdBuf, VkCommandBufferResetFlags{});
		VkCommandBufferBeginInfo info;
		info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		info.pNext = VK_NULL_HANDLE;
		info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
		info.pInheritanceInfo = VK_NULL_HANDLE;
		vkBeginCommandBuffer(cmdBuf, &info);
		VkBufferCopy copyList;
		copyList.srcOffset = other_offset;
		copyList.dstOffset = offset;
		copyList.size = size;
		vkCmdCopyBuffer(cmdBuf, other.buf, buf, 1, &copyList);
		vkEndCommandBuffer(cmdBuf);

		dev->getGraphicsQueue().submitCommandBuffer(cmdBuf);
		dev->getGraphicsQueue().flush(nullptr, nullptr);
	}
	mut.unlock();
}

void VulkanBuffer::copy(VkExtent3D extend, VkOffset3D offset, Image& image)
{
	mut.lock();
	vkResetCommandBuffer(cmdBuf, 0);

	VkCommandBufferBeginInfo info{};
	info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	vkBeginCommandBuffer(cmdBuf, &info);

	VkImageMemoryBarrier transferBarrier = {};
	transferBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	transferBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	transferBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
	transferBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	transferBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	transferBarrier.image = image.getImage();
	transferBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	transferBarrier.subresourceRange.baseMipLevel = 0;
	transferBarrier.subresourceRange.levelCount = 1;
	transferBarrier.subresourceRange.baseArrayLayer = 0;
	transferBarrier.subresourceRange.layerCount = 1;
	transferBarrier.srcAccessMask = 0;
	transferBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

	vkCmdPipelineBarrier(cmdBuf, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &transferBarrier);

	VkBufferImageCopy copyRegion{};
	copyRegion.bufferOffset = 0;
	copyRegion.bufferRowLength = 0;
	copyRegion.bufferImageHeight = 0;

	copyRegion.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	copyRegion.imageSubresource.mipLevel = 0;
	copyRegion.imageSubresource.baseArrayLayer = 0;
	copyRegion.imageSubresource.layerCount = 1;

	copyRegion.imageOffset = offset;
	copyRegion.imageExtent = extend;
	vkCmdCopyBufferToImage(cmdBuf, buf, image.getImage(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &copyRegion);


	VkImageMemoryBarrier shaderBarrier = {};
	shaderBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	shaderBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
	shaderBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	shaderBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	shaderBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	shaderBarrier.image = image.getImage();
	shaderBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	shaderBarrier.subresourceRange.baseMipLevel = 0;
	shaderBarrier.subresourceRange.levelCount = 1;
	shaderBarrier.subresourceRange.baseArrayLayer = 0;
	shaderBarrier.subresourceRange.layerCount = 1;
	shaderBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
	shaderBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

	vkCmdPipelineBarrier(cmdBuf, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, nullptr, 0, nullptr, 1, &shaderBarrier);

	vkEndCommandBuffer(cmdBuf);

	dev->getGraphicsQueue().submitCommandBuffer(cmdBuf); //BAD. Will have to determine queue based on mode or write some sort of inheritance
	dev->getGraphicsQueue().flush(nullptr, nullptr);
	mut.unlock();
}

void VulkanBuffer::map()
{
	VkResult res = vkMapMemory(*dev, mem, 0, size, 0, &extData);
	switch (res)
	{
	case VK_SUCCESS:
		break;
	case VK_ERROR_OUT_OF_HOST_MEMORY:
		throw VulkanApplicationError("Could not map Memory to host, not enough Memory available.");
		break;
	case VK_ERROR_OUT_OF_DEVICE_MEMORY:
		throw VulkanApplicationError("Could not allocate enough device memory for mapping.");
		break;
	case VK_ERROR_MEMORY_MAP_FAILED:
		throw VulkanApplicationError("Mapping Vulkan memory failed.");
		break;
	}
}

void VulkanBuffer::unmap()
{
	vkUnmapMemory(*dev, mem);
	extData = nullptr;
}


VulkanBuffer::~VulkanBuffer()
{
	if (mem != VK_NULL_HANDLE || buf != VK_NULL_HANDLE)
	{
		if (dev != nullptr)
			clear();
	}

	core->getEventBus()->removeHandler(this->getHandlerID());
}

void VulkanBuffer::allocate()
{
	mut.lock();
	if (state != UNDEFINED)
		throw VulkanConstructionError("Attempting to allocate over a valid Buffer. would cause Memory Leak.");
	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = size; //4MB
	bufferInfo.usage = flags;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	if (vkCreateBuffer(*dev, &bufferInfo, VK_NULL_HANDLE, &buf) != VK_SUCCESS) {
		throw std::runtime_error("failed to create buffer!");
	}

	VkMemoryRequirements req{};
	vkGetBufferMemoryRequirements(*dev, buf, &req);

	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = req.size; //4 MB
	allocInfo.memoryTypeIndex = getMemoryType(req.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT); //needs to allocate invisible

	if (vkAllocateMemory(*dev, &allocInfo, VK_NULL_HANDLE, &mem) != VK_SUCCESS)
		throw VulkanConstructionError("failed to allocate buffer memory!");

	vkBindBufferMemory(*dev, buf, mem, 0);

	state = EMPTY;

	VkCommandBufferAllocateInfo cmdInfo;
	cmdInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	cmdInfo.pNext = nullptr;
	cmdInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	cmdInfo.commandPool = dev->getGraphicsQueue().getCommandPoolHandle();
	cmdInfo.commandBufferCount = 1;
	vkAllocateCommandBuffers(*dev, &cmdInfo, &cmdBuf);

	mut.unlock();
}

void VulkanBuffer::clear()
{
	if (extData != nullptr)
		unmap();
	if (state == UNDEFINED)
		throw VulkanDestructionError("Attempting to destroy invalid Buffer.");
	mut.lock();
	vkDestroyBuffer(*dev, buf, VK_NULL_HANDLE);
	buf = VK_NULL_HANDLE;
	vkFreeMemory(*dev, mem, VK_NULL_HANDLE);
	mem = VK_NULL_HANDLE;
	vkFreeCommandBuffers(*dev, dev->getGraphicsQueue().getCommandPoolHandle(), 1, &cmdBuf);
	cmdBuf = VK_NULL_HANDLE;
	state = UNDEFINED;
	mut.unlock();
}

uint32_t VulkanBuffer::getMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties)
{
	VkPhysicalDeviceMemoryProperties memProperties;
	vkGetPhysicalDeviceMemoryProperties(dev->getPhysicalDevice()->getHandle(), &memProperties);
	for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++)
		if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties)
			return i;

	throw VulkanApplicationError("failed to find suitable memory type!");
}

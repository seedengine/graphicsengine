#include <vulkan\vulkan.h>
#include <mutex>
#include "CoreEngine\Event.h"

class Image;
class Device;

//Inheritance
#pragma once
class VulkanBuffer : public EventHandler
{
public:
	VulkanBuffer(ComponentInterface*, VkBufferUsageFlags, size_t size);
	VulkanBuffer(const VulkanBuffer&);
	const VulkanBuffer& operator=(const VulkanBuffer&);


	void operator()(ComponentInterface* core, const EventBase* evt);

	void set(void* data, size_t size, size_t offset = 0);
	void copy(size_t size, size_t offset, VulkanBuffer& other, size_t other_offset);
	void copy(VkExtent3D, VkOffset3D, Image&);

	void map();

	void unmap();

	const VkBuffer& getBufferHandle() const
	{
		return buf;
	}
	const VkDeviceMemory& getMemoryHandle() const
	{
		return mem;
	}

	~VulkanBuffer();
private:
	void allocate();
	void clear();


	uint32_t getMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties);

	VkDeviceMemory mem;
	VkBuffer buf;
	VkCommandBuffer cmdBuf;
	std::mutex mut;

	void* extData;

	ComponentInterface* core;
	Device* dev;

	VkBufferUsageFlags flags;
	size_t size;
	enum BufferState
	{
		UNDEFINED,
		EMPTY,
		WRITTEN
	} state;
};

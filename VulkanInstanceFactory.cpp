#include "VulkanInstanceFactory.h"
#include "Instance.h"
#include "CoreEngine/StaticCoreEngine.h"

#include <iostream>

 //TODO make defines for Engine

VulkanInstanceFactory::VulkanInstanceFactory(ComponentInterface* core) : appInfo{}, instanceInfo{}, logger(core->getLog())
{
	appInfo.sType				= VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pEngineName			= "Vulkan Framework";
	appInfo.engineVersion		= (uint32_t)VK_MAKE_VERSION(1, 0, 0);
	appInfo.apiVersion			= (uint32_t)VK_API_VERSION_1_2;

	instanceInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	instanceInfo.pApplicationInfo = &appInfo;

	uint32_t extensionCount;
	vkEnumerateInstanceExtensionProperties( NULL, &extensionCount, VK_NULL_HANDLE);
	if (core->getDebugState())
		logger->writeInfo("========================= Scanning for Instance Extensions ==========================");
	VkExtensionProperties* extension = (VkExtensionProperties*)malloc(extensionCount * sizeof(VkExtensionProperties));
	vkEnumerateInstanceExtensionProperties(NULL, &extensionCount, extension);
	for (VkExtensionProperties* it = extension; it < extension + extensionCount; it++) {
		if (core->getDebugState())
			logger->writeInfo((std::string("Found Instance Extension: ") + it->extensionName + " ver.: " + std::to_string(it->specVersion)).c_str());
		availableExtensions.insert(it->extensionName);
	}
	free(extension);
	if (core->getDebugState())
	{
		logger->writeInfo("========================= Scan for Instance Extensions completed ==========================");
		logger->writeInfo("====================== Scanning for Instance Validation Layers ======================");
	}
	uint32_t layerCount;
	vkEnumerateInstanceLayerProperties(&layerCount, VK_NULL_HANDLE);
	VkLayerProperties* layers = (VkLayerProperties*)malloc(layerCount * sizeof(VkLayerProperties));
	vkEnumerateInstanceLayerProperties(&layerCount, layers);
	for (VkLayerProperties* it = layers; it < layers + layerCount; it++) {
		if (core->getDebugState())
			logger->writeInfo((std::string("Found Validation Layer: ") + it->layerName + " ver.: " + std::to_string(it->implementationVersion)).c_str());
		availableLayers.insert(it->layerName);
	}
	free(layers);
	if (core->getDebugState())
		logger->writeInfo("========================= Scan for Instance Layers completed ==========================");
}
VulkanInstanceFactory::~VulkanInstanceFactory()
{}


void VulkanInstanceFactory::setAppVersion(uint32_t version)
{
	appInfo.pApplicationName = "Vulkan Engine";
	appInfo.applicationVersion = version;
}

bool VulkanInstanceFactory::isExtensionAvailable(const char *extension) const
{
	return availableExtensions.find(extension)->compare(extension) == 0;
}
bool VulkanInstanceFactory::validateExtensionList(const char** extensionList, size_t count) const
{
	for (unsigned i = 0; i < count; i++)
		if (!isExtensionAvailable(extensionList[i]))
			return false;
	return true;
}
void VulkanInstanceFactory::setExtensionList(const char** extensions, size_t count)
{
	instanceInfo.enabledExtensionCount = (uint32_t)count;
	instanceInfo.ppEnabledExtensionNames = extensions;
}

bool VulkanInstanceFactory::isLayerAvailable(const char *layer) const
{
	std::set<std::string>::const_iterator str;
	if ((str = availableLayers.find(layer)) != availableLayers.cend())
	{
		return str->compare(layer) == 0;
	}
	else
		return false;
}
bool VulkanInstanceFactory::validateLayerList(const char** layerList, size_t count) const
{
	for (unsigned i = 0; i < count; i++)
		if (!isLayerAvailable(layerList[i]))
			return false;
	return true;
}
void VulkanInstanceFactory::setLayerList(const char** layers, size_t count)
{
	instanceInfo.enabledLayerCount = (uint32_t)count;
	instanceInfo.ppEnabledLayerNames = layers;
}

void VulkanInstanceFactory::buildInstance(Instance& ref)
{
	ref.construct(instanceInfo);
	if (instanceInfo.enabledLayerCount)
		ref.buildDebugCallback(logger);
}

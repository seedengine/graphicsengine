#include "Light.h"

#include <glm\gtc\matrix_transform.hpp>
#include "Device.h"
#include "GraphicsEngine.h"

#include "DescriptorSet.h"

#include "GraphicsModuleAction.h"

BaseLight::BaseLight(ComponentInterface* core, ILightRenderer* renderer, glm::vec3 c, float i) : 
	ILight(core, renderer),
	_color(c),
	_intensity(i)
{
}
const void BaseLight::writeData(DescriptorSet* setMgr, DescriptorSet* shadowSet)
{
	this->_writeData(setMgr, shadowSet);
	setMgr->writeUniformBuffer(lightData);
	shadowSet->writeUniformBuffer(shadowData);
}

void BaseLight::_updateData(GraphicsModuleAction::BaseLightData* data)
{
	_color = data->color;
	_intensity = data->intensity;
}

void BaseLight::_writeData(DescriptorSet* setMgr, DescriptorSet* shadowSet, uint32_t offset)
{
	glm::vec4 data = glm::vec4(_color, _intensity);

	setMgr->set(lightData, &data, offset, 16);

	LightTransform transform;
	transform.lightView = glm::mat4();
	transform.lightPos = glm::vec3();

	shadowSet->set(shadowData, &transform);
}

PointLight::PointLight(ComponentInterface* core, ILightRenderer* renderer, glm::vec3 c, float i, float ex, float lin, float con, glm::vec3 p) :
	BaseLight(core, renderer, c, i),
	_constant(con),
	_linear(lin),
	_exponent(ex),
	_pos(p)
{
	calcRange();
}

void PointLight::_updateData(GraphicsModuleAction::PointLightData* data)
{
	BaseLight::_updateData(&data->base);

	_pos = data->position;
	_constant = data->attenuationConstant;
	_linear = data->attenuationLinear;
	_exponent = data->attenuationExponent;
	calcRange();
}

void PointLight::_writeData(DescriptorSet* setMgr, DescriptorSet* shadowSet, uint32_t offset)
{
	BaseLight::_writeData(setMgr, shadowSet, offset);

	glm::vec4 data;
	data = glm::vec4(_pos, 1);

	offset += DirectionalLight::getLightSize();

	setMgr->set(lightData, &data, offset, 16);
	setMgr->set(lightData, &_constant, offset + 16, 4);
	setMgr->set(lightData, &_linear, offset + 20, 4);
	setMgr->set(lightData, &_exponent, offset + 24, 4);
	setMgr->set(lightData, &_range, offset + 28, 4);

	LightTransform transform;
	glm::vec3 forward; //calculate
	transform.lightView = glm::lookAt(_pos, _pos + forward, glm::vec3(0.0f, 1.0f, 0.0f));
	transform.lightPos = _pos;

	shadowSet->set(shadowData, &transform);
}

SpotLight::SpotLight(ComponentInterface* core, ILightRenderer* renderer, glm::vec3 c, float i, float ex, float lin, float con, glm::vec3 p, glm::vec3 dir, float cut) :
	PointLight(core, renderer, c, i, ex, lin, con, p),
	_direction(dir),
	_cutoff(cut)
{
}

void SpotLight::_updateData(GraphicsModuleAction::SpotLightData* data)
{
	PointLight::_updateData(&data->base);

	_cutoff =data->cutoff;
	_direction = data->direction;
}

void SpotLight::_writeData(DescriptorSet* setMgr, DescriptorSet* shadowSet, uint32_t offset)
{
	PointLight::_writeData(setMgr, shadowSet, 0);
	glm::vec4 data;
	data = glm::vec4(_direction, 1);

	setMgr->set(lightData, &data, BaseLight::getLightSize(), 16);
	setMgr->set(lightData, &_cutoff, PointLight::getLightSize(), 4);


	LightTransform transform;

	static const glm::vec3 UP = glm::vec3(0.0f, 1.0f, 0.0f);

	glm::vec3 right = glm::normalize(glm::cross(_direction, UP));
	glm::vec3 up = glm::normalize(glm::cross(right, _direction));

	glm::mat4 transMat = glm::perspective(_cutoff, 1.333f, 0.1f, 100.0f) * glm::lookAt(getPosition(), getPosition() + _direction, up);
	transform.lightView = transMat;
	transform.lightPos = getPosition();

	uint32_t size = sizeof(transform.lightView);

	setMgr->set(lightData, &transMat, PointLight::getLightSize() + 16, 64);

	shadowSet->set(shadowData, &transform);
}

DirectionalLight::DirectionalLight(ComponentInterface* core, ILightRenderer* renderer, glm::vec3 c, float i, glm::vec3 dir) :
	BaseLight(core, renderer, c, i),
	_normal(dir)
{
}

void DirectionalLight::_updateData(GraphicsModuleAction::DirectionalLightData* data)
{
	BaseLight::_updateData(&data->base);

	_normal = data->direction;
}

void DirectionalLight::_writeData(DescriptorSet* setMgr, DescriptorSet* shadowSet, uint32_t offset)
{
	BaseLight::_writeData(setMgr, shadowSet);

	glm::vec4 data;
	data = glm::vec4(_normal, 1);
	setMgr->set(lightData, &data, BaseLight::getLightSize(), 16); //needs to be converted to one write in accordance with minUniformBufferOffsetAlignment (256 for example)
}
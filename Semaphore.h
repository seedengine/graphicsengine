#include <vulkan\vulkan.h>
#include "CoreEngine/IHandle.h"

class Device;

#pragma once
class Semaphore : public IHandle<VkSemaphore>
{
public:
	Semaphore(VkPipelineStageFlags mask) : stageMask(mask) {} //Allocation
	void construct(Device& dev); //Construction

	const VkSemaphore& getHandle() const;

	inline const VkPipelineStageFlags& getStageMask() const { return stageMask; }

	void destroy(Device& dev);
	~Semaphore() {}
private:
	VkSemaphore semaphore;
	const VkPipelineStageFlags stageMask;
};


#include "CoreEngine\IMemoryManager.h"
#include "CoreEngine\IMemoryInterface.h"
#include "VulkanBuffer.h"
#include "CoreEngine\ThreadPool.h"

#include <vulkan\vulkan.h>
#include <cstdint>

struct Binding
{
	uint32_t type;
	uint32_t binding;
	uint32_t size;

	bool operator<(const Binding& other) const
	{
		return binding < other.binding;
	}
	bool operator<=(const Binding& other) const
	{
		return binding <= other.binding;
	}
	bool operator>(const Binding& other) const
	{
		return binding > other.binding;
	}
	bool operator>=(const Binding& other) const
	{
		return binding >= other.binding;
	}
};

class DescriptorSet;
class Device;

struct Descriptor
{
	VulkanBuffer* buffer;
	VkDescriptorSet descriptor;
};

struct ImageSampler
{
	Image* img;
	VkSampler* sampler;
	VkImageLayout layout =  VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
};

class DescriptorSetInterface : public IMemoryInterface<Descriptor>
{
	union DescriptorData
	{
		VkDescriptorBufferInfo uniformInfo;
		VkDescriptorImageInfo imageInfo;
	};
public:
	DescriptorSetInterface(ComponentInterface*, VkDescriptorPool*, DescriptorSet*, uint32_t setID, VkDeviceSize alignment);

	virtual MemoryRange allocate(size_t);
	virtual void freeRange(MemoryRange memory);
	virtual void set(MemoryRange, void*);
	virtual void set(MemoryRange, void*, size_t, size_t);

	virtual MemoryRange copy(MemoryRange src, MemoryRange dst); //copy from set to set
	virtual void swap(MemoryRange src, MemoryRange dst); //another copy

	void destroy();
	void update();

	void writeUniformBuffer(MemoryRange range);
	void writeImageBuffer(MemoryRange range, Image* img, VkSampler* sampler, VkImageLayout layout);

private:

	std::mutex lock;

	VkDescriptorPool* pool;
	DescriptorSet* layout;
	ComponentInterface* core;
	uint32_t setID;
	Task updateTask;

	std::vector<VkWriteDescriptorSet> pendingWrites;
	std::vector<VkCopyDescriptorSet> pendingCopies;
	std::vector<DescriptorData> setData;
	const VkDeviceSize alignment;
};

class CommandBufferRecord;

#pragma once
class DescriptorSet : public IMemoryManager<DescriptorSetInterface, Binding>, public Identifier, public IHandle<VkDescriptorSetLayout>
{
public:

	DescriptorSet(ComponentInterface* core, VkDescriptorPool* pool, uint32_t id, VkDeviceSize alignment) : IMemoryManager<DescriptorSetInterface, Binding>(0, core, pool, this, id, alignment), Identifier(id) {}

	void buildLayout(Device*);

	void registerBinding(VkDescriptorType type, uint32_t binding, uint32_t size);

	void bind(CommandBufferRecord*, MemoryRange);

	void update();


	virtual void set(MemoryRange, void*);
	virtual void set(MemoryRange range, void* data, size_t offset, size_t size);
	void writeUniformBuffer(MemoryRange range);
	void writeImageBuffer(MemoryRange range, Image* img, VkSampler* sampler, VkImageLayout layout);


	virtual const VkDescriptorSetLayout& getHandle() const
	{
		return layout;
	}

	size_t getBindingFromOffset(size_t offset);

private:
	VkDescriptorSetLayout layout;
	size_t size;
};


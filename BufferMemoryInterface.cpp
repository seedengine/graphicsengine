#include "BufferMemoryInterface.h"
#include "GraphicsExceptions.h"
#include "Device.h"
#include "GraphicsEngine.h"
#include <assert.h>


BufferMemoryInterface::BufferMemoryInterface(size_t size) : IMemoryInterface<VulkanBuffer*>(size), DATA_SIZE(size), core(nullptr), stageBuffer(nullptr)
{
}


BufferMemoryInterface::~BufferMemoryInterface()
{
}

void BufferMemoryInterface::constructInterface(ComponentInterface* core, VkBufferUsageFlags usage)
{
	this->usage = (usage | VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT);
	this->core = core;

	stageBuffer = new VulkanBuffer(core, this->usage, DATA_SIZE);

	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = ((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice().getTransferQueue().getCommandPoolHandle();
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = 1;

	if (vkAllocateCommandBuffers(((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice(), &allocInfo, &transferBuffer) != VK_SUCCESS) {
		throw VulkanConstructionError("failed to allocate command buffers!");
	}
}

void BufferMemoryInterface::destroyInterface(Device& dev)
{
	vkResetCommandBuffer(transferBuffer, 0);
	vkFreeCommandBuffers(dev, dev.getTransferQueue().getCommandPoolHandle(), 1, &transferBuffer);
	delete stageBuffer;
	stageBuffer = nullptr;
}

void BufferMemoryInterface::resetBuffer(size_t size, VkBufferUsageFlags flags)
{ 
	DATA_SIZE = size;
	this->usage = (usage | VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT);
	//reallocate the buffers. provide function in VkBuffer for that
}

MemoryRange BufferMemoryInterface::allocate(size_t)
{
	if (!core)
		throw VulkanApplicationError("Invalid Device set for Allocation");

	std::unique_lock<std::mutex> lock(allocationLock);
	allocations.push_back(new VulkanBuffer(core, usage, DATA_SIZE));
	return MemoryRange(allocations.size() - 1, 0, DATA_SIZE);
}

void BufferMemoryInterface::freeRange( MemoryRange memory)
{
	std::unique_lock<std::mutex> lock(allocationLock);
	delete allocations[memory.memIndex];
	allocations.erase(allocations.begin() + memory.memIndex);
}

void BufferMemoryInterface::set( MemoryRange range, void* data)
{
	transferLock.lock();
	stageBuffer->set(data, range.size);

	allocations[range.memIndex]->copy(range.size, range.offset, *stageBuffer, 0U);

	transferLock.unlock();
}

void BufferMemoryInterface::set(MemoryRange range, void* data, size_t offset, size_t size)
{

	transferLock.lock();
	stageBuffer->set(data, size);

	allocations[range.memIndex]->copy(size, range.offset + offset, *stageBuffer, 0U);
	transferLock.unlock();
}

MemoryRange BufferMemoryInterface::copy( MemoryRange src, MemoryRange dst)
{
	assert(src.size == dst.size);

	allocations[dst.memIndex]->copy(dst.size, dst.offset, *allocations[src.memIndex], src.offset);

	return dst;
}

void BufferMemoryInterface::swap( MemoryRange src, MemoryRange dst)
{
	assert(src.size == dst.size);

	transferLock.lock();
	VkCommandBufferBeginInfo info{};
	info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	vkBeginCommandBuffer(transferBuffer, &info);

	VkBufferCopy copyRegions[] = { 
		VkBufferCopy{ dst.offset, 0, src.size }, 
		VkBufferCopy{ src.offset, dst.offset, src.size }, 
		VkBufferCopy{ 0, src.offset, src.size } };
	vkCmdCopyBuffer(transferBuffer, getBufferHandle(dst), stageBuffer->getBufferHandle(), 1, copyRegions);
	vkCmdCopyBuffer(transferBuffer, getBufferHandle(src), getBufferHandle(dst), 1, &copyRegions[1]);
	vkCmdCopyBuffer(transferBuffer, stageBuffer->getBufferHandle(), getBufferHandle(src), 1, &copyRegions[2]);

	vkEndCommandBuffer(transferBuffer);

	((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice().getTransferQueue().submitCommandBuffer(transferBuffer);
	((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice().getTransferQueue().flush(nullptr, nullptr);
	vkResetCommandBuffer(transferBuffer, 0);
	transferLock.unlock();
}

const VkDeviceMemory& BufferMemoryInterface::getMemoryHandle(MemoryRange range) const
{
	return (*((IMemoryInterface<VulkanBuffer*>*)this)->getAllocation(range.memIndex))->getMemoryHandle();
}

const VkBuffer& BufferMemoryInterface::getBufferHandle(MemoryRange range) const
{
	return (*((IMemoryInterface<VulkanBuffer*>*)this)->getAllocation(range.memIndex))->getBufferHandle();
}

VkBuffer BufferMemoryInterface::construct(Device& dev)
{
	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = DATA_SIZE;
	bufferInfo.usage = usage;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	VkBuffer buf = 0;

	if (vkCreateBuffer(dev.getHandle(), &bufferInfo, VK_NULL_HANDLE, &buf) != VK_SUCCESS) {
		throw std::runtime_error("failed to create buffer!");
	}

	return buf;
}

VkMemoryRequirements BufferMemoryInterface::getRequirements(Device& dev, VkBuffer* buffer)
{
	VkMemoryRequirements memRequirements {};
	vkGetBufferMemoryRequirements(dev.getHandle(), *buffer, &memRequirements);
	return memRequirements;
}

uint32_t BufferMemoryInterface::getMemoryType(Device& dev, uint32_t typeFilter, VkMemoryPropertyFlags properties)
{
	VkPhysicalDeviceMemoryProperties memProperties;
	vkGetPhysicalDeviceMemoryProperties(dev.getPhysicalDevice()->getHandle(), &memProperties);
	for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++)
		if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties)
			return i;
	
	throw VulkanApplicationError("failed to find suitable memory type!");
}

VkDeviceMemory BufferMemoryInterface::allocateVisible(Device& dev, VkMemoryRequirements req)
{
	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = req.size;
	allocInfo.memoryTypeIndex = getMemoryType( dev, req.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

	VkDeviceMemory mem;

	if (vkAllocateMemory(dev.getHandle(), &allocInfo, VK_NULL_HANDLE, &mem) != VK_SUCCESS)
		throw VulkanConstructionError("failed to allocate vertex buffer memory!");

	return mem;
}
VkDeviceMemory BufferMemoryInterface::allocate(Device& dev, VkMemoryRequirements req)
{
	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = req.size;
	allocInfo.memoryTypeIndex = getMemoryType( dev, req.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	VkDeviceMemory mem;

	if (vkAllocateMemory(dev.getHandle(), &allocInfo, VK_NULL_HANDLE, &mem) != VK_SUCCESS)
		throw VulkanConstructionError("failed to allocate vertex buffer memory!");

	return mem;
}

void BufferMemoryInterface::deallocate(Device& dev, VkDeviceMemory mem)
{
	vkFreeMemory(dev.getHandle(), mem, VK_NULL_HANDLE);
}
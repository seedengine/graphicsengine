#include "VulkanDeviceFactory.h"
#include "Device.h"
#include "Instance.h"
#include "VulkanGraphicsEvents.h"
#include "CoreEngine/Event.hpp"

#include "CoreEngine/Log.h"
#include "CoreEngine/StaticCoreEngine.h"

#include <iostream>

const float VulkanDeviceFactory::defaultPriority = 1.0f;

VulkanDeviceFactory::VulkanDeviceFactory(ComponentInterface* core) : deviceFeature{}, featureExtInfo{}, deviceInfo{}, logger(core->getLog()), core(core)
{
	deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	//deviceInfo.pNext = nullptr;
	deviceInfo.pNext = &featureExtInfo;
	deviceInfo.enabledLayerCount = 0;
	deviceInfo.enabledExtensionCount = 0;
	deviceInfo.pEnabledFeatures = NULL;

	featureExtInfo.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
	featureExtInfo.pNext = &extInfo;


	VulkanFeatureSetupEvent evt;
	evt.setFeatures(&featureExtInfo.features);
	core->getEventBus()->invokeEventSynchonous(core, evt);
}


VulkanDeviceFactory::~VulkanDeviceFactory()
{
	availableExtensions.clear();
	availableLayers.clear();
}

void VulkanDeviceFactory::selectPhysicalDevice()
{
	dev = PhysicalDevice::getBestFreeDevice();

	VkPhysicalDeviceProperties devProp;
	vkGetPhysicalDeviceProperties(dev->getHandle(), &devProp);

	//needs to be fixed for different vendors
	int majorVersion = (devProp.driverVersion >> 22);
	int minorVersion = (devProp.driverVersion & 0x003FF000) >> 14;
	int patch = devProp.driverVersion & 0xFFF;
	std::string versionString;
	versionString += "Using Device ";
	versionString += devProp.deviceName;
	versionString += " with Driver Version ";
	versionString += std::to_string(majorVersion);
	if (minorVersion > 0 || patch > 0)
	{
		versionString += ".";
		versionString += std::to_string(minorVersion)
;
	}
	if (patch > 0)
	{
		versionString += ".";
		versionString += std::to_string(patch);
	}

	majorVersion = (devProp.apiVersion >> 22);
	minorVersion = (devProp.apiVersion & 0x003FF000) >> 12;
	patch = devProp.apiVersion & 0xFFF;
	std::string apiString;
	apiString += "Operating on API Version ";
	apiString += std::to_string(majorVersion);
	if (minorVersion > 0 || patch > 0)
	{
		apiString += ".";
		apiString += std::to_string(minorVersion);
	}
	if (patch > 0)
	{
		apiString += ".";
		apiString += std::to_string(patch);
	}

	core->getLog()->writeInfo(versionString);
	core->getLog()->writeInfo(apiString);

	//Select Queues
	//Can be optimized for vendors
	graphicsQueueObj = dev->getPresentationQueue();
	computeQueueObj = dev->findFirstQueueFamily(VK_QUEUE_COMPUTE_BIT);
	transferQueueObj = dev->findFirstQueueFamily(VK_QUEUE_TRANSFER_BIT);

	std::set<unsigned> unifier = { graphicsQueueObj.getID(),  computeQueueObj.getID(),  transferQueueObj.getID() };
	queueInfo.resize(unifier.size());
	unsigned ctr = 0;
	for (auto i = unifier.begin(); i != unifier.end(); i++) {
		VkDeviceQueueCreateInfo& queueInfoRef = queueInfo[ctr];
		queueInfoRef = {};
		ctr++;
		queueInfoRef.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueInfoRef.queueFamilyIndex = *i;
		queueInfoRef.queueCount = 1;
		queueInfoRef.pQueuePriorities = &defaultPriority;
	}

	deviceInfo.pQueueCreateInfos = queueInfo.data();
	deviceInfo.queueCreateInfoCount = (uint32_t)queueInfo.size();


	uint32_t extensionCount;
	vkEnumerateDeviceExtensionProperties( dev->getHandle(), NULL, &extensionCount, VK_NULL_HANDLE);
	if (core->getDebugState())
		logger->writeInfo("========================= Scanning for Device Extensions ==========================");
	VkExtensionProperties* extension = (VkExtensionProperties*)malloc(extensionCount * sizeof(VkExtensionProperties));
	vkEnumerateDeviceExtensionProperties(dev->getHandle(), NULL, &extensionCount, extension);
	for (VkExtensionProperties* it = extension; it < extension + extensionCount; it++) {
		if (core->getDebugState())
			logger->writeInfo((std::string("Found Device Extension: ") + it->extensionName + " ver.: " + std::to_string(it->specVersion)).c_str());
		availableExtensions.insert(std::string(it->extensionName));
	}
	free(extension);
	if (core->getDebugState())
	{
		logger->writeInfo("========================= Scan for Device Extensions completed ==========================");
		logger->writeInfo("====================== Scanning for Device Validation Layers ======================");
	}
	uint32_t layerCount;
	vkEnumerateDeviceLayerProperties( dev->getHandle(), &layerCount, VK_NULL_HANDLE);
	VkLayerProperties* layers = (VkLayerProperties*)malloc(layerCount * sizeof(VkLayerProperties));
	vkEnumerateDeviceLayerProperties(dev->getHandle(), &layerCount, layers);
	for (VkLayerProperties* it = layers; it < layers + layerCount; it++) {
		if (core->getDebugState())
			logger->writeInfo( (std::string("Found Validation Layer: ") + it->layerName + " ver.: " +std::to_string(it->implementationVersion)).c_str());
		availableLayers.insert(it->layerName);
	}
	free(layers);
	if (core->getDebugState())
		logger->writeInfo("========================= Scan for Device Layers completed ==========================");




	extInfo.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES_EXT;
	extInfo.pNext = nullptr;
	extInfo.descriptorBindingUpdateUnusedWhilePending = VK_TRUE;




}

void VulkanDeviceFactory::setActivationLayers(const char** layers, size_t count)
{
	deviceInfo.enabledLayerCount = (uint32_t)count;
	deviceInfo.ppEnabledLayerNames = layers;
}

bool VulkanDeviceFactory::validateExtensionList(const char** extensions, size_t count) const
{
	for (unsigned i = 0; i < count; i++)
		if (!isExtensionAvailable(extensions[i]))
			return false;
	return true;
}

bool VulkanDeviceFactory::isExtensionAvailable(const char *layer) const
{
	return availableExtensions.find(layer) != availableExtensions.end();
}

void VulkanDeviceFactory::setDeviceExtensions(const char** extensions, size_t count)
{
	deviceInfo.enabledExtensionCount = (uint32_t)count;
	deviceInfo.ppEnabledExtensionNames = extensions;
}

void VulkanDeviceFactory::buildDevice(Device& ref)
{
	ref.construct(core, std::move(dev), deviceInfo, graphicsQueueObj.getID(), computeQueueObj.getID(), transferQueueObj.getID());
}

void VulkanDeviceFactory::readPhysicalDevices(Instance& instance, Surface& context)
{
	PhysicalDevice::scanDevices(instance, context);
	selectPhysicalDevice();
}

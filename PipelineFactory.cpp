#include "PipelineFactory.h"

#include "ShaderModule.h"
#include "Pipeline.h"
#include "RenderPass.h"
#include "Surface.h"
#include "Device.h"
#include "GraphicsExceptions.h"
#include "PipelineLayout.h"

#include <vector>
#include <mutex>

PipelineFactory::PipelineFactory() :
	vertexStructure{},
	inputAssembly{},
	pipelineViewport{},
	rasterizationState{},
	multisampleStage{},
	depthStencilState{},
	colorBlending{},
	pipelineDynamics{},
	info{}
{
	vertexStructure.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	tesselation.sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
	pipelineViewport.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	rasterizationState.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	multisampleStage.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	depthStencilState.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	pipelineDynamics.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;

	info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;

	multisampleStage.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	pipelineCount = 0;

	setLineWidth(1.0f);
	useDefaultInputAssembly();
	//CONST
	setFragmentCulling(VK_CULL_MODE_BACK_BIT, VK_FRONT_FACE_COUNTER_CLOCKWISE);
	//CONST (might need change in the future tho)
	disableDepthBias();
	disableDepthClamp();
	//CONST
	setShadingSampleCount(1.0f);
	//Viewport CONST
	std::vector<VkDynamicState> states({
		VK_DYNAMIC_STATE_VIEWPORT,
		VK_DYNAMIC_STATE_SCISSOR
		});
	setColorBlendingOperation(VK_LOGIC_OP_CLEAR);
	setPipelineDynamicStates(states);
	setBlendConstants(0.0f, 0.0f, 0.0f, 0.0f);

	depthStencilState.pNext = nullptr;
	depthStencilState.flags = 0;
	depthStencilState.depthBoundsTestEnable = VK_FALSE;
}

void PipelineFactory::construct(Device& dev)
{
	features = dev.getPhysicalDevice()->getDeviceFeatures();

	VkPipelineCacheCreateInfo cacheInfo{};
	cacheInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
	cacheInfo.initialDataSize = 0;
	cacheInfo.pInitialData = nullptr;

	if (vkCreatePipelineCache(dev.getHandle(), &cacheInfo, VK_NULL_HANDLE, &cache) != VK_SUCCESS)
		throw VulkanConstructionError("failed to create pipeline!");

}

PipelineFactory& PipelineFactory::setVertexInputStructure(VertexStructure vStruct)
{
	structure = vStruct;
	vertexStructure = structure.getVertexStructure();
	return *this;
}

PipelineFactory& PipelineFactory::useDefaultEmptyInput()
{
	setVertexInputStructure(
		VertexStructure{
		std::vector<VkVertexInputAttributeDescription>(),
		std::vector<VkVertexInputBindingDescription>()
	}
	);
	return *this;
}

PipelineFactory& PipelineFactory::useDefaultVertexInput3D()
{
	VkVertexInputAttributeDescription attrib[] = {
		VkVertexInputAttributeDescription{ 0, 0, VK_FORMAT_R32G32B32_SFLOAT, 0 },	//Position
		VkVertexInputAttributeDescription{ 1, 0, VK_FORMAT_R32G32_SFLOAT, 12 },		//Texture Coord
		VkVertexInputAttributeDescription{ 2, 0, VK_FORMAT_R32G32B32_SFLOAT, 20 }	//Normal Coord
	};
	setVertexInputStructure(
		VertexStructure{
			std::vector<VkVertexInputAttributeDescription>(std::begin(attrib), std::end(attrib)),
			std::vector<VkVertexInputBindingDescription>(1, 
			VkVertexInputBindingDescription{ 0, 32, VK_VERTEX_INPUT_RATE_VERTEX }
			)
		}
	);
	return *this;
}

PipelineFactory& PipelineFactory::useDefaultVertexInput2D()
{
	VkVertexInputAttributeDescription attrib[] = {
		VkVertexInputAttributeDescription{ 0, 0, VK_FORMAT_R32G32_SFLOAT, 0 },		//Position
		VkVertexInputAttributeDescription{ 1, 0, VK_FORMAT_R32G32_SFLOAT, 8 }		//Texture Coord
	};
	setVertexInputStructure(
		VertexStructure{
		std::vector<VkVertexInputAttributeDescription>(std::begin(attrib), std::end(attrib)),
		std::vector<VkVertexInputBindingDescription>(1, VkVertexInputBindingDescription{ 0, sizeof(float) * 4, VK_VERTEX_INPUT_RATE_VERTEX })
	}
	);
	return *this;
}

PipelineFactory& PipelineFactory::setInputAssemblyStructure(AssemblyStructure structure)
{
	inputAssembly = structure.getInputAssemblyState();
	return *this;
}

PipelineFactory& PipelineFactory::useDefaultInputAssembly()
{
	setInputAssemblyStructure({ VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST, VK_FALSE });
	return *this;
}

PipelineFactory& PipelineFactory::addShader(ShaderModule& shader, VkShaderStageFlagBits type)
{
	shaderStages.push_back(shader.getShaderInfo(type));
	return *this;
}

PipelineFactory& PipelineFactory::setSurfaceViewport(Surface* surface)
{
	VkExtent2D surfaceExtend = surface->getExtend();
	pipelineViewport.viewportCount = 1;
	this->viewports = std::vector<VkViewport>({
		VkViewport{ 0.0f, 0.0f, float(surfaceExtend.width), float(surfaceExtend.height), 0.0f, 1.0f }
	});
	pipelineViewport.pViewports = this->viewports.data();

	pipelineViewport.scissorCount = 1;
	this->scissors = std::vector<VkRect2D>({
		VkRect2D{ VkOffset2D{ 0, 0 }, surfaceExtend }
	});
	pipelineViewport.pScissors = this->scissors.data();
	return *this;
}

PipelineFactory& PipelineFactory::setLineWidth(float width)
{
	rasterizationState.lineWidth = width;
	return *this;
}

PipelineFactory& PipelineFactory::setFillMode(VkPolygonMode mode)
{
	rasterizationState.polygonMode = mode;
	return *this;
}

PipelineFactory& PipelineFactory::setFragmentCulling(VkCullModeFlags culling, VkFrontFace front)
{
	rasterizationState.cullMode  = culling;
	rasterizationState.frontFace = front;
	return *this;
}

PipelineFactory& PipelineFactory::enableDepthBias()
{
	rasterizationState.depthBiasEnable = VK_TRUE;
	return *this;
}

PipelineFactory& PipelineFactory::disableDepthBias()
{
	rasterizationState.depthBiasEnable = VK_FALSE;
	return *this;
}

PipelineFactory& PipelineFactory::setDepthBias( float biasConstant, float biasSlope)
{
	rasterizationState.depthBiasConstantFactor = biasConstant;
	rasterizationState.depthBiasSlopeFactor = biasSlope;
	return *this;
}

PipelineFactory& PipelineFactory::enableDepthClamp()
{
	rasterizationState.depthClampEnable = VK_TRUE;
	return *this;
}

PipelineFactory& PipelineFactory::disableDepthClamp()
{
	rasterizationState.depthClampEnable = VK_FALSE;
	return *this;
}

PipelineFactory& PipelineFactory::setDepthClamp(float depthClamp)
{
	rasterizationState.depthBiasClamp = depthClamp;
	return *this;
}

PipelineFactory& PipelineFactory::setShadingSampleCount(float count)
{
	multisampleStage.minSampleShading = count;
	return *this;
}

PipelineFactory& PipelineFactory::enableDepthCheck()
{
	depthStencilState.depthTestEnable = VK_TRUE;
	return *this;
}

PipelineFactory& PipelineFactory::disableDepthCheck()
{
	depthStencilState.depthTestEnable = VK_FALSE;
	return *this;
}

PipelineFactory& PipelineFactory::enableDepthWrite()
{
	depthStencilState.depthWriteEnable = VK_TRUE;
	return *this;
}

PipelineFactory& PipelineFactory::disableDepthWrite()
{
	depthStencilState.depthWriteEnable = VK_FALSE;
	return *this;
}

PipelineFactory& PipelineFactory::setDepthComparisonOperation(VkCompareOp operation)
{
	depthStencilState.depthCompareOp = operation;
	return *this;
}

PipelineFactory& PipelineFactory::setDepthBounds(float min, float max)
{
	if (min == max)
		depthStencilState.stencilTestEnable = VK_FALSE;
	depthStencilState.minDepthBounds = min;
	depthStencilState.maxDepthBounds = max;
	return *this;
}

PipelineFactory& PipelineFactory::enableStencil()
{
	depthStencilState.stencilTestEnable = VK_TRUE;
	return *this;
}
PipelineFactory& PipelineFactory::disableStencil()
{
	depthStencilState.stencilTestEnable = VK_FALSE;
	return *this;
}

PipelineFactory& PipelineFactory::setStencilTest(VkStencilOpState front, VkStencilOpState back)
{
	depthStencilState.front = front;
	depthStencilState.back = back;
	return *this;
}

PipelineFactory& PipelineFactory::setColorBlendingOperation(VkLogicOp op)
{
	if (op == VK_LOGIC_OP_CLEAR)
	{
		colorBlending.logicOpEnable = VK_FALSE;
	}
	else
	{
		colorBlending.logicOpEnable = VK_TRUE;
		colorBlending.logicOp = op;
	}
		return *this;
}

PipelineFactory& PipelineFactory::setColorAttachments(std::vector<Compressor::ColorBlendAttachment>&& attachments)
{
	colorBlending.attachmentCount = uint32_t(attachments.size());
	uint32_t pos = blendingAttachments.size();
	std::transform(attachments.begin(), attachments.end(), std::back_inserter(blendingAttachments), BlendAttachmentConverter());
	colorBlending.pAttachments = &blendingAttachments.data()[pos];
	return *this;
}

PipelineFactory& PipelineFactory::setBlendConstants(float r, float g, float b, float a)
{
	colorBlending.blendConstants[0] = r;
	colorBlending.blendConstants[1] = g;
	colorBlending.blendConstants[2] = b;
	colorBlending.blendConstants[3] = a;
	return *this;
}

PipelineFactory& PipelineFactory::setPipelineDynamicStates(std::vector<VkDynamicState>& states)
{
	pipelineDynamics.dynamicStateCount = uint32_t(states.size());
	dynamicStates = states;
	pipelineDynamics.pDynamicStates = dynamicStates.data();
	return *this;
}

PipelineFactory& PipelineFactory::setPipelineLayout(PipelineLayout&& layout)
{
	this->layout = std::move(layout);
	return *this;
}

PipelineFactory& PipelineFactory::setSubpass( uint32_t subPass)
{
	info.subpass = subPass;
	return *this;
}

PipelineFactory& PipelineFactory::build(Device& dev, IRenderSystem* renderPass, IPipeline* pipeline, uint32_t& pipelineID)
{
	info.renderPass = *(RenderPass*)renderPass;

	info.stageCount = uint32_t(shaderStages.size());
	info.pStages = shaderStages.data();

	info.pVertexInputState = &vertexStructure;
	info.pInputAssemblyState = &inputAssembly;
	info.pTessellationState = &tesselation;
	info.pViewportState = &pipelineViewport;
	info.pRasterizationState = &rasterizationState;
	info.pMultisampleState = &multisampleStage;
	info.pDepthStencilState = &depthStencilState;
	info.pColorBlendState = &colorBlending;
	info.pDynamicState = &pipelineDynamics;

	((Pipeline*)pipeline)->construct( dev, cache, info, std::move(layout));
	return *this;
}

PipelineFactory& PipelineFactory::reset()
{
	viewports.clear();
	scissors.clear();
	shaderStages.clear();
	pushConstants.clear();
	return *this;
}

VkGraphicsPipelineCreateInfo PipelineFactory::generateInfoFromDataFile(Compressor::PipelineData data)
{
	useDefaultInputAssembly();

	if (data.dimension == 2)
	{
		useDefaultVertexInput2D();
	}
	else if (data.dimension == 3)
	{
		useDefaultVertexInput3D();
	}

	if (data.featureData & 1)
	{
		//later
	}
	if (data.featureData & 2)
	{
		setDepthBias(data.biasMin, data.biasMax);
	}
	if (data.featureData & 4)
	{
		setDepthClamp(data.clampVal);
	}
	if (data.featureData & 8)
	{
		enableDepthCheck();
		setDepthComparisonOperation(data.comp);
	}
	else
		disableDepthCheck();
	if (data.featureData & 16)
	{
		enableDepthWrite();
		setDepthBounds(data.boundsMin, data.boundsMax);
	}
	else
		disableDepthWrite();


	if (features.fillModeNonSolid == VK_TRUE)
		switch (data.polygonDisplay)
		{
		case Compressor::PolygonDisplay::POINTS:
			setFillMode(VK_POLYGON_MODE_POINT);
			break;
		case Compressor::PolygonDisplay::LINE:
			setFillMode(VK_POLYGON_MODE_LINE);
			break;
		case Compressor::PolygonDisplay::FILL:
			setFillMode(VK_POLYGON_MODE_FILL);
			break;
		default:
			setFillMode(VK_POLYGON_MODE_FILL);
			break;
		}
	else
		setFillMode(VK_POLYGON_MODE_FILL);

	if (features.wideLines == VK_TRUE)
		setLineWidth(data.lineWidth);
	else
		setLineWidth(1.0f);

	setColorBlendingOperation(VK_LOGIC_OP_CLEAR);

	std::vector<Compressor::ColorBlendAttachment> attachments = data.attachments;
	setColorAttachments(std::move(attachments));


	VkCullModeFlags cullMode = VK_CULL_MODE_NONE;
	if (data.featureData & 32)
	{
		cullMode |= VK_CULL_MODE_BACK_BIT;
	}
	if (data.featureData & 64)
	{
		cullMode |= VK_CULL_MODE_FRONT_BIT;
	}
	if (data.featureData & 128)
	{
		setFragmentCulling(cullMode, VK_FRONT_FACE_COUNTER_CLOCKWISE);
	}
	else
	{
		setFragmentCulling(cullMode, VK_FRONT_FACE_CLOCKWISE);
	}

	info.pVertexInputState = &vertexStructure;
	info.pInputAssemblyState = &inputAssembly;
	info.pTessellationState = &tesselation;
	info.pViewportState = &pipelineViewport;
	info.pRasterizationState = &rasterizationState;
	info.pMultisampleState = &multisampleStage;
	info.pDepthStencilState = &depthStencilState;
	info.pColorBlendState = &colorBlending;
	info.pDynamicState = &pipelineDynamics;

	return info;
}

void PipelineFactory::destroy(Device& dev)
{
	vkDestroyPipelineCache(dev.getHandle(), cache, VK_NULL_HANDLE);
}

PipelineFactory::~PipelineFactory()
{
}

#include "SwapChain.h"
#include "Device.h"
#include "GraphicsExceptions.h"
#include "Image.h"
#include "Pipeline.h"
#include "Surface.h"
#include "Semaphore.h"

#include <algorithm>


SwapChain::SwapChain()
{
}

void SwapChain::construct(Device & dev, Surface& window)
{
	const SurfacePresentationDetails& details = window.getSurfaceProperties();

	swapChainFormat = window.getImageViewFormat().format;
	swapChainExtent = window.getExtend();

	VkPresentModeKHR bestMode = VK_PRESENT_MODE_FIFO_KHR;

	for (const auto& availablePresentMode : details.presentModes) {
		if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
			bestMode = availablePresentMode;
			break;
		}
		else if (availablePresentMode == VK_PRESENT_MODE_IMMEDIATE_KHR) {
			bestMode = availablePresentMode;
		}
	}

	uint32_t imgCnt = details.capabilities.minImageCount + 1;
	if (details.capabilities.maxImageCount > 0 && imgCnt > details.capabilities.maxImageCount) {
		imgCnt = details.capabilities.maxImageCount;
	}

	VkSwapchainCreateInfoKHR createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.surface = window.getHandle();
	createInfo.minImageCount = imgCnt;
	createInfo.imageFormat = swapChainFormat;
	createInfo.imageColorSpace = window.getImageViewFormat().colorSpace;
	createInfo.imageExtent = swapChainExtent;
	createInfo.imageArrayLayers = 1;
	createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

	uint32_t graphicsQueue = dev.getGraphicsQueue().getFamilyID();
	createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	createInfo.queueFamilyIndexCount = 1;
	createInfo.pQueueFamilyIndices = &graphicsQueue;

	createInfo.preTransform = details.capabilities.currentTransform;
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	createInfo.presentMode = bestMode;
	createInfo.clipped = VK_TRUE;
	createInfo.oldSwapchain = VK_NULL_HANDLE; //create one depth stack/swap space to reuse data

	if (vkCreateSwapchainKHR(dev.getHandle(), &createInfo, VK_NULL_HANDLE, &swapChain) != VK_SUCCESS)
		throw VulkanConstructionError("failed to create swap chain!");

	vkGetSwapchainImagesKHR(dev.getHandle(), swapChain, &imgCnt, VK_NULL_HANDLE);
	VkImage* images = (VkImage*)malloc(imgCnt * sizeof(VkImage));
	vkGetSwapchainImagesKHR(dev.getHandle(), swapChain, &imgCnt, images);
	ISwappable<Image>::resize(imgCnt);

	ISwappable<Image>::setCurrPos(0); //reset index

	for (size_t i = 0; i < imgCnt; i++)
	{
		ISwappable<Image>::getCurrentRef() = Image(images[i], swapChainExtent.width, swapChainExtent.height, swapChainFormat);
		ISwappable<Image>::getCurrentRef().init(dev);
		ISwappable<Image>::next();
	}

	free(images);
}

void SwapChain::rebuildSwapChain(Device & dev, Surface & window)
{
	ISwappable<Image>::execute<Device&>(std::function<void(Image&, Device&)>(&Image::destroy), dev);
	const SurfacePresentationDetails& details = window.getSurfaceProperties();
	VkSwapchainKHR oldSwapChain = swapChain;
	swapChain = 0;

	swapChainFormat = window.getImageViewFormat().format;
	swapChainExtent = window.getExtend();

	VkPresentModeKHR bestMode = VK_PRESENT_MODE_FIFO_KHR;

	for (const auto& availablePresentMode : details.presentModes) {
		if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
			bestMode = availablePresentMode;
			break;
		}
		else if (availablePresentMode == VK_PRESENT_MODE_IMMEDIATE_KHR) {
			bestMode = availablePresentMode;
		}
	}

	uint32_t imgCnt = details.capabilities.minImageCount + 1;
	if (details.capabilities.maxImageCount > 0 && imgCnt > details.capabilities.maxImageCount) {
		imgCnt = details.capabilities.maxImageCount;
	}

	VkSwapchainCreateInfoKHR createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.surface = window.getHandle();
	createInfo.minImageCount = imgCnt;
	createInfo.imageFormat = swapChainFormat;
	createInfo.imageColorSpace = window.getImageViewFormat().colorSpace;
	createInfo.imageExtent = swapChainExtent;
	createInfo.imageArrayLayers = 1;
	createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

	uint32_t graphicsQueue = dev.getGraphicsQueue().getFamilyID();
	createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	createInfo.queueFamilyIndexCount = 1;
	createInfo.pQueueFamilyIndices = &graphicsQueue;

	createInfo.preTransform = details.capabilities.currentTransform;
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	createInfo.presentMode = bestMode;
	createInfo.clipped = VK_TRUE;
	createInfo.oldSwapchain = oldSwapChain;

	if (vkCreateSwapchainKHR(dev.getHandle(), &createInfo, VK_NULL_HANDLE, &swapChain) != VK_SUCCESS)
		throw VulkanConstructionError("failed to create swap chain!");

	vkGetSwapchainImagesKHR(dev.getHandle(), swapChain, &imgCnt, VK_NULL_HANDLE);
	VkImage* images = (VkImage*)malloc(imgCnt * sizeof(VkImage));
	vkGetSwapchainImagesKHR(dev.getHandle(), swapChain, &imgCnt, images);
	ISwappable<Image>::resize(imgCnt);
	ISwappable<Image>::setCurrPos(0); //reset index

	for (size_t i = 0; i < imgCnt; i++)
	{
		ISwappable<Image>::getCurrentRef() = Image(images[i], swapChainExtent.width, swapChainExtent.height, swapChainFormat);
		ISwappable<Image>::next();
	}
	free(images);
	vkDestroySwapchainKHR(dev.getHandle(), oldSwapChain, VK_NULL_HANDLE);
}

Attachment SwapChain::getFramebufferAttachment(uint32_t framebufferCnt) const
{
	VkAttachmentDescription attachmentInfo{};
	attachmentInfo.format = swapChainFormat;
	attachmentInfo.samples = VK_SAMPLE_COUNT_1_BIT;
	attachmentInfo.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	attachmentInfo.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	attachmentInfo.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachmentInfo.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachmentInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	attachmentInfo.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	return Attachment(attachmentInfo, VkClearValue{ 0.0f, 0.0f, 0.0f, 1.0f }, getList()[std::min(getSize() - 1, framebufferCnt)].getImageView(), VkExtent3D{ swapChainExtent.width, swapChainExtent.height, 1 });
}



SwapChain::~SwapChain()
{
}

const VkSwapchainKHR& SwapChain::getHandle() const
{
	return swapChain;
}

uint32_t SwapChain::getNextImage(Device& dev, Semaphore& semaphore)
{
	uint32_t imageIndex;
	if ( VK_SUCCESS != vkAcquireNextImageKHR(dev.getHandle(), swapChain, std::numeric_limits<uint64_t>::max(), semaphore.getHandle(), VK_NULL_HANDLE, &imageIndex))
		throw VulkanApplicationError("Failed to get Image ID.");
	ISwappable<Image>::setCurrPos(imageIndex);
	
	return imageIndex;
}

void SwapChain::destroy(Device& dev)
{
	if (swapChain == VK_NULL_HANDLE && getSize() == 0)
		return;



	ISwappable<Image>::execute([this, &dev](Image& buf, size_t id) {
		buf.destroy(dev);
	});

	vkDestroySwapchainKHR(dev.getHandle(), swapChain, VK_NULL_HANDLE);
}

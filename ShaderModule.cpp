#include "ShaderModule.h"
#include "GraphicsExceptions.h"
#include "GraphicsEvents.h"
#include "GraphicsEngine.h"

ShaderModule::ShaderModule(ComponentInterface* core) : EventHandler(core), code(nullptr)
{
	core->getEventBus()->registerHandler(this, VulkanDeviceConstructionEvent::getClassID());
	core->getEventBus()->registerHandler(this, VulkanDeviceDestructionEvent::getClassID());
}

void ShaderModule::operator()(ComponentInterface* core, const EventBase* evt)
{
	if (evt->is<VulkanDeviceConstructionEvent>())
	{
		if (code == nullptr || size == 0)
			throw FrameworkException("Shader has not been initialized with code.");
		VkShaderModuleCreateInfo moduleInfo{};
		moduleInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		moduleInfo.codeSize = size;
		moduleInfo.pCode = reinterpret_cast<const uint32_t*>(code);
		if (vkCreateShaderModule(((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice(), &moduleInfo, VK_NULL_HANDLE, &shader) != VK_SUCCESS)
			throw VulkanConstructionError("Failed to build Shader Module");
	}
	else if (evt->is<VulkanDeviceDestructionEvent>())
	{
		VulkanDeviceDestructionEvent* data = (VulkanDeviceDestructionEvent*)evt;
		vkDestroyShaderModule(*data->getDeviceHandle(), shader, VK_NULL_HANDLE);
	}
}

VkPipelineShaderStageCreateInfo ShaderModule::getShaderInfo(VkShaderStageFlagBits type)
{
	if (shader == VK_NULL_HANDLE)
		throw VulkanApplicationError("Shader has not been constructed yet");

	VkPipelineShaderStageCreateInfo info{};
	info.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	info.pSpecializationInfo = nullptr;
	info.module = shader;
	info.stage = type;
	info.pName = "main";
	info.flags = 0;

	return info;
}

ShaderModule::~ShaderModule()
{
	
	if (code != nullptr)
		free(code);
}

void ShaderModule::setShaderCode(uint32_t * code, uint32_t size)
{
	if (this->code != nullptr)
		free(this->code);
	this->code = (uint32_t*)malloc(size);
	memcpy(this->code, code, size);
	this->size = size;
}

#include <vulkan\vulkan.h>
#include <glm\glm.hpp>
#include <vector>

#include "UtilStruct.h"
#include "CoreEngine\IObjectComponent.h"
#include "BufferMemoryInterface.h"
#include "CoreEngine\Transformation.h"
#include "CoreEngine\BlockMemoryManager.h"

class Device;
class SwapChainImage;
class IRenderContext;

#pragma once
class Mesh : public IObjectComponent
{
private:
	struct MeshHeader
	{
		uint32_t vertices; /*first two bits are for structure data*/
		uint32_t faceCount;
	};


	class Vertices {
	public:
		Vertices(uint8_t size = 8);

		uint8_t getVertexSize();
		void setVertexSize(uint8_t);

		size_t size();
		size_t getByteSize();

		void push(float* v, size_t length);
		void push(const Vertices& other);
		void erase(size_t pos);
		void erase(size_t start, size_t end);
		void pop();
		void pop(size_t amount);
		void clear();

		float operator[](size_t pos);
		float* get();
	private:
		void shrink(size_t);
		void extend(size_t);
		
		uint8_t vertexSize;
		std::vector<float> values;
	};
public:
	Mesh(ComponentInterface*, uint8_t, glm::vec3 offset);
	Mesh(ComponentInterface*, std::string path, glm::vec3 offset);
	Mesh(ComponentInterface*, const char* path, glm::vec3 offset);

	VkBufferUsageFlags getBufferRequirements();
	size_t getDataSize();

	virtual void operator()(ComponentInterface*, const EventBase* handle);

	virtual inline void listRequirements(std::set<uint64_t>& req);

	~Mesh();
private:
	void bindBuffers(BlockMemoryManager<BufferMemoryInterface>* buf, IRenderContext* image);

	glm::vec3 offset;

	Vertices vertices;
	std::vector<uint32_t> indices;
	MemoryRange dataStorage;
	std::string path;
};

#include "Pipeline.h"
#include "Device.h"
#include "GraphicsExceptions.h"
#include "RenderPass.h"
#include "GraphicsEngine.h"


Pipeline::Pipeline(ComponentInterface* core) : EventHandler(core)
{
	core->getEventBus()->registerHandler(this, LoadEvent::getClassID());
	core->getEventBus()->registerHandler(this, VulkanDeviceDestructionEvent::getClassID());
}

Pipeline::Pipeline(ComponentInterface* core, std::string name) : EventHandler(core), file(name)
{
	core->getEventBus()->registerHandler(this, LoadEvent::getClassID());
	core->getEventBus()->registerHandler(this, VulkanDeviceDestructionEvent::getClassID());
}

void Pipeline::construct(Device& dev, VkPipelineCache & cache, VkGraphicsPipelineCreateInfo info, PipelineLayout&& layout)
{
	this->layout = std::move(layout);
	info.layout = this->layout.getHandle();
	if (vkCreateGraphicsPipelines(dev.getHandle(), cache, 1, &info, VK_NULL_HANDLE, &pipeline) != VK_SUCCESS)
		throw VulkanConstructionError("failed to create pipeline!");
}

void Pipeline::construct(Device& dev, IRenderSystem* pass, uint32_t renderStepID, Surface* surface, PipelineLayout&& layout, std::function<void(PipelineFactory&)> transform)
{
	PipelineFactory& factory = ((RenderPass*)pass)->getFactory();

	factory.reset();
	factory.setSurfaceViewport(surface);

	transform(factory);

	VkGraphicsPipelineCreateInfo info = factory.generateInfoFromDataFile(data);

	info.renderPass = *(RenderPass*)pass;
	info.subpass = renderStepID;
	pass->getRenderStep(renderStepID)->registerPipeline(this);

	std::vector<VkPipelineShaderStageCreateInfo> moduleList;
	for (auto p : modules)
		moduleList.push_back(p.second->getShaderInfo(getTypeFromName(p.first)));
	info.stageCount = uint32_t(moduleList.size());
	info.pStages = moduleList.data();

	this->layout = std::move(layout);
	info.layout = this->layout.getHandle();
	

	transform(factory);
	
	if (vkCreateGraphicsPipelines(dev.getHandle(), factory.getPipelineCache(), 1, &info, VK_NULL_HANDLE, &pipeline) != VK_SUCCESS)
		throw VulkanConstructionError("failed to create pipeline!");
}

const VkPipeline& Pipeline::getHandle() const
{
	return pipeline;
}

void Pipeline::bind(IRenderContext* image)
{
	vkCmdBindPipeline(((CommandBufferRecord*)image->getContext())->getCommandBuffer(), VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
	((CommandBufferRecord*)image->getContext())->setCurrentPipeline(this);
}

void Pipeline::operator()(ComponentInterface* core, const EventBase* evt)
{
	if (evt->is<LoadEvent>())
	{
		load(core);
	}
	else if (evt->is<VulkanDeviceDestructionEvent>())
	{
		VulkanDeviceDestructionEvent* evtHandle = (VulkanDeviceDestructionEvent*)evt;
		vkDestroyPipeline(*evtHandle->getDeviceHandle(), pipeline, VK_NULL_HANDLE);
		layout.destroy(*evtHandle->getDeviceHandle());
	}

}

void Pipeline::load(ComponentInterface* core)
{
	if (file.empty())
		return; //not loading when programatically created

	std::ifstream dataFile = std::ifstream(file, std::ios::in | std::ios::binary);

	dataFile.read((char*)&data.dimension, 1);
	dataFile.read((char*)&data.shaderIDs, 1);
	dataFile.read((char*)&data.lineWidth, 1);
	dataFile.read((char*)&data.polygonDisplay, 1);
	dataFile.read((char*)&data.samples, 1);
	dataFile.read((char*)&data.featureData, 1);

	//Stencil
	if (data.featureData & 1)
	{
		dataFile.read((char*)&data.front, sizeof(Compressor::StencilData));
		dataFile.read((char*)&data.back, sizeof(Compressor::StencilData));
	}
	//depthBias
	if (data.featureData & 2)
	{
		dataFile.read((char*)&data.biasMin, 4);
		dataFile.read((char*)&data.biasMax, 4);
	}
	//depthClamp
	if (data.featureData & 4)
	{
		dataFile.read((char*)&data.clampVal, 4);
	}
	//depthCheck
	if (data.featureData & 8)
	{
		dataFile.read((char*)&data.comp, 4);
	}
	//DepthWrite
	if (data.featureData & 16)
	{
		dataFile.read((char*)&data.boundsMin, 4);
		dataFile.read((char*)&data.boundsMax, 4);
	}


	uint32_t tmp = 0;
	dataFile.read((char*)&tmp, 4);
	data.attachments.resize(tmp);
	dataFile.read((char*)data.attachments.data(), sizeof(Compressor::ColorBlendAttachment) * tmp);

	if (data.shaderIDs & 1)
	{
		modules["vertex"] = new ShaderModule(core);
		uint32_t size = 0;
		dataFile.read((char*)&size, 4);
		uint32_t* data;
		data = (uint32_t*)malloc(size);
		dataFile.read((char*)data, size);
		modules["vertex"]->setShaderCode(data, size);
	}

	if (data.shaderIDs & 2)
	{
		//Tesselation Evaluation
		modules["tesEval"] = new ShaderModule(core);
		uint32_t size = 0;
		dataFile.read((char*)&size, 4);
		uint32_t* data;
		data = (uint32_t*)malloc(size);
		dataFile.read((char*)data, size);
		modules["tesEval"]->setShaderCode(data, size);
	}

	if (data.shaderIDs & 4)
	{
		//Tesselation Control
		modules["tessCon"] = new ShaderModule(core);
		uint32_t size = 0;
		dataFile.read((char*)&size, 4);
		uint32_t* data;
		data = (uint32_t*)malloc(size);
		dataFile.read((char*)data, size);
		modules["tessCon"]->setShaderCode(data, size);
	}

	if (data.shaderIDs & 8)
	{
		//Geometry Shader
		modules["geometry"] = new ShaderModule(core);
		uint32_t size = 0;
		dataFile.read((char*)&size, 4);
		uint32_t* data;
		data = (uint32_t*)malloc(size);
		dataFile.read((char*)data, size);
		modules["geometry"]->setShaderCode(data, size);
	}

	if (data.shaderIDs & 16)
	{
		//Fragment Shader
		modules["fragment"] = new ShaderModule(core);
		uint32_t size = 0;
		dataFile.read((char*)&size, 4);
		uint32_t* data;
		data = (uint32_t*)malloc(size);
		dataFile.read((char*)data, size);
		modules["fragment"]->setShaderCode(data, size);
	}

	dataFile.close();


	//needs to be passed to factory and used to actually create object
}

VkShaderStageFlagBits Pipeline::getTypeFromName(std::string str)
{
	if (str == "vertex")
		return VK_SHADER_STAGE_VERTEX_BIT;
	else if(str == "tesEval")
		return VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
	else if(str == "tessCon")
		return VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
	else if(str == "geometry")
		return VK_SHADER_STAGE_GEOMETRY_BIT;
	else if(str == "fragment")
		return VK_SHADER_STAGE_FRAGMENT_BIT;
	else
		return VK_SHADER_STAGE_VERTEX_BIT;
}

Pipeline::~Pipeline()
{
	for (auto pair : modules)
	{
		delete pair.second;
	}
}

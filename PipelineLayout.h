#include <vulkan\vulkan.h>
#include <vector>
#include <set>

#include "CoreEngine/IHandle.h"
#include "UtilStruct.h"

class Device;
class ComponentInterface;

#pragma once
class PipelineLayout : 
	public IHandle<VkPipelineLayout>
{
public:
	PipelineLayout();

	void construct(ComponentInterface*, Device&, std::set<uint32_t> sets = std::set<uint32_t>() , std::vector<VkPushConstantRange> pConstants = std::vector<VkPushConstantRange>());

	const VkPipelineLayout& getHandle() const;

	bool hasSet(uint32_t id);

	void destroy(Device&);

	~PipelineLayout();
private:
	VkPipelineLayout layout;

	std::set<uint32_t> sets;

	std::vector<VkPushConstantRange> pushConstants;
	uint32_t pushConstantLimit;
};


#include "DepthImage.h"
#include "GraphicsEngine.h"
#include "CoreEngine\IEngine.h"

DepthImage::DepthImage(ComponentInterface* core, uint32_t width, uint32_t height) : EventHandler(core), core(core), data(nullptr), width(width), height(height)
{
	core->getEventBus()->registerHandler(this, VulkanDeviceConstructionEvent::getClassID());
	core->getEventBus()->registerHandler(this, RenderSystemSetupEvent::getClassID());
	core->getEventBus()->registerHandler(this, VulkanDeviceDestructionEvent::getClassID());
}


DepthImage::~DepthImage()
{
	if (data)
		delete data;
}

void DepthImage::operator()(ComponentInterface * core, const EventBase* evt)
{
	if (evt->is<VulkanDeviceConstructionEvent>())
	{
		VulkanDeviceConstructionEvent* data = (VulkanDeviceConstructionEvent*)evt;
		img.construct(*data->getDeviceHandle(), width, height);
	}
	else if (evt->is<RenderSystemSetupEvent>())
	{
		RenderSystemSetupEvent* data = (RenderSystemSetupEvent*)evt;
		img.init(((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice()); //These calls really aren't elegant. Device and Surface need abstraction i feel.
	}
	else if (evt->is<VulkanDeviceDestructionEvent>())
	{
		VulkanDeviceDestructionEvent* data = (VulkanDeviceDestructionEvent*)evt;
		img.destroy(*data->getDeviceHandle());
	}
}

void DepthImage::DepthImageObject::construct(Device& dev, uint32_t width, uint32_t height)
{
	Image::construct(dev, width, height, getFormatFromDevice(*dev.getPhysicalDevice(), { VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT }, VK_IMAGE_TILING_OPTIMAL, VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT));
	VkCommandBufferAllocateInfo allocInfo;
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.pNext = nullptr;
	allocInfo.commandPool = dev.getGraphicsQueue().getCommandPoolHandle();
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = 1;
	vkAllocateCommandBuffers(dev, &allocInfo, &buf);
}

void DepthImage::DepthImageObject::init(Device& dev)

{
	Image::init(dev);

	//VkCommandBufferBeginInfo info{};
	//info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	//info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	//vkBeginCommandBuffer(buf, &info);

	//reset(buf);

	//vkEndCommandBuffer(buf);

	//dev.getGraphicsQueue().submitCommandBuffer(buf);

}

void DepthImage::DepthImageObject::reset(ComponentInterface* core)
{
	VkCommandBufferBeginInfo info{};
	info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	vkBeginCommandBuffer(buf, &info);

	reset(buf);

	vkEndCommandBuffer(buf);

	((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice().getGraphicsQueue().submitCommandBuffer(buf);
}

void DepthImage::DepthImageObject::reset(VkCommandBuffer & record)
{
	VkImageMemoryBarrier transferBarrier = {};
	transferBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	transferBarrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	transferBarrier.newLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
	transferBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	transferBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	transferBarrier.image = getImage();
	transferBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
	transferBarrier.subresourceRange.baseMipLevel = 0;
	transferBarrier.subresourceRange.levelCount = 1;
	transferBarrier.subresourceRange.baseArrayLayer = 0;
	transferBarrier.subresourceRange.layerCount = 1;
	transferBarrier.srcAccessMask = 0;
	transferBarrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

	vkCmdPipelineBarrier(record, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT, 0, 0, nullptr, 0, nullptr, 1, &transferBarrier);
}

inline void DepthImage::reset()
{
	img.reset(core);
}

void DepthImage::reset(CommandBufferRecord& context)
{
	VkCommandBuffer buf = context.getCommandBuffer();
	img.reset(buf);
}

void* DepthImage::getComponentData()
{
	VkAttachmentDescription desc;
	desc.flags = 0;
	desc.format = VK_FORMAT_D16_UNORM;//img.getFormat();
	desc.samples = VK_SAMPLE_COUNT_1_BIT;
	desc.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	desc.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	desc.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	desc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	desc.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	desc.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;

	VkClearValue val{};
	val.depthStencil = VkClearDepthStencilValue{ 1.0f, 0 };

	if (data != nullptr)
		delete data;
	data = new Attachment(desc, val, getImageObject().getImageView(), VkExtent3D{img.getWidth(), img.getHeight(), 1}); //Memory Leak? Memory Leak

	return data;
}

void DepthImage::DepthImageObject::destroy(Device& dev)
{
	Image::destroy(dev);
	vkFreeCommandBuffers(dev, dev.getGraphicsQueue().getCommandPoolHandle(), 1, &buf);
}

void DepthImage::DepthImageObject::createImage(Device &dev)
{
	VkImageCreateInfo imageInfo = {};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageInfo.imageType = VK_IMAGE_TYPE_2D;
	imageInfo.extent.width = getWidth();
	imageInfo.extent.height = getHeight();
	imageInfo.extent.depth = 1;
	imageInfo.mipLevels = 1;
	imageInfo.arrayLayers = 1;
	imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	imageInfo.format = VK_FORMAT_D16_UNORM;//getFormat();
	imageInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
	imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;

	VkImage tex;
	if (vkCreateImage(dev.getHandle(), &imageInfo, nullptr, &tex) != VK_SUCCESS) {
		throw std::runtime_error("failed to create image!");
	}
	setImage(std::move(tex));
}

void DepthImage::DepthImageObject::createImageView(Device &dev)
{
	VkImageViewCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	createInfo.image = getImage();
	createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	createInfo.format = VK_FORMAT_D16_UNORM;//getFormat();
	createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
	createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
	createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
	createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
	createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
	createInfo.subresourceRange.baseMipLevel = 0;
	createInfo.subresourceRange.levelCount = 1;
	createInfo.subresourceRange.baseArrayLayer = 0;
	createInfo.subresourceRange.layerCount = 1;

	VkImageView view;
	if (vkCreateImageView(dev.getHandle(), &createInfo, VK_NULL_HANDLE, &view) != VK_SUCCESS) {
		throw VulkanConstructionError("failed to create image views!");
	}
	setImageView(std::move(view));
}

VkFormat DepthImage::DepthImageObject::getFormatFromDevice(const PhysicalDevice& dev, const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features)
{
	for (VkFormat format : candidates) {
		VkFormatProperties props;
		vkGetPhysicalDeviceFormatProperties(dev, format, &props);

		if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
			return format;
		}
		else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) {
			return format;
		}
	}

	throw VulkanConstructionError("failed to find supported format!");
}



__declspec(dllexport) IRenderComponent* __cdecl createDepthImage(ComponentInterface* core, uint32_t width, uint32_t height)
{
	return new DepthImage(core, width, height);
}
__declspec(dllexport) void __cdecl destroyDepthImage(IRenderComponent* ptr, ComponentInterface * core)
{
	delete((DepthImage*)ptr);
}
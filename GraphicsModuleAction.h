#include <string>
#include <glm\glm.hpp>

#pragma once

#define GRAPHICS_MODULE_NAME "GraphicsSystem"

class ILightRenderer;

namespace GraphicsModuleAction
{
	enum EngineAction
	{
		UPDATE_RENDER_CYCLE,
		ADD_FRAMEBUFFER_VIEW
	};

	enum EngineState
	{
		UNINITIALIZED = 0,
		INSTANCE_CREATED = 1,
		DEVICE_CREATED = 2,
		RENDER_SYSTEM_INITIALIZED =4,
		OUTPUT_CREATED = 8
	};

	struct AddFramebufferView
	{
		const void* view;
	};

	struct MeshData
	{
		std::string name;
		glm::vec3 offset;
	};
	struct TextureData
	{
		std::string name;
	};
	struct MaterialData
	{
		float intensity;
		float exponent;
	};


	struct BaseLightData
	{
		ILightRenderer* renderer;
		glm::vec3 color;
		float intensity;
	};
	struct DirectionalLightData {
		BaseLightData base;
		glm::vec3 direction;
	};
	struct PointLightData {
		BaseLightData base;
		glm::vec3 position;
		float attenuationConstant;
		float attenuationLinear;
		float attenuationExponent;
	};
	struct SpotLightData {
		PointLightData base;
		glm::vec3 direction;
		float cutoff;
	};
};

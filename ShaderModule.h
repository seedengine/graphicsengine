#include "CoreEngine\IEngine.h"
#include <vulkan\vulkan.h>

#pragma once
class ShaderModule : public EventHandler
{
public:
	ShaderModule(ComponentInterface*);

	void operator()(ComponentInterface*, const EventBase*);

	VkPipelineShaderStageCreateInfo getShaderInfo(VkShaderStageFlagBits type);

	ShaderModule& operator=(const ShaderModule& other)
	{
		fileName = other.fileName;
		if (other.code != nullptr)
		{
			size = other.size;
			code = (uint32_t*)malloc(other.size);
			memcpy(code, other.code, size);
		}
		shader = other.shader;
		return *this;
	}

	~ShaderModule();

	void setShaderCode(uint32_t* code, uint32_t);
private:
	std::string fileName;
	uint32_t* code;
	uint32_t size;
	VkShaderModule shader;
};


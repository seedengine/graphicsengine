#include <vulkan\vulkan.h>

#include "ITexture.h"
#include "Device.h"
#include "GraphicsExceptions.h"
#include "Image.h"
#include "VulkanBuffer.h"

#include <png.h>

class Device;
class DescriptorSet;

#pragma once
class Texture : public ITexture, public Image
{
public:
	Texture(ComponentInterface* parent, const std::string path) : ITexture(parent), path(path), component(parent)
	{}
	Texture(ComponentInterface* parent, const char* path) : ITexture(parent), path(path), component(parent)
	{}

	virtual const VkSampler getSampler() const { return sampler; }

	virtual void operator()(ComponentInterface* core, const EventBase*);

	~Texture();
protected:
	virtual void load();

	virtual ImageType getFileExtension();
	virtual ImageType getFileImageType(std::ifstream&);

	virtual inline void listRequirements(std::set<uint64_t>& req);
private:
	static void userReadData(png_structp pngPtr, png_bytep data, png_size_t length);
	VkFormat getFormatFromStructure()
	{
		switch (_structure)
		{
		case R:
			return VK_FORMAT_R8_UNORM;
		case RA:
			return VK_FORMAT_R8G8_UNORM;
		case RGB:
			return VK_FORMAT_R8G8B8_UNORM;
		case RGBA:
			return VK_FORMAT_R8G8B8A8_UNORM;
		default:
			return VK_FORMAT_R8_UNORM;
		}
	}

	static VulkanBuffer* stageBuffer;

	ComponentInterface* component;

	VkSampler sampler;
	DescriptorSet* set;
	MemoryRange descriptorID;

	std::string path;

	uint32_t _width, _height;
	uint16_t _depth;
	uint32_t _compression;

	PixelStructure _structure;

	uint8_t* _data;
};

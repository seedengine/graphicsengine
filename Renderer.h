#include "CoreEngine\Event.h"
#include "GraphicsEvents.h"
#include "GraphicsEngine.h"
#include "Pipeline.h"
#include "PipelineLayout.h"
#include "IPipeline.h"
#include "RenderContext.h"

#pragma once
class Renderer : public RenderPass
{
	class RenderStep : public Subpass {
	public:
		RenderStep(ComponentInterface* core, std::string pipeline, std::function<void(IRenderContext*)> renderFunc);

		void construct(IRenderSystem* renderSystem, Surface* surface);

		virtual void render(IRenderContext*);

		void destroy();
	private:
		ComponentInterface* core;
		std::function<void(IRenderContext*)> renderFunc;
		uint32_t pipelineID;
		IPipeline* p;
		VkPushConstantRange range;
	};
public:
	Renderer(ComponentInterface* engine, std::string pipeline, bool registerEventHandler = true);

	virtual void operator()(ComponentInterface*, const EventBase*);

	virtual void _render(IRenderContext* context);

	virtual ~Renderer() {}
protected:
	RenderStep step;

	ComponentInterface* core;
	Surface* surface;
};

extern "C" __declspec(dllexport) IRenderSystem* __cdecl createRenderer(ComponentInterface* core, std::string pipeline, bool registerEventHandler);
extern "C" __declspec(dllexport) void __cdecl destroyRenderer(IRenderSystem*, ComponentInterface* core);

#include "RenderContext.h"
#include "IPipeline.h"
#include "CoreEngine/OrderedList.hpp"


void IRenderStep::registerPipeline(IPipeline* pipeline) { pipelines.insert(pipeline->getID()); }


IRenderSystem::ComponentRef IRenderSystem::addRenderComponent(IRenderComponent* component)
{
	static std::mutex mut;
	mut.lock();
	ComponentRef ref = components.size();
	components.push_back(component);
	mut.unlock();
	return ref;
}

IRenderSystem::StepRef IRenderSystem::addRenderStep(IRenderStep* step)
{
	static std::mutex mut;
	mut.lock();
	StepRef ref = steps.size();
	step->parent = this;
	steps.insert(step);
	mut.unlock();
	return ref;
}


IRenderStep* IRenderSystem::getRenderStep(const uint32_t stepRef) 
{
	return const_cast<IRenderStep*>(steps.find(stepRef));
}

void IRenderSystem::render(IRenderContext* context)
{
	for (IRenderStep* step : steps)
		step->render(context);
}
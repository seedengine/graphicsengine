#include "CoreEngine\Event.h"

class Instance;
class ComponentInterface;
class Device;

#pragma once
DEFINE_EVENT(VulkanInstanceConstructionEvent)
public:
	void setInstance(Instance* instance) { this->instance = instance; }
	Instance* getInstance() const { return instance; }
private:
	Instance* instance;
};
DEFINE_EVENT(PhysicalDeviceValidationEvent)
};
DEFINE_EVENT(VulkanFeatureSetupEvent)
public:
	VkPhysicalDeviceFeatures* getFeatures() { return features; }
	void setFeatures(VkPhysicalDeviceFeatures* features) { this->features = features; }
private:
	VkPhysicalDeviceFeatures* features;
};
DEFINE_EVENT(VulkanDeviceConstructionEvent)
public:
	void setDeviceHandle(Device* dev) { device = dev; }
	Device* getDeviceHandle() const { return device; }
private:
	Device* device;
};

DEFINE_EVENT(VulkanDeviceDestructionEvent)
public:
	void setDeviceHandle(Device* dev) { device = dev; }
	Device* getDeviceHandle() const { return device; }
private:
	Device* device;
};
DEFINE_EVENT(VulkanInstanceDestructionEvent)
public:
	void setInstance(Instance* instance) { this->instance = instance; }
	Instance* getInstance() const { return instance; }
private:
	Instance* instance;
};

//DEFINE_COND_EVENT(std::negation<std::is_empty<Instance>>, VulkanInstanceDestructionEvent)
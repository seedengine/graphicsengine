#include <vulkan\vulkan.h>
#include <vector>
#include <set>

#include "CoreEngine/DataStructures.h"

class Instance;
class Log;
class ComponentInterface;

#pragma once
class VulkanInstanceFactory
{
public:
	VulkanInstanceFactory(ComponentInterface* logger);
	~VulkanInstanceFactory();


	void setAppVersion(uint32_t version);

	bool isExtensionAvailable(const char*) const;
	bool validateExtensionList(const char**, size_t count) const;
	void setExtensionList(const char**, size_t count);

	bool isLayerAvailable(const char*) const;
	bool validateLayerList(const char**, size_t count) const;
	void setLayerList(const char**, size_t count);

	void buildInstance(Instance&);
private:
	Log* logger;

	VkApplicationInfo appInfo;
	VkInstanceCreateInfo instanceInfo;

	std::set<std::string> availableLayers;
	std::set<std::string> availableExtensions;
};


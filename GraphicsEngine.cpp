#include "GraphicsEngine.h"

#include "CoreEngine/Log.h"
#include "UserInterface/IUserInterface.h"
#include "VulkanInstanceFactory.h"
#include "GraphicsExceptions.h"
#include "VulkanDeviceFactory.h"
#include "CoreEngine/IEngine.h"
#include "GraphicsModuleAction.h"
#include "Camera.h"
#include "Mesh.h"
#include "Texture.h"
#include "Material.h"
#include "Light.h"
#include "UserInterface/InputEvents.h"
#include "UserInterface\UserInterfaceModuleAction.h"
#include "VulkanGraphicsEvents.h"
#include "DescriptorSetFactory.h"
#include "CoreEngine/Event.hpp"

#include <mutex>

const 

DEFINE_EVENT(FinalizationSetupEvent)
};
DEFINE_EVENT(FinalizationEvent)
};
DEFINE_EVENT(VulkanCleanupEvent)
};


const char* GraphicsEngine::validationLayers[] = {
	//"VK_LAYER_KHRONOS_validation",
	"VK_LAYER_LUNARG_standard_validation"
	//NVIDIA checks
	//"VK_LAYER_NV_optimus"
	//NSight checks
	//"VK_LAYER_NV_GPU_Trace_release_public_2020_3_1",
	//"VK_LAYER_NV_nomad_release_public_2020_3_1",
	//"VK_LAYER_NV_nsight"
	//not a thing anymore
	//"VK_LAYER_LUNARG_parameter_validation",
	//"VK_LAYER_LUNARG_core_validation"
};
const char* GraphicsEngine::deviceExtensions[] = {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME,
	
	//Vulkan Device Indexing, Maintance included in 1.1 core
	//VK_KHR_MAINTENANCE1_EXTENSION_NAME,
	//VK_KHR_MAINTENANCE2_EXTENSION_NAME,
	//VK_KHR_MAINTENANCE3_EXTENSION_NAME,
	VK_EXT_DESCRIPTOR_INDEXING_EXTENSION_NAME
};

GraphicsEngine::GraphicsEngine(ComponentInterface * component) :
	IEngineModule(component),
	EventHandler(component),
	frameRender([this]() { run(); }), 
	core(component),
	setupComplete(false),
	device(component),
	state(GraphicsModuleAction::EngineState::UNINITIALIZED),
	depthMap(component, 800, 600)
{
	component->getLog()->writeInfo("Initializing Engine Graphics component.");

	//Instance construction
	{
		component->getLog()->writeInfo("[Graphics] Building Vulkan Instance.");
		VulkanInstanceFactory instanceFactory(component);
		component->getLog()->writeInfo( "[Graphics] Initializing Vulkan Graphics on Version 1.2");
		instanceFactory.setAppVersion(VK_MAKE_VERSION(1, 2, 0));

		if (component->getDebugState() && instanceFactory.validateLayerList(validationLayers, sizeof(validationLayers) / sizeof(char*)))
		{
			instanceFactory.setLayerList(validationLayers, sizeof(validationLayers) / sizeof(char*));
			component->getLog()->writeInfo("[Graphics] Enabling the following Validation Layers for Vulkan Instance:");
			for (const char * layer : validationLayers)
				component->getLog()->writeInfo(("[Graphics]         " + std::string(layer)).c_str());
		}
		else
			component->getLog()->writeError("[Graphics] Debug Validation Layers not available for Vulkan Instance.");

		std::vector<const char*> extensionList = getExtensionList(component);
		
		if (instanceFactory.validateExtensionList(extensionList.data(), extensionList.size()))
		{
			instanceFactory.setExtensionList(extensionList.data(), extensionList.size());
			component->getLog()->writeInfo("[Graphics] Enabling the following Extensions for Vulkan Instance:");
			for (const char * extension : extensionList)
				component->getLog()->writeInfo(("[Graphics]         " + std::string(extension)).c_str());
		}
		else
			throw VulkanNotSupportedError("Missing required Extensions."); //TODO list missing extensions
		instanceFactory.buildInstance(instance);
	}
	state |= GraphicsModuleAction::EngineState::INSTANCE_CREATED;
	
	component->getLog()->writeInfo("[Graphics] Registering Event Handling.");
	component->getEventBus()->registerHandler(this, ResizeEvent::getClassID());
	component->getEventBus()->registerHandler(this, VulkanInstanceConstructionEvent::getClassID());
	component->getEventBus()->registerHandler(this, RenderSystemSetupEvent::getClassID());
	component->getEventBus()->registerHandler(this, FinalizationEvent::getClassID());
	component->getEventBus()->registerHandler(this, FinalizationSetupEvent::getClassID());
	component->getEventBus()->registerHandler(this, VulkanCleanupEvent::getClassID());
	component->getEventBus()->registerHandler(this, VulkanDeviceDestructionEvent::getClassID());
	component->getEventBus()->registerHandler(this, VulkanInstanceDestructionEvent::getClassID());
	component->getEventBus()->registerHandler(this, VulkanFeatureSetupEvent::getClassID());
	VulkanInstanceConstructionEvent evt;
	evt.setInstance(&instance);
	component->getEventBus()->dispatchEvent(core, evt, { LoadEvent::getClassID() }); //fired when Instance is defined
	subEvents.insert(VulkanInstanceConstructionEvent::getClassID());
	subEvents.insert(VulkanDeviceConstructionEvent::getClassID());
	subEvents.insert(RenderSystemInitEvent::getClassID());
	FinalizationPreparationEvent finalizationPreparation(&subEvents);
	FinalizationSetupEvent finalizationSetup;
	component->getEventBus()->dispatchEvent(core, finalizationSetup, { FinalizationPreparationEvent::getClassID()}); //fired when Instance is defined
	component->getEventBus()->dispatchEvent(core, finalizationPreparation); //fired when Instance is defined
}

GraphicsEngine::~GraphicsEngine()
{
}

void GraphicsEngine::construct(ComponentInterface * component)
{
	//Surface requires UserInterface. Since UserInterface is a different component its required to be done in construct since only then the existence can be guaranteed
	component->getLog()->writeInfo("[Graphics] Constructing Rendering Surface on Window.");
	surface.construct(instance, component);

	//Device construction (Depends on Surface to evaluate device.)
	{
		component->getLog()->writeInfo("[Graphics] Building Vulkan Device.");
		VulkanDeviceFactory deviceFactory{ component };
		component->getLog()->writeInfo("[Graphics] Scanning for available Hardware.");
		deviceFactory.readPhysicalDevices(instance, surface);

		if (deviceFactory.validateExtensionList(deviceExtensions, sizeof(deviceExtensions) / sizeof(char*)))
		{
			deviceFactory.setDeviceExtensions(deviceExtensions, sizeof(deviceExtensions) / sizeof(char*));
			component->getLog()->writeInfo("[Graphics] Enabling the following Extensions for Vulkan Device:");
			for (const char * extension : deviceExtensions)
				component->getLog()->writeInfo(("[Graphics]         " + std::string(extension)).c_str());
		}
		else
			throw VulkanNotSupportedError("Missing required Extensions."); //TODO list missing extensions

		if (component->getDebugState())
		{
			deviceFactory.setActivationLayers(validationLayers, sizeof(validationLayers) / sizeof(char*));
			component->getLog()->writeInfo("[Graphics] Enabling the following Validation Layers for Vulkan Device:");
			for (const char * layer : validationLayers)
				component->getLog()->writeInfo(("[Graphics]         " + std::string(layer)).c_str());
		}
		else
			component->getLog()->writeError("[Graphics] Debug Validation Layers not available for Vulkan Device.");

		deviceFactory.buildDevice(device);
	}

	depthMap.construct();

	state |= GraphicsModuleAction::EngineState::DEVICE_CREATED;
	VulkanDeviceConstructionEvent deviceEvt;
	deviceEvt.setDeviceHandle(&device);
	component->getEventBus()->dispatchEvent(core, deviceEvt, { VulkanInstanceConstructionEvent::getClassID() });

	RenderSystemSetupEvent setupEvt;
	setupEvt.setSurface(&surface);
	RenderSystemInitEvent initEvent;
	component->getEventBus()->dispatchEvent(core, setupEvt, {VulkanDeviceConstructionEvent::getClassID()});
	component->getEventBus()->dispatchEvent(core, initEvent, {RenderSystemSetupEvent::getClassID()});

}

void GraphicsEngine::destroy()
{
	//Synchronous invocations since at this points multithreading isn't active anymore. Might be cahnged down the line.

	VulkanDeviceDestructionEvent deviceEvt;
	deviceEvt.setDeviceHandle(&device);
	core->getEventBus()->invokeEventSynchonous(core, deviceEvt);
	VulkanInstanceDestructionEvent evt;
	evt.setInstance(&instance);
	core->getEventBus()->invokeEventSynchonous(core, evt);
	VulkanCleanupEvent cleanup;
	core->getEventBus()->invokeEventSynchonous(core, cleanup);
}

uint32_t GraphicsEngine::getState()
{
	return state;
}

void __cdecl GraphicsEngine::doAction(uint32_t action, void* data)
{
	switch (action)
	{
	case GraphicsModuleAction::EngineAction::UPDATE_RENDER_CYCLE:
		updateCommandBuffers();
		break;
	default:
		core->getLog()->writeError(("GraphicsSystem does not recognize " + std::to_string(action) + " as an action.").c_str());
	}
}

void GraphicsEngine::run()
{
	
	device.waitUntilIdle();
	device.present(this);
}

void GraphicsEngine::submitBuffer()
{
	device.getGraphicsQueue().submitCommandBuffer(buffers[swapchain.getIdx()]->getHandle());
}

void GraphicsEngine::prepareCommandBuffers()
{
	while (buffers.size() < swapchain.getSize())
	{
		CommandBuffer* buf = new CommandBuffer();
		buf->construct(device, true, 0 /*no clue why this number exists or what it does*/);
		buffers.push_back(buf);
	}

	RecordSubpassEvent subpassEvt;
	core->getEventBus()->invokeEventSynchonous(core, subpassEvt);
}

void GraphicsEngine::updateCommandBuffers()
{
	uint32_t id = 0;
	for (CommandBuffer*& buffer : buffers) {
		CommandBufferRecord rec;
		buffer->record(&rec, VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT | VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT, VK_NULL_HANDLE, VK_NULL_HANDLE, 0);
		ExecuteRenderPass evt;
		evt.setCommandBuffer(&rec);
		evt.setBufferID(id);
		core->getEventBus()->invokeEventSynchonous(core, evt);

		swapchain.next();
		id += 1;
	}
}

void GraphicsEngine::operator()(ComponentInterface*, const EventBase* evt)
{
	if (evt->is<ResizeEvent>())
	{
		if (!setupComplete)
			return;
		static std::mutex mut;
		mut.lock();
		surface.readSurfaceProperties(device.getPhysicalDevice());
		device.waitUntilIdle();
		swapchain.rebuildSwapChain(device, surface);
		prepareCommandBuffers();
		updateCommandBuffers();

		mut.unlock();
	}
	else if (evt->is<RenderSystemSetupEvent>())
	{
		swapchain.construct(device, surface);
		state |= GraphicsModuleAction::EngineState::RENDER_SYSTEM_INITIALIZED;
	}
	else if (evt->is<FinalizationSetupEvent>())
	{
		FinalizationEvent evt;
		core->getEventBus()->dispatchEvent(core, evt, subEvents);
	}
	else if (evt->is<FinalizationEvent>())
	{
		device.getDescriptorFactory().update();
		prepareCommandBuffers();
		updateCommandBuffers();

		core->getThreadPool()->listHighFreqTask(ScheduledTask(&frameRender, true));
		core->getLog()->writeInfo("Registered Render Task.");

		setupComplete = true;
	}
	else if (evt->is<VulkanInstanceDestructionEvent>())
	{
		device.destroy();
		surface.destroy(instance);
	}
	else if (evt->is<VulkanDeviceDestructionEvent>())
	{
		swapchain.destroy(device);
	}
	else if (evt->is<VulkanFeatureSetupEvent>())
	{
		VulkanFeatureSetupEvent* data = (VulkanFeatureSetupEvent*)evt;

		//requirements for fast wireframe
		data->getFeatures()->wideLines = VK_TRUE;
		data->getFeatures()->fillModeNonSolid = VK_TRUE;
	}
	else if (evt->is<VulkanCleanupEvent>())
	{
		instance.destroy();
	}
}

std::vector<const char*> GraphicsEngine::getExtensionList(ComponentInterface* component)
{
	std::vector<const char*> extensions;

	UserInterfaceModuleAction::GetVulkanExtensionList data;

	component->getModule(USER_INTERFACE_MODULE_NAME)->doAction(UserInterfaceModuleAction::EngineAction::GET_VULKAN_EXTENSION_LIST, &data);

	for (unsigned int i = 0; i < data.count; i++)
		extensions.push_back(data.text[i]);

	if (component->getDebugState())
		extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
	return extensions;
}

__declspec(dllexport) IEngineModule* __cdecl createModule(ComponentInterface* core, std::string name)
{
	if (name == GRAPHICS_MODULE_NAME)
		return (IEngineModule*)new GraphicsEngine(core);
	else
		return nullptr;
}
__declspec(dllexport) void __cdecl destroyModule(IEngineModule* ptr, ComponentInterface * core, std::string name)
{
	if (name == GRAPHICS_MODULE_NAME)
		delete((GraphicsEngine*)ptr);
}

IObjectComponent * __cdecl createComponent(ComponentInterface * core, std::string name, void* extData)
{
	if (name == "Camera")
		return (IObjectComponent*)new Camera(core);
	else if (name == "Mesh")
	{
		GraphicsModuleAction::MeshData* data = (GraphicsModuleAction::MeshData*) extData;
		return ((IObjectComponent*)new Mesh(core, data->name, data->offset));
	}
	else if (name == "Texture")
	{
		GraphicsModuleAction::TextureData* data = (GraphicsModuleAction::TextureData*) extData;
		return ((IObjectComponent*)new Texture(core, data->name));
	}
	else if (name == "Material")
	{
		GraphicsModuleAction::MaterialData* data = (GraphicsModuleAction::MaterialData*) extData;
		return ((IObjectComponent*)new Material(core, data->exponent, data->intensity));
	}
	else if (name == "AmbientLight")
	{
		GraphicsModuleAction::BaseLightData* data = (GraphicsModuleAction::BaseLightData*) extData;
		return ((IObjectComponent*)new BaseLight(core, data->renderer, data->color, data->intensity));
	}
	else if (name == "DirectionalLight")
	{
		GraphicsModuleAction::DirectionalLightData* data = (GraphicsModuleAction::DirectionalLightData*) extData;
		return ((IObjectComponent*)new DirectionalLight(core, data->base.renderer, data->base.color, data->base.intensity, data->direction));
	}
	else if (name == "PointLight")
	{
		GraphicsModuleAction::PointLightData* data = (GraphicsModuleAction::PointLightData*) extData;
		return ((IObjectComponent*)new PointLight(core, data->base.renderer, data->base.color, data->base.intensity, data->attenuationConstant, data->attenuationLinear, data->attenuationExponent, data->position));
	}
	else if (name == "SpotLight")
	{
		GraphicsModuleAction::SpotLightData* data = (GraphicsModuleAction::SpotLightData*) extData;
		return ((IObjectComponent*)new SpotLight(core, data->base.base.renderer, data->base.base.color, data->base.base.intensity, data->base.attenuationConstant, data->base.attenuationLinear, data->base.attenuationExponent, data->base.position, data->direction, data->cutoff));
	}
	else
		return nullptr;
}

void __cdecl destroyComponent(ComponentInterface * core, IObjectComponent * obj, std::string name)
{
	if (name == "Camera")
		delete((Camera*)obj);
	else if (name == "Mesh")
		delete ((Mesh*)obj);
	else if (name == "Texture")
		delete((Texture*)obj);
	else if (name == "Material")
		delete((Material*)obj);
}

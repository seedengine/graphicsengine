#include "RenderPass.h"

#include "CoreEngine/StaticCoreEngine.h"
#include "CoreEngine/Event.hpp"
#include "Device.h"
#include "SwapChain.h"
#include "GraphicsExceptions.h"
#include "PipelineLayout.h"
#include "GraphicsEngine.h"
#include "CoreEngine\OrderedList.hpp"
#include "CoreEngine/ISwappable.h"


RenderPass::RenderPass(ComponentInterface* component, bool registerEventInvokations) : EventHandler(component), interface(component), framebuffers(3)
{
	component->getEventBus()->registerHandler(this, FinalizationPreparationEvent::getClassID());
	if (registerEventInvokations)
	{
		component->getEventBus()->registerHandler(this, RecordSubpassEvent::getClassID());
	}
}

void RenderPass::construct(Surface* surface)
{
	Device& dev = ((GraphicsEngine*)interface->getModule(GRAPHICS_MODULE_NAME))->getDevice();

	idCtr.store(0);
	pipelineProducer.construct(dev);
	std::vector<VkSubpassDescription> subpassDesc(steps.size());
	for (size_t i = 0; i < subpassDesc.size(); i++)
	{
		subpassDesc[i] = *(VkSubpassDescription*)steps[i]->getStepData();
	}
	
	for (size_t i = 1; i < subpassDesc.size(); i++)
	{
		Dependency dep1{i-1, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, 0 }, dep2{ i, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT };

		registerDependency(dep1, dep2);
	}

	std::vector<VkAttachmentDescription> attachments;
	for (const FramebufferAttachmentProviderAttachment& provider : providers)
	{
		VkAttachmentDescription att = ((Attachment*)(provider.provider->getFramebufferAttachment(0).getComponentData()))->getAttachmentHandle();
		if (provider.shouldClear)
			att.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		else
		{
			att.initialLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
			att.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
		}
		attachments.push_back(att);
	}
	for (IRenderComponent* desc : components)
	{
		IRenderComponent* data = (IRenderComponent*)desc->getComponentData();
		attachments.push_back(((Attachment*)data)->getAttachmentHandle());
	}


	VkRenderPassCreateInfo renderPassInfo{};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
	renderPassInfo.pAttachments = attachments.data();
	renderPassInfo.subpassCount = static_cast<uint32_t>(subpassDesc.size());
	renderPassInfo.pSubpasses = subpassDesc.data();
	renderPassInfo.dependencyCount = static_cast<uint32_t>(dependencies.size());
	renderPassInfo.pDependencies = dependencies.data();

	if (vkCreateRenderPass(dev, &renderPassInfo, VK_NULL_HANDLE, &pass) != VK_SUCCESS) {
		throw VulkanConstructionError("failed to create render pass!");
	}

	framebuffers.executeIndexed(std::function<void(Framebuffer&, size_t)>([this, &dev](Framebuffer& framebuffer, size_t idx)
	{
		uint32_t width = 65536, height = 65536, depth = 1;

		std::vector<VkImageView> auxViews;
		auxViews.reserve(components.size() + providers.size());
		
		for (const FramebufferAttachmentProviderAttachment& provider : providers)
		{
			Attachment attachment = provider.provider->getFramebufferAttachment(idx);
			if (provider.shouldClear)
				attachment.getAttachmentHandle().loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
			else
				attachment.getAttachmentHandle().loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
			VkExtent3D extent = attachment.getExtent();
			width = std::min(width, std::max( 1u, extent.width));
			height = std::min(height, std::max(1u, extent.height));
			depth = std::min(depth, std::max(1u, extent.depth));
			auxViews.push_back(attachment.getImageView());
		}
		
		for (size_t i = 0; i < components.size(); i++)
		{
			Attachment* attachment = ((Attachment*)components[i]->getComponentData());
			VkExtent3D extent = attachment->getExtent();
			width = std::min(width, std::max(1u, extent.width));
			height = std::min(height, std::max(1u, extent.height));
			depth = std::min(depth, std::max(1u, extent.depth));
			auxViews.push_back(attachment->getImageView());
		}

		interface->getLog()->writeInfo(("[RenderPass] selected framebuffer Resolution: (" + std::to_string(width) + ", " + std::to_string(height) + ", " + std::to_string(depth) + ")").c_str());

		VkFramebufferCreateInfo framebufferInfo;
		framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebufferInfo.pNext = nullptr;
		framebufferInfo.flags = 0;
		framebufferInfo.renderPass = pass;
		framebufferInfo.attachmentCount = auxViews.size();
		framebufferInfo.pAttachments = auxViews.data();
		framebufferInfo.width = width;
		framebufferInfo.height = height;
		framebufferInfo.layers = depth;
		framebuffer.extent = VkExtent2D{ width, height};
		if (vkCreateFramebuffer(dev, &framebufferInfo, VK_NULL_HANDLE, &framebuffer.framebuffer) != VK_SUCCESS) {
			throw VulkanConstructionError("failed to create Framebuffer!");
		}
	}));


	for (IRenderStep* subpass : steps)
		((Subpass*)subpass)->construct(dev, idCtr++);

	PipelineBuildEvent evt;
	evt.setRenderSystem(this);
	evt.setSurface(surface);
	interface->getEventBus()->dispatchEvent(interface, evt, {DescriptorAllocationEvent::getClassID()});
}

const VkRenderPass& RenderPass::getHandle() const
{
	return pass;
}

PipelineFactory & RenderPass::getFactory()
{
	pipelineProducer.reset();
	return pipelineProducer;
}

void RenderPass::operator()(ComponentInterface *, const EventBase * evt)
{
	if (evt->is<FinalizationPreparationEvent>())
	{
		FinalizationPreparationEvent* data = (FinalizationPreparationEvent*)evt;
		data->addDependentEvent(PipelineBuildEvent::getClassID());
	}
	else if (evt->is<RecordSubpassEvent>()) {
		recordSubpasses();
	}
}

void RenderPass::registerDependency(Dependency & src, Dependency & dst)
{
	VkSubpassDependency dependency = {};
	dependency.srcSubpass = src.pass;
	dependency.srcStageMask = src.stage;
	dependency.srcAccessMask = src.access;
	dependency.dstSubpass = dst.pass;
	dependency.dstStageMask = dst.stage;
	dependency.dstAccessMask = dst.access;
	dependencies.push_back(dependency);
}

void RenderPass::recordSubpasses()
{
	for (IRenderStep* subpass : steps)
		((Subpass*)subpass)->updateAll(*this);
}

void RenderPass::render(CommandBufferRecord& rec, uint32_t buffer_id)
{
	if (!getActiveState())
		return;

	RenderContext context(&rec, -1, -1);

	{
		VkRenderPassBeginInfo renderPassInfo = {};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		renderPassInfo.pNext = nullptr;
		renderPassInfo.renderPass = pass;
		renderPassInfo.framebuffer = getFramebuffer(buffer_id).framebuffer;
		renderPassInfo.renderArea.offset = { 0, 0 };
		renderPassInfo.renderArea.extent = getFramebuffer(buffer_id).extent;
		std::vector<VkClearValue> vals;
		for (const FramebufferAttachmentProviderAttachment provider : providers)
		{
			Attachment attachment = provider.provider->getFramebufferAttachment(framebuffers.getIdx());
			if (attachment.getAttachmentHandle().loadOp == VK_ATTACHMENT_LOAD_OP_CLEAR)
				vals.push_back(attachment.getClearValue());
		}
		for (IRenderComponent* component : components)
		{
			Attachment* att = (Attachment*)component->getComponentData();
			if (att->getAttachmentHandle().loadOp == VK_ATTACHMENT_LOAD_OP_CLEAR)
				vals.push_back(att->getClearValue());
		}
		renderPassInfo.clearValueCount = vals.size();
		renderPassInfo.pClearValues = vals.data();
		VkCommandBuffer cmdBuf = rec.getCommandBuffer();
		vkCmdBeginRenderPass(cmdBuf, &renderPassInfo, getRenderMethod());
	}

	auto subpassIt = steps.begin();
	IRenderStep* sub = *subpassIt;
	while (subpassIt != steps.end()) {
		sub = *subpassIt;
		sub->executeStep(&context);
		subpassIt++;
		if (subpassIt == steps.end())
			break;
		vkCmdNextSubpass(rec.getCommandBuffer(), getRenderMethod());
	}

	vkCmdEndRenderPass(rec.getCommandBuffer());
	
	//framebuffers.next();
}

Framebuffer RenderPass::getFramebuffer(uint32_t id) { return framebuffers.getList()[id]; }

void RenderPass::destroy()
{
	Device& dev = ((GraphicsEngine*)interface->getModule(GRAPHICS_MODULE_NAME))->getDevice();
	for (IRenderStep* subpass : steps)
		((Subpass*)subpass)->destroy(dev);
	vkDestroyRenderPass(dev.getHandle(), pass, VK_NULL_HANDLE);
	pipelineProducer.destroy(dev);
}

RenderPass::~RenderPass()
{
}

#include "Device.h"
#include "GraphicsExceptions.h"

#include "CommandBuffer.h"

CommandBufferRecord::CommandBufferRecord() : buf(nullptr), pipeline(nullptr)
{
}

VkCommandBuffer CommandBufferRecord::getCommandBuffer() const
{
	VkCommandBuffer cmdBuf = buf->cmdBuf;
	return cmdBuf;
}
Pipeline* CommandBufferRecord::getCurrentPipeline()
{
	return pipeline;
}
void CommandBufferRecord::setCurrentPipeline(Pipeline* pipeline)
{
	this->pipeline = pipeline;
}

CommandBufferRecord::~CommandBufferRecord()
{
	if (buf != nullptr)
	{
		vkEndCommandBuffer(buf->cmdBuf);
		buf->bufferState = BufferState::READY;
	}
}
CommandBufferRecord::CommandBufferRecord(CommandBuffer* buf, VkCommandBufferUsageFlags flags, const VkFramebuffer& frame, const VkRenderPass& pass, const uint32_t subpass) : buf(buf)
{
	VkCommandBufferInheritanceInfo inheritance{};
	inheritance.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
	inheritance.pNext = nullptr;

	inheritance.framebuffer = frame;
	inheritance.renderPass = pass;
	inheritance.subpass = subpass;

	inheritance.occlusionQueryEnable = VK_FALSE;
	inheritance.queryFlags = 0;
	inheritance.pipelineStatistics = 0;

	VkCommandBufferBeginInfo beginInfo;
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.pNext = nullptr;
	beginInfo.pInheritanceInfo = &inheritance;
	beginInfo.flags = flags;

	vkBeginCommandBuffer(buf->cmdBuf, &beginInfo);
}

CommandBuffer::CommandBuffer() :
	bufferState(INVALID),
	primary(true),
	recorded(false)
{
}

void CommandBuffer::construct(Device& dev, bool primary, uint32_t)
{
	this->primary = primary;
	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = dev.getGraphicsQueue().getCommandPoolHandle();
	allocInfo.level = primary ? VK_COMMAND_BUFFER_LEVEL_PRIMARY : VK_COMMAND_BUFFER_LEVEL_SECONDARY;
	allocInfo.commandBufferCount = 1;

	if (vkAllocateCommandBuffers(dev.getHandle(), &allocInfo, &cmdBuf) != VK_SUCCESS) {
		throw VulkanConstructionError("failed to allocate command buffers!");
	}
	bufferState = READY;
}

void CommandBuffer::destroy(Device& dev)
{
	if (bufferState == INVALID)
		return;
	vkResetCommandBuffer(cmdBuf, 0);
	vkFreeCommandBuffers(dev.getHandle(), dev.getGraphicsQueue().getCommandPoolHandle(), 1, &cmdBuf);
}

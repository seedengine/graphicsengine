#include <vulkan\vulkan.h>

#include "UtilStruct.h"
#include "PipelineLayout.h"

class ShaderModule;
class IPipeline;
class Device;
class IRenderSystem;
class Surface;

#pragma once
class PipelineFactory
{
public:
	PipelineFactory();

	void construct(Device&);


	PipelineFactory& setVertexInputStructure(VertexStructure);
	PipelineFactory& useDefaultEmptyInput();
	PipelineFactory& useDefaultVertexInput3D();
	PipelineFactory& useDefaultVertexInput2D();

	PipelineFactory& setInputAssemblyStructure(AssemblyStructure);
	PipelineFactory& useDefaultInputAssembly();

	PipelineFactory& addShader(ShaderModule&, VkShaderStageFlagBits);

	PipelineFactory& setSurfaceViewport(Surface*);

	PipelineFactory& setLineWidth(float width);
	PipelineFactory& setFillMode(VkPolygonMode mode);
	PipelineFactory& setFragmentCulling(VkCullModeFlags, VkFrontFace);
	PipelineFactory& enableDepthBias();
	PipelineFactory& disableDepthBias();
	PipelineFactory& setDepthBias(float biasConstant, float biasSlope);
	PipelineFactory& enableDepthClamp();
	PipelineFactory& disableDepthClamp();
	PipelineFactory& setDepthClamp(float depthClamp);

	PipelineFactory& setShadingSampleCount(float count);
	
	PipelineFactory& enableDepthCheck();
	PipelineFactory& disableDepthCheck();
	PipelineFactory& enableDepthWrite();
	PipelineFactory& disableDepthWrite();
	PipelineFactory& setDepthComparisonOperation(VkCompareOp);
	PipelineFactory& setDepthBounds(float min, float max);
	PipelineFactory& enableStencil();
	PipelineFactory& disableStencil();
	PipelineFactory& setStencilTest(VkStencilOpState front, VkStencilOpState back);

	PipelineFactory& setColorBlendingOperation(VkLogicOp);
	PipelineFactory& setColorAttachments(std::vector<Compressor::ColorBlendAttachment>&&);
	PipelineFactory& setBlendConstants( float, float, float, float);

	PipelineFactory& setPipelineDynamicStates(std::vector<VkDynamicState>&);

	PipelineFactory& setPipelineLayout(PipelineLayout&& layout);

	PipelineFactory& setSubpass(uint32_t);

	PipelineFactory& build(Device& dev, IRenderSystem* renderPass, IPipeline* pipeline, uint32_t& pipelineID);
	PipelineFactory& reset();

	VkPipelineCache& getPipelineCache() { return cache; }
	VkGraphicsPipelineCreateInfo generateInfoFromDataFile(Compressor::PipelineData);

	void destroy(Device&);

	~PipelineFactory();
private:
	std::atomic<uint32_t> pipelineCount;

	VertexStructure structure;
	std::vector<VkViewport> viewports;
	std::vector<VkRect2D> scissors;
	std::vector<VkPipelineColorBlendAttachmentState> blendingAttachments;
	std::vector<VkDynamicState> dynamicStates;
	std::vector<VkPipelineShaderStageCreateInfo> shaderStages;
	std::vector<VkPushConstantRange> pushConstants;
	
	
	VkPipelineVertexInputStateCreateInfo vertexStructure;
	VkPipelineInputAssemblyStateCreateInfo inputAssembly;
	VkPipelineTessellationStateCreateInfo tesselation;
	VkPipelineViewportStateCreateInfo pipelineViewport;
	VkPipelineRasterizationStateCreateInfo rasterizationState;
	VkPipelineMultisampleStateCreateInfo multisampleStage;
	VkPipelineDepthStencilStateCreateInfo depthStencilState;
	VkPipelineColorBlendStateCreateInfo  colorBlending;
	VkPipelineDynamicStateCreateInfo pipelineDynamics;
	PipelineLayout layout;
	VkGraphicsPipelineCreateInfo info;

	VkPhysicalDeviceFeatures features;
	VkPipelineCache cache;
};


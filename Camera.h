#include "ICamera.h"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_FORCE_RIGHT_HANDED

#include <glm\glm.hpp>


class ComponentInterface;
class Device;
class RenderPass;
class CommandBufferRecord;
class DescriptorSet;

#pragma once
class Camera : public ICamera
{
#pragma pack(push, 1)
	struct MVP
	{
		glm::mat4 TransMat;
		glm::mat4 ProjMat;
		glm::vec3 Eye;
	};
#pragma pack(pop)
public:
	Camera(ComponentInterface* engine);

	virtual void translate(glm::vec3);
	virtual void rotate(double x, double y);
	virtual glm::vec3 getPos() { return _pos; }
	virtual glm::vec3 getForward() { return forward; }
	virtual glm::vec3 getRight() { return right; }
	virtual glm::vec3 getUp() { return up; }

	virtual void setPos(glm::vec3);
	virtual void setForward(glm::vec3);

	virtual void operator()(ComponentInterface*, const EventBase* evt);

	~Camera();

protected:
	virtual inline void listRequirements(std::set<uint64_t>& req);
private:
	void updateVectorsFromRotation();

	DescriptorSet* mvpDescriptor;
	MemoryRange mvpID;

	MVP mvp;
	bool initialized;


	double lastX, lastY;
	double XAngle, YAngle;
	glm::vec3 _pos;
	glm::vec3 forward;
	glm::vec3 up;
	glm::vec3 right;

};


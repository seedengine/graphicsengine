#include <vulkan/vulkan.h>

#include "VulkanInstanceFactory.h"
#include "CoreEngine/IHandle.h"


class Log;

//Class wrapping the VkInstance if the Vulkan Library. Handles Instance Debug Callbacks.
#pragma once
class Instance : public IHandle<VkInstance>
{
	friend void VulkanInstanceFactory::buildInstance(Instance&);
public:
	Instance();

	void construct(VkInstanceCreateInfo&);
	void destroy();

	const VkInstance& getHandle() const;

	~Instance();
private:
	void buildDebugCallback(Log* logger);

	VkInstance instance;
	VkDebugReportCallbackEXT debugCallback;

	static Log* logger;
	static VKAPI_ATTR VkBool32 VKAPI_CALL VKDebugCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType, uint64_t obj, size_t location, int32_t code, const char * layerPrefix, const char * msg, void * userData);
};


#pragma once
#include <utility>
#include <vector>
#include "CoreEngine\IEngine.h"
#include "CoreEngine\OrderedList.h"

#include "Subpass.h"
#include "DepthImage.h"
#include "RenderPass.h"
#include "ILight.h"
#include "ShadowRenderer.h"

class DescriptorSet;
class LightRenderer;

class ShadowRenderSystem : public RenderPass
{
	class StepProxy : public Subpass
	{
	public:
		StepProxy(ShadowRenderSystem* parent) : Subpass(VK_PIPELINE_BIND_POINT_GRAPHICS, 0), parent(parent) {}

		virtual void executeStep(IRenderContext* context) { parent->executePass(context); }
		virtual void render(IRenderContext*) {}
	private:
		ShadowRenderSystem* parent;
	} proxy;
public:
	ShadowRenderSystem(LightRenderer*, ComponentInterface*);

	void constructPipelines(Surface*);
	void setupRenderPass(Device*, DepthImage*);
	void constructDescriptors(DescriptorSet* set) { shadowSet = set; }
	void setLightData(ILight* light) { this->light = light; }
	void executePass(IRenderContext*);
protected:
	virtual VkSubpassContents getRenderMethod() { return VK_SUBPASS_CONTENTS_INLINE; }
private:
	IPipeline* dirShadowPipeline;

	ILight* light;
	LightRenderer* host;
	DescriptorSet* shadowSet;
	ComponentInterface* core;
};

class LightRenderSystem : public RenderPass
{
	class StepProxy : public Subpass
	{
	public:
		StepProxy(LightRenderSystem* parent) : Subpass(VK_PIPELINE_BIND_POINT_GRAPHICS, 0), parent(parent) {}

		virtual void executeStep(IRenderContext* context) { parent->executePass(context); }
		virtual void render(IRenderContext*) {}
	private:
		LightRenderSystem* parent;
	} proxy;
public:
	LightRenderSystem(LightRenderer*, ComponentInterface*);

	void constructPipelines(Surface*);
	void setupRenderPass() { proxy.addColorAttachment(0); proxy.setStencilAttachment(1); addRenderStep(&proxy); }
	void setupSampler(Device*, DepthImage*);
	void constructDescriptors(DescriptorSet* set, DescriptorSet* shadow);
	void setLightData(ILight* light) { this->light = light; }
	void executePass(IRenderContext*);
protected:
	virtual VkSubpassContents getRenderMethod() { return VK_SUBPASS_CONTENTS_INLINE; }
private:
	IPipeline* ambientLightingPipeline;
	IPipeline* directionalLightingPipeline;
	IPipeline* pointLightingPipeline;
	IPipeline* spotLightingPipeline;
	VkSampler sampler;

	ILight* light;
	DescriptorSet* lightSet;
	DescriptorSet* shadowMapSet;
	MemoryRange samplerDescriptor;
	ComponentInterface* core;
	LightRenderer* host;
};


class LightRenderer :
	public ILightRenderer,
	public EventHandler
{
public:
	LightRenderer(ComponentInterface* core);

	virtual void operator()(ComponentInterface* core, const EventBase* evt);
	virtual void updateData(ILight* id);

	virtual void registerLight(ILight*);
	virtual void unregisterLight(ILight*);

	size_t getCount() { return lights.size(); }
	ILight* getLight(uint32_t id);

	uint32_t getShadowAttachmentReference() { return depthVal; }

	virtual void render(CommandBufferRecord&, uint32_t);

private: //lazy initialization?
	ShadowRenderSystem shadowStep;
	LightRenderSystem lightStep;

	DepthImage shadowMap;
	uint32_t depthVal;
	Attachment shadowAttachment;
	ShadowRenderer shadowRenderer;

	Surface* surface;

	std::atomic_bool setupComplete;

	DescriptorSet* lightSet;
	DescriptorSet* shadowSet; //binding dor the light matrix
	DescriptorSet* shadowMapSet;
	MemoryRange samplerDescriptor;

	OrderedList<ILight*, Identifier::cmp<ILight*>> lights;
};

extern "C" __declspec(dllexport) ILightRenderer* __cdecl createLightRenderer(ComponentInterface* core);
extern "C" __declspec(dllexport) void __cdecl destroyLightRenderer(IRenderStep*);

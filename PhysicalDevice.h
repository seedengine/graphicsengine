#include <vulkan\vulkan.h>
#include <vector>
#include "CoreEngine/IHandle.h"


class Instance;
class Surface;

//Class containing data required for evaluating physical devices available and storing their data
#pragma once
class PhysicalDevice : 
	public IHandle<VkPhysicalDevice>
{
	friend class Surface;
public:
	class QueueFamily {
	public:
		QueueFamily(unsigned id, bool presentationSup, VkQueueFamilyProperties& properties) : 
			id(id), 
			familySize(properties.queueCount), 
			presentationSupport(presentationSup), 
			type(properties.queueFlags)
		{}
		QueueFamily() : 
			id(-1), 
			familySize(0), 
			type(0) 
		{}

		bool isGraphicQueueFamily() const { return type & VK_QUEUE_GRAPHICS_BIT ? true : false; }
		bool isComputeQueueFamily() const { return type & VK_QUEUE_COMPUTE_BIT ? true : false; }
		bool isTransferQueueFamily() const { return type & VK_QUEUE_TRANSFER_BIT ? true : false; }
		bool isType(int type) const { return this->type & type; }
		int getType() const { return type; }
		bool isPresentationQueue() { return presentationSupport; }

		unsigned getID() const { return id; }
		int getSize() const { return familySize; }

		~QueueFamily() {}
	private:
		unsigned id;
		unsigned familySize; 
		bool presentationSupport;
		int type;
	};

	PhysicalDevice() : score(-1), deviceQueues() {}
	PhysicalDevice(PhysicalDevice&& other);

	PhysicalDevice& operator=(const PhysicalDevice& dev) = delete;
	PhysicalDevice& operator=(PhysicalDevice&& other);


	void construct(VkPhysicalDevice&&);


	const VkPhysicalDevice& getHandle() const { return handle; }

	~PhysicalDevice();

	QueueFamily findFirstQueueFamily(int type);
	QueueFamily getPresentationQueue();

	const VkPhysicalDeviceLimits& getDeviceLimits() const
	{
		return properties.limits;
	}
	const VkPhysicalDeviceFeatures& getDeviceFeatures() const
	{
		return features;
	}

	static void scanDevices(Instance&, Surface&);
	static PhysicalDevice* getBestFreeDevice();

private:
	void evaluateDevice(Surface& surface);
	void readQueueFamilies(Surface&);

	static bool isDeviceSuitable(PhysicalDevice& dev, Surface& surface);

	static uint32_t insert(PhysicalDevice&& obj, size_t start = 0, size_t end = -1)
	{
		if (end == -1)
			end = devList.size();
		uint64_t score = obj.score;
		size_t range = end - start;

		if (devList.empty() || (devList.at(devList.size() - 1)).score < score)
		{
			devList.push_back(static_cast<PhysicalDevice&&>(obj));
			return 0;
		}
		else if (start + 1 == end && devList.at(start).score < score && devList.at(end).score > score)
		{
			devList.insert(devList.begin() + start, static_cast<PhysicalDevice&&>(obj));
			return (uint32_t)start;
		}
		else
		{
			if (devList.at(start + (range / 2)).score > score)
				return insert(static_cast<PhysicalDevice&&>(obj), start, start + (range / 2));
			else
				return insert(static_cast<PhysicalDevice&&>(obj), start, start + (range / 2));
		}

	}


	VkPhysicalDevice handle;
	unsigned score;
	std::vector<QueueFamily> deviceQueues;
	VkPhysicalDeviceFeatures features;
	VkPhysicalDeviceProperties properties;


	static std::vector<PhysicalDevice> devList;
};

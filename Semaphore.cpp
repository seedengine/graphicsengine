#include "Semaphore.h"
#include "GraphicsExceptions.h"
#include "Device.h"

void Semaphore::construct(Device & dev)
{
	VkSemaphoreCreateInfo semaphoreInfo{};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	if (vkCreateSemaphore(dev.getHandle(), &semaphoreInfo, VK_NULL_HANDLE, &semaphore) != VK_SUCCESS)
		throw VulkanConstructionError("failed to create semaphores!");
}


const VkSemaphore& Semaphore::getHandle() const 
{
	return semaphore;
}

void Semaphore::destroy(Device & dev)
{
	vkDestroySemaphore(dev.getHandle(), semaphore, VK_NULL_HANDLE);
}
#include <vector>
#include <mutex>
#include <set>
#include "CoreEngine\Identifier.h"
#include "CoreEngine\OrderedList.h"

#pragma once

#ifndef RENDER_INTERFACE
	#define RENDER_INTERFACE

	class IRenderSystem;
	class IPipeline;

	class IRenderComponent
	{
	public:
		//structural modification of data
		virtual void* getComponentData() = 0;

		virtual void construct() {}
		virtual void init() {}
		virtual void destroy() {}
		//functional modification of data
		virtual void prepareRender() {}
		virtual void endRender() {}
	};

	class IRenderContext
	{
	public:
		virtual void* getContext() = 0;
		virtual void* getExtendedData() = 0;
		virtual const uint32_t getSubpassID() const = 0;
		virtual const uint32_t getPipelineID() const = 0;
	};

	class IRenderStep : public Identifier
	{
		friend class IRenderSystem;
	public:
		virtual void* getStepData() = 0;
		virtual void render(IRenderContext*) = 0;
		virtual void executeStep(IRenderContext*) = 0;
		const IRenderSystem* getParent() const { return parent; }
		void registerPipeline(IPipeline* pipeline);
	protected:

		virtual void update() = 0;

		std::set<uint32_t> pipelines;
	private:
		IRenderSystem* parent;
	};



	class IRenderSystem
	{
	public:
		typedef uint32_t ComponentRef;
		typedef uint32_t StepRef;

		virtual ComponentRef addRenderComponent(IRenderComponent* component);
		virtual StepRef addRenderStep(IRenderStep* step);
		IRenderStep* getRenderStep(const uint32_t stepRef);

		void setClearVal(bool val) { shouldClear = val; }

		virtual void render(IRenderContext* context);

		virtual void destroy() = 0;

		void activate() { isActive = true; }
		void deactivate() { isActive = false; }
		bool getActiveState() { return isActive; }
	protected:
		bool isActive = true;
		static constexpr bool compare( IRenderStep*const& l,  IRenderStep*const& r) { return r->getID() < l->getID(); }

		bool shouldClear;
		std::vector<IRenderComponent*> components;
		OrderedList<IRenderStep*, compare> steps;

	};
#endif

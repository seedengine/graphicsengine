#include "LightRenderer.h"

#include "Light.h"

#include "Pipeline.h"
#include "GraphicsEngine.h"
#include "CoreEngine\IEngine.h"
#include "GraphicsModuleAction.h"
#include "CoreEngine\OrderedList.hpp"
#include "UserInterface\IUserInterface.h"

LightRenderer::LightRenderer(ComponentInterface * core) :
	EventHandler(core),
	shadowRenderer(core),
	surface(nullptr),
	shadowMap(core, 800, 600),
	shadowStep(this, core),
	lightStep(this, core)
{
	core->getEventBus()->registerHandler(this, VulkanDeviceConstructionEvent::getClassID());
	core->getEventBus()->registerHandler(this, RenderSystemSetupEvent::getClassID());
	core->getEventBus()->registerHandler(this, RenderSystemInitEvent::getClassID());
	core->getEventBus()->registerHandler(this, DescriptorRegistrationEvent::getClassID());
	core->getEventBus()->registerHandler(this, ExecuteRenderPass::getClassID());
	core->getEventBus()->registerHandler(this, RecordSubpassEvent::getClassID());
}

void LightRenderer::operator()(ComponentInterface * core, const EventBase * evt)
{
	if (evt->is<VulkanDeviceConstructionEvent>())
	{
		VulkanDeviceConstructionEvent* data = (VulkanDeviceConstructionEvent*)evt;

		shadowMap.construct();

	}
	else if (evt->is<RenderSystemSetupEvent>())
	{
		RenderSystemSetupEvent* data = (RenderSystemSetupEvent*)evt;
		surface = data->getSurface();

		GraphicsEngine* engine = (GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME);
		Device& dev = engine->getDevice();

		FramebufferAttachmentProviderAttachment att;
		att.provider = &engine->getSwapChain();
		att.shouldClear = false;
		lightStep.addProvider(att);
		lightStep.addRenderComponent(engine->getDepthMapAttachment());

		shadowStep.constructDescriptors(shadowSet);
		lightStep.constructDescriptors(lightSet, shadowMapSet);

		shadowStep.setupRenderPass(&dev, &shadowMap);
		lightStep.setupRenderPass();
	}
	else if (evt->is<RenderSystemInitEvent>())
	{
		shadowStep.construct(surface);
		lightStep.construct(surface);
		for (auto& light : lights)
		{
			MemoryRange lightmem = lightSet->allocate(((BaseLight*)light)->getDataSize());
			MemoryRange shadowmem = shadowSet->allocate(sizeof(LightTransform));

			light->assign(lightmem, shadowmem);
			((BaseLight*)light)->writeData(lightSet, shadowSet);

		}
		lightStep.setupSampler(&((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice(), &shadowMap);
	}
	else if (evt->is<DescriptorRegistrationEvent>())
	{
		DescriptorRegistrationEvent* data = (DescriptorRegistrationEvent*)evt;
		data->getDescriptorFactory()->registerBinding(core, 2, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 0, BaseLight::getLightSize());
		data->getDescriptorFactory()->registerBinding(core, 2, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, DirectionalLight::getLightSize());
		data->getDescriptorFactory()->registerBinding(core, 2, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 2, PointLight::getLightSize());
		data->getDescriptorFactory()->registerBinding(core, 2, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 3, SpotLight::getLightSize());


		data->getDescriptorFactory()->registerBinding(core, 5, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 0, sizeof(LightTransform));

		data->getDescriptorFactory()->registerBinding(core, 4, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 0, 0);

		shadowSet = data->getDescriptorFactory()->getDescriptorSet(core, 5);
		lightSet = data->getDescriptorFactory()->getDescriptorSet(core, 2);
		shadowMapSet = data->getDescriptorFactory()->getDescriptorSet(core, 4);
	}
	else if (evt->is<ExecuteRenderPass>())
	{
		ExecuteRenderPass* data = (ExecuteRenderPass*)evt;

		render(*data->getCommandBuffer(), data->getBufferID());
	}
	else if (evt->is<RecordSubpassEvent>())
	{
		shadowStep.constructPipelines(surface);
		lightStep.constructPipelines(surface);

	}
}

void LightRenderer::updateData(ILight* id)
{
	ILight* tmp = id;
	size_t data = lights.getPos(tmp);

	((BaseLight*)lights[data])->writeData(lightSet, shadowSet);

	lightSet->update();
}

void LightRenderer::registerLight(ILight* obj)
{
	if (setupComplete)
	{
		ILight* data;
		data = (BaseLight*)obj;
		data->assign(lightSet->allocate(((BaseLight*)data)->getDataSize()), shadowSet->allocate(sizeof(LightTransform)));
		size_t pos = lights.insert(data);

		((BaseLight*)lights[pos])->writeData(lightSet, shadowSet);

		lightSet->update();
		shadowStep.setLightData(data);
		lightStep.setLightData(data);
	}
	else
	{
		lights.insert(obj);
	}
}

void LightRenderer::unregisterLight(ILight* data)
{
	lights.erase(lights.getPos(data));
}

ILight * LightRenderer::getLight(uint32_t id)
{
	return lights.find(id);
}

void LightRenderer::render(CommandBufferRecord& cmdBuf, uint32_t bufferID)
{
	//shadowMap.reset(cmdBuf);
	for (auto light : lights)
	{
		//init light data in steps
		shadowStep.setLightData(light);
		lightStep.setLightData(light);

		shadowStep.render(cmdBuf, bufferID); //render to framebuffer
		cmdBuf.resetSets();
		shadowSet->bind(&cmdBuf, light->getShadowData());
		lightStep.render(cmdBuf, bufferID);
	}
}

ILightRenderer * __cdecl createLightRenderer(ComponentInterface * core)
{
	return new LightRenderer(core);
}

void __cdecl destroyLightRenderer(IRenderStep* ptr)
{
	delete (LightRenderer*)ptr;
}



LightRenderSystem::LightRenderSystem(LightRenderer* host, ComponentInterface* core) : 
	RenderPass(core, false),
	core(core),
	proxy(this),
	ambientLightingPipeline(new Pipeline(core, "../Assets/Shader/Light/ambient.shdr")),
	directionalLightingPipeline(new Pipeline(core, "../Assets/Shader/Light/directional.shdr")),
	pointLightingPipeline(new Pipeline(core, "../Assets/Shader/Light/point.shdr")),
	spotLightingPipeline(new Pipeline(core, "../Assets/Shader/Light/spot.shdr"))
{
	setClearVal(false);
}

void LightRenderSystem::setupSampler(Device* dev, DepthImage* shadowMap)
{
	VkSamplerCreateInfo samplerInfo = {};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.magFilter = VK_FILTER_LINEAR;
	samplerInfo.minFilter = VK_FILTER_LINEAR;
	samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
	samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
	samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
	samplerInfo.anisotropyEnable = VK_FALSE;
	samplerInfo.maxAnisotropy = 1;
	samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
	samplerInfo.unnormalizedCoordinates = VK_FALSE;
	samplerInfo.compareEnable = VK_FALSE;
	samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
	samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	samplerInfo.mipLodBias = 0.0f;
	samplerInfo.maxAnisotropy = 1.0f;
	samplerInfo.minLod = 0.0f;
	samplerInfo.maxLod = 1.0f;
	samplerInfo.borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE;
	if (vkCreateSampler(*dev, &samplerInfo, nullptr, &sampler) != VK_SUCCESS) {
		throw std::runtime_error("failed to create texture sampler!");
	}
	samplerDescriptor = shadowMapSet->allocate(0);
	ImageSampler d;
	d.img = (Image*)&shadowMap->getImageObject();
	d.sampler = &sampler;
	d.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
	shadowMapSet->set(samplerDescriptor, &d);
}

void LightRenderSystem::constructPipelines(Surface* surface)
{
	PipelineLayout layout;
	uint32_t id = proxy.getID();
	layout.construct(core, ((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice(), { 0, 1, 2, 3, 4, 5 }, { VkPushConstantRange{VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(glm::vec3)} });
	((Pipeline*)ambientLightingPipeline)->construct(((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice(), this, id, surface, std::move(layout));
	((Pipeline*)directionalLightingPipeline)->construct(((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice(), this, id, surface, std::move(layout));
	((Pipeline*)pointLightingPipeline)->construct(((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice(), this, id, surface, std::move(layout));

	/*
	std::function<void(PipelineFactory&)> transform = [](PipelineFactory& fac) {
		VkVertexInputAttributeDescription attrib[] = {
			VkVertexInputAttributeDescription{ 0, 0, VK_FORMAT_R32G32B32_SFLOAT, 0 },	//Position
			VkVertexInputAttributeDescription{ 1, 0, VK_FORMAT_R32G32_SFLOAT, 12 },		//Texture Coord
			VkVertexInputAttributeDescription{ 2, 0, VK_FORMAT_R32G32B32_SFLOAT, 20 },	//Normal Coord
			VkVertexInputAttributeDescription{ 3, 0, VK_FORMAT_R32G32B32_SFLOAT, 32 }	//Shadow Coord
		};
		fac.setVertexInputStructure(
			VertexStructure{
				std::vector<VkVertexInputAttributeDescription>(std::begin(attrib), std::end(attrib)),
				std::vector<VkVertexInputBindingDescription>(1,
				VkVertexInputBindingDescription{ 0, 32, VK_VERTEX_INPUT_RATE_VERTEX }
				)
			}
		);
	};
	*/

	((Pipeline*)spotLightingPipeline)->construct(((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice(), this, id, surface, std::move(layout));
}

void LightRenderSystem::constructDescriptors(DescriptorSet * set, DescriptorSet * shadow)
{
	lightSet = set;
	shadowMapSet = shadow;
}

void LightRenderSystem::executePass(IRenderContext* context)
{
	uint32_t width, height;
	((IUserInterface*)core->getModule("UserInterface"))->getWindowSize(width, height);

	VkViewport viewport = VkViewport{ 0.0f, 0.0f, float(width), float(height), 0.0f, 1.0f };
	vkCmdSetViewport(((CommandBufferRecord*)context->getContext())->getCommandBuffer(), 0, 1, &viewport);
	VkRect2D scissors = { VkOffset2D{ 0, 0 }, VkExtent2D{ width, height} };
	vkCmdSetScissor(((CommandBufferRecord*)context->getContext())->getCommandBuffer(), 0, 1, &scissors);

	switch (((BaseLight*)light)->getDataSize())
	{
	case BaseLight::getLightSize():
		ambientLightingPipeline->bind(context);
		break;
	case DirectionalLight::getLightSize():
		directionalLightingPipeline->bind(context);
		break;
	case PointLight::getLightSize():
		pointLightingPipeline->bind(context);
		break;
	case SpotLight::getLightSize():
		spotLightingPipeline->bind(context);
		break;
	}
	lightSet->bind((CommandBufferRecord*)context->getContext(), light->getAssignedData());
	shadowMapSet->bind((CommandBufferRecord*)context->getContext(), samplerDescriptor);

	RecordEvent evt;
	evt.setRenderContext(context);

	core->getEventBus()->invokeEventSynchonous<RecordEvent>(core, evt);
}



ShadowRenderSystem::ShadowRenderSystem(LightRenderer* host, ComponentInterface* core) :
	RenderPass(core, false),
	proxy(this),
	core(core),
	dirShadowPipeline(new Pipeline(core, "../Assets/Shader/Light/shadow/ShadowPipeline.shdr"))
{
	setClearVal(true);
}

void ShadowRenderSystem::constructPipelines(Surface* surface)
{
	PipelineLayout layout;
	layout.construct(core, ((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice(), { 0, 1, 2, 3, 4, 5 }, { VkPushConstantRange{VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(glm::vec3)} });
	
	((Pipeline*)dirShadowPipeline)->construct(((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice(), this, proxy.getID()/*Subpass*/, surface, std::move(layout));
}

void ShadowRenderSystem::setupRenderPass(Device *, DepthImage* shadowMap)
{

	auto depthVal = addRenderComponent((Attachment*)shadowMap->getComponentData());


	//proxy.addColorAttachment(0);
	proxy.setStencilAttachment(depthVal); //to offset providers

	Dependency depStart0;
	Dependency depEnd0;

	depStart0.pass = externalSubpass;
	depEnd0.pass = 0;
	depStart0.stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
	depEnd0.stage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
	depStart0.access = VK_ACCESS_SHADER_READ_BIT;
	depEnd0.access = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

	registerDependency(depStart0, depEnd0);

	Dependency depStart1;
	Dependency depEnd1;

	depStart1.pass = 0;
	depEnd1.pass = externalSubpass;
	depStart1.stage = VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
	depEnd1.stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
	depStart1.access = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
	depEnd1.access = VK_ACCESS_SHADER_READ_BIT;

	registerDependency(depStart1, depEnd1);

	addRenderStep(&proxy);
}

void ShadowRenderSystem::executePass(IRenderContext* context)
{
	uint32_t width, height;
	((IUserInterface*)core->getModule("UserInterface"))->getWindowSize(width, height);

	VkViewport viewport = VkViewport{ 0.0f, 0.0f, float(width), float(height), 0.0f, 1.0f };
	vkCmdSetViewport(((CommandBufferRecord*)context->getContext())->getCommandBuffer(), 0, 1, &viewport);
	VkRect2D scissors = { VkOffset2D{ 0, 0 }, VkExtent2D{ width, height} };
	vkCmdSetScissor(((CommandBufferRecord*)context->getContext())->getCommandBuffer(), 0, 1, &scissors);

	dirShadowPipeline->bind(context);

	shadowSet->bind((CommandBufferRecord*)context->getContext(), light->getShadowData());

	RecordEvent evt;
	evt.setRenderContext(context);
	core->getEventBus()->invokeEventSynchonous<RecordEvent>(core, evt);
}

#include <vulkan\vulkan.h>

#include <vector>
#include <string>
#include <set>

#include "PhysicalDevice.h"

class Device;
class Log;
class Instance;
class ComponentInterface;

#pragma once
class VulkanDeviceFactory
{
public:
	VulkanDeviceFactory(const VulkanDeviceFactory& other) = delete;
	VulkanDeviceFactory(ComponentInterface* logger);
	~VulkanDeviceFactory();

	void setActivationLayers(const char** layers, size_t count);
	bool validateExtensionList(const char** extensions, size_t count) const;
	bool isExtensionAvailable(const char*) const;
	void setDeviceExtensions(const char** extensions, size_t count);

	void buildDevice(Device&);

	void readPhysicalDevices(Instance&, Surface&);
private:
	void selectPhysicalDevice();


	ComponentInterface* core;
	Log* logger;

	std::vector<VkDeviceQueueCreateInfo> queueInfo;
	VkPhysicalDeviceFeatures deviceFeature;
	VkDeviceCreateInfo deviceInfo;
	VkPhysicalDeviceFeatures2 featureExtInfo;
	VkPhysicalDeviceDescriptorIndexingFeaturesEXT extInfo;
	std::set<std::string> availableExtensions;
	std::set<std::string> availableLayers;

	PhysicalDevice::QueueFamily graphicsQueueObj;
	PhysicalDevice::QueueFamily computeQueueObj;
	PhysicalDevice::QueueFamily transferQueueObj;

	PhysicalDevice* dev;

	const static float defaultPriority;
};

#include <vulkan\vulkan.h>
#include "CoreEngine\IHandle.h"
#include "Device.h"
#include "GraphicsExceptions.h"

class Device;

#pragma once
class Image
{
public:
	Image();
	Image(VkImage&, uint32_t, uint32_t, VkFormat);

	virtual void construct(Device&, uint32_t width, uint32_t height, VkFormat format);

	virtual void init(Device&);

	virtual void destroy(Device&);

	~Image();

	const VkFormat& getFormat() const { return format; }
	const uint32_t& getWidth() const { return width; }
	const uint32_t& getHeight() const { return height; }
	const VkImage& getImage() const { return tex; }
	const VkDeviceMemory& getImageMemory() const { return texMem; }
	VkImageView getImageView() const { return view; }
	virtual const VkSampler getSampler() const { return VK_NULL_HANDLE; }
protected:
	void allocateMemory(Device &dev);
	virtual void createImage(Device &dev);
	virtual void createImageView(Device &dev);

	void setImage(VkImage&& image) { tex = image; }
	void setImageView(VkImageView&& view) { this->view = view; }
	void setFormat(const VkFormat& format) { this->format = format; }
	void setWidth(const uint32_t& width) { this->width = width; }
	void setHeight(const uint32_t& height) { this->height = height; }
	bool ownsImage;
private:
	uint32_t width, height;
	VkFormat format;
	VkDeviceMemory texMem;
	VkImage tex;
	VkImageView view;


	uint32_t getMemoryType(Device& dev, uint32_t typeFilter, VkMemoryPropertyFlags properties)
	{
		VkPhysicalDeviceMemoryProperties memProperties;
		vkGetPhysicalDeviceMemoryProperties(dev.getPhysicalDevice()->getHandle(), &memProperties);
		for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++)
			if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties)
				return i;
			else continue;

		throw VulkanApplicationError("failed to find suitable memory type!");
	}
};


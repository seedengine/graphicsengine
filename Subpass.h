#include <vulkan\vulkan.h>
#include <vector>

#include "CoreEngine/ISwappable.h"
#include "RenderContext.h"
#include "CommandBuffer.h"
#include "UtilStruct.h"

//Add ID and pass it through recording
class RenderPass;

#pragma once
class Subpass : public IRenderStep,
	public IHandle<CommandBuffer>
{
public:
	Subpass(VkPipelineBindPoint, uint32_t commandBufferCount = 1);

	virtual void construct(Device&, uint32_t);

	const VkSubpassDescription buildInfo() const
	{
		VkSubpassDescription info{};

		info.flags = 0;

		info.pipelineBindPoint = bindPt;
		
		info.colorAttachmentCount = static_cast<uint32_t>(colorReferences.size());
		info.pColorAttachments = colorReferences.data();
		info.inputAttachmentCount = static_cast<uint32_t>(inputReferences.size());
		info.pInputAttachments = inputReferences.data();

		if (stencilReference.attachment != -1)
		{
			info.pDepthStencilAttachment = &stencilReference;
		}
		else
			info.pDepthStencilAttachment = nullptr;

		//Needed for Multisampling
		info.pResolveAttachments = nullptr;
		info.preserveAttachmentCount = 0;
		info.pPreserveAttachments = nullptr;

		return info;
	}

	const uint32_t getID() { return id; }

	void* getStepData();
	virtual void executeStep(IRenderContext*);

	const CommandBuffer& getHandle() const
	{
		return commands.getCurrent();
	}

	void updateLatestBuffer(RenderPass& pass);
	void updateAll(RenderPass& pass);

	virtual void destroy(Device&);

	~Subpass();

	void addColorAttachment(AttachmentRef reference)
	{
		VkAttachmentReference ref{};
		ref.attachment = reference;
		ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		colorReferences.push_back(ref);
	}
	void addInputReference(AttachmentRef reference)
	{
		VkAttachmentReference ref{};
		ref.attachment = reference;
		ref.layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		inputReferences.push_back(ref);
	}
	void setStencilAttachment(AttachmentRef reference)
	{
		stencilReference.attachment = reference;
		stencilReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
		
	}
protected:
	virtual void update();

	inline constexpr uint32_t getID() const { return id; }
	ISwappable<CommandBuffer> commands;

private:
	uint32_t id;

	VkSubpassDescription desc;
	const VkPipelineBindPoint bindPt;
	std::vector<VkAttachmentReference> colorReferences;
	std::vector<VkAttachmentReference> inputReferences;
	VkAttachmentReference stencilReference;

};

#include "Renderer.h"

#include "UserInterface/IUserInterface.h"
#include "IPipeline.h"
#include "GraphicsExceptions.h"
#include "VulkanGraphicsEvents.h"
#include "CoreEngine\Event.hpp"

Renderer::Renderer(ComponentInterface* engine, std::string pipeline, bool registerEventHandler) : 
	RenderPass(engine),
	core(engine),
	step(engine, pipeline, [this] (IRenderContext* context){this->_render(context); })
{
	if (registerEventHandler)
	{
		engine->getEventBus()->registerHandler(this, RenderSystemSetupEvent::getClassID());
		engine->getEventBus()->registerHandler(this, RenderSystemInitEvent::getClassID());
		engine->getEventBus()->registerHandler(this, PipelineBuildEvent::getClassID());
		engine->getEventBus()->registerHandler(this, VulkanDeviceDestructionEvent::getClassID());
		engine->getEventBus()->registerHandler(this, ExecuteRenderPass::getClassID());
	}
}


void Renderer::operator()(ComponentInterface* core, const EventBase* evt)
{
	if (evt->is<RenderSystemSetupEvent>())
	{
		RenderSystemSetupEvent* data = (RenderSystemSetupEvent*)evt;
		surface = data->getSurface();

		GraphicsEngine* engine = (GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME);
		FramebufferAttachmentProviderAttachment att;
		att.provider = &engine->getSwapChain();
		att.shouldClear = shouldClear;
		addProvider(att);
		addRenderComponent(engine->getDepthMapAttachment());

		this->addRenderStep(&step);
	}
	else if (evt->is<RenderSystemInitEvent>())
	{
		RenderPass::construct(surface);
	}
	else if (evt->is<PipelineBuildEvent>())
	{
		PipelineBuildEvent* data = (PipelineBuildEvent*)evt;
		if (data->getRenderSystem()->getRenderStep(0)->getID() == step.getID())
			step.construct(data->getRenderSystem(), data->getSurface());
	}
	else if (evt->is<VulkanDeviceDestructionEvent>())
	{
		step.destroy();
	}
	else if (evt->is<ExecuteRenderPass>())
	{
		ExecuteRenderPass* data = (ExecuteRenderPass*)evt;

		RenderPass::render(*data->getCommandBuffer(), data->getBufferID());
	}
	else {
		RenderPass::operator()(core, evt);
	}
}


void Renderer::_render(IRenderContext* context)
{
	uint32_t width, height;
	((IUserInterface*)core->getModule("UserInterface"))->getWindowSize(width, height);

	VkViewport viewport = VkViewport{ 0.0f, 0.0f, float(width), float(height), 0.0f, 1.0f };
	vkCmdSetViewport(((CommandBufferRecord*)context->getContext())->getCommandBuffer(), 0, 1, &viewport);
	VkRect2D scissors = { VkOffset2D{ 0, 0 }, VkExtent2D{ width, height} };
	vkCmdSetScissor(((CommandBufferRecord*)context->getContext())->getCommandBuffer(), 0, 1, &scissors);

	RecordEvent evt;
	evt.setRenderContext(context);
	core->getEventBus()->invokeEventSynchonous<RecordEvent>(core, evt);
}

__declspec(dllexport) IRenderSystem* __cdecl createRenderer(ComponentInterface* core, std::string pipeline, bool registerEventHandler)
{
	return new Renderer(core, pipeline, registerEventHandler);
}
__declspec(dllexport) void __cdecl destroyRenderer(IRenderSystem* ptr, ComponentInterface * core)
{
	delete((Renderer*)ptr);
}

Renderer::RenderStep::RenderStep(ComponentInterface* core, std::string pipeline, std::function<void(IRenderContext*)> renderFunc) :
	Subpass(VK_PIPELINE_BIND_POINT_GRAPHICS),
	renderFunc(renderFunc),
	core(core),
	p(new Pipeline(core, pipeline))
{
	range.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
	range.offset = 0;
	range.size = sizeof(glm::vec3);
	addColorAttachment(0); //adds result color
	setStencilAttachment(1);
}

void Renderer::RenderStep::construct(IRenderSystem* renderSystem, Surface* surface)
{
	PipelineLayout layout;
	std::vector<VkPushConstantRange> pushConstants({ range });
	layout.construct(core, ((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice(), { 0, 1 }, pushConstants);
	if (p == nullptr)
		throw VulkanConstructionError("Failed to build Pipeline from Datafile");
	((Pipeline*)p)->construct(((GraphicsEngine*)core->getModule(GRAPHICS_MODULE_NAME))->getDevice(), renderSystem, Subpass::getID(), surface, std::move(layout));

	this->registerPipeline(p);
}

void Renderer::RenderStep::render(IRenderContext* context)
{
	p->bind(context);

	renderFunc(context);
}

void Renderer::RenderStep::destroy()
{
	if (p != nullptr)
	{
		delete p;
		p = nullptr;
	}
}

#include "CoreEngine\IObjectComponent.h"

#pragma once
class ITexture : public IObjectComponent
{
public:
	ITexture(ComponentInterface* core) : IObjectComponent(core)
	{}

protected:
	enum ImageType
	{
		BMP,
		PNG,
		NIL
	};
	enum PixelStructure
	{
		R = 1,
		RA = 2,
		RGB = 3,
		RGBA = 4
	};

	virtual ImageType getFileExtension() = 0;
	virtual ImageType getFileImageType(std::ifstream&) = 0;

};
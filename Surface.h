#include "Instance.h"
#include "PhysicalDevice.h"
#include "UtilStruct.h"
#include "CoreEngine/IHandle.h"

#pragma once
class Surface : 
	public IHandle<VkSurfaceKHR>
{
public:
	Surface();

	void construct(Instance&, ComponentInterface*);

	const VkSurfaceKHR& getHandle() const;
	void readSurfaceProperties(const PhysicalDevice*);
	SurfacePresentationDetails getSurfaceProperties();
	VkSurfaceFormatKHR getImageViewFormat();
	VkExtent2D getExtend();

	void destroy(Instance&);

	~Surface();
private:
	SurfacePresentationDetails surfaceDetails;
	VkSurfaceFormatKHR imageFormat;
	VkExtent2D surfaceExtent;
	VkSurfaceKHR surface;
};


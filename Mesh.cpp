#include "Mesh.h"
#include "GraphicsExceptions.h"
#include "Device.h"
#include "CoreEngine/Util.h"
#include "CoreEngine\IEngine.h"
#include "GraphicsEngine.h"

#include <algorithm>
#include <fstream>
#include <functional>
#include <sstream>
#include <glm\gtc\type_ptr.hpp>
#include "Pipeline.h"


Mesh::Mesh(ComponentInterface* core, uint8_t vertexScale, glm::vec3 offset) : IObjectComponent(core), vertices(vertexScale), path(), offset(offset)
{
}

Mesh::Mesh(ComponentInterface* core, std::string path, glm::vec3 offset) : IObjectComponent(core), path(path), vertices(0), offset(offset)
{
}
Mesh::Mesh(ComponentInterface* core, const char* path, glm::vec3 offset) : IObjectComponent(core), path(path), vertices(0), offset(offset)
{
}

VkBufferUsageFlags Mesh::getBufferRequirements()
{
	return VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
}

size_t Mesh::getDataSize()
{
	return vertices.getByteSize() + indices.size() * sizeof(uint32_t);
}

void Mesh::operator()(ComponentInterface* core, const EventBase* handle)
{
	if (handle->is<LoadEvent>())
	{
		if (!path.empty())
		{
			MeshHeader head;
			head.faceCount = 0;
			head.vertices = 0;

			std::ifstream file(path, std::fstream::in | std::fstream::binary);

			file.read((char*)&head, sizeof(MeshHeader));

			uint32_t vertexCount = head.vertices & ((((uint32_t)1) << 30) - 1);
			uint32_t extendedVertexData = head.vertices >> 30;
			uint32_t vertexSize = 3 + (extendedVertexData & 1 ? 3 : 0) + (extendedVertexData & 2 ? 2 : 0);
			vertices.setVertexSize(8);


			float* vert = (float*)malloc(vertexCount * 8 * sizeof(float));
			memset(vert, 0, vertexCount * 8 * sizeof(float));
			for (uint32_t i = 0; i < vertexCount; i++)
			{
				file.read((char*)&vert[i * 8], 3 * sizeof(float));
				if (extendedVertexData & 2)
					file.read((char*)&vert[i * 8 + 3], 2 * sizeof(float));
				if (extendedVertexData & 1)
					file.read((char*)&vert[i * 8 + 5], 3 * sizeof(float));
			}
			vertices.push(vert, vertexCount * 8);
			free(vert);

			indices.resize(head.faceCount * 3);
			file.read((char*)indices.data(), head.faceCount * 12);

			file.close();
		}
	}
	else if (handle->is<GraphicsMemoryReadyEvent>())
	{
		GraphicsMemoryReadyEvent* evt = (GraphicsMemoryReadyEvent*)handle;
		dataStorage = evt->getDeviceHandle()->getDataMemoryHandler()->allocate(getDataSize());

		void* tmpMem = malloc(dataStorage.size);
		memcpy(tmpMem, vertices.get(), vertices.getByteSize());
		memcpy(((uint8_t*)((size_t)tmpMem) + vertices.getByteSize()), indices.data(), indices.size() * sizeof(uint32_t));
		evt->getDeviceHandle()->getDataMemoryHandler()->set(dataStorage, tmpMem);
		free(tmpMem);
	}
	else if (handle->is<VulkanDeviceDestructionEvent>())
	{
		VulkanDeviceDestructionEvent* evt = (VulkanDeviceDestructionEvent*)handle;
		evt->getDeviceHandle()->getDataMemoryHandler()->clearMemoryRange(dataStorage);
	}
	else if (handle->is<RecordEvent>())
	{
		RecordEvent* evt = (RecordEvent*)handle;
		bindBuffers(((GraphicsEngine*)coreHandler->getModule(GRAPHICS_MODULE_NAME))->getDevice().getDataMemoryHandler(), evt->getRenderContext());
		
		CommandBufferRecord* record = (CommandBufferRecord*)evt->getRenderContext()->getContext();

		vkCmdPushConstants(record->getCommandBuffer(), record->getCurrentPipeline()->getLayout(), VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(glm::vec3), &offset);

		vkCmdDrawIndexed(record->getCommandBuffer(), static_cast<uint32_t>(indices.size()), 1, 0, 0, 0);
	}
}

inline void Mesh::listRequirements(std::set<uint64_t>& req)
{
	req.insert(LoadEvent::getClassID());
	req.insert(VulkanInstanceConstructionEvent::getClassID());
	req.insert(GraphicsMemoryReadyEvent::getClassID());
	req.insert(VulkanDeviceDestructionEvent::getClassID());
	req.insert(RecordEvent::getClassID());
}


Mesh::~Mesh()
{
}

void Mesh::bindBuffers(BlockMemoryManager<BufferMemoryInterface>* buf, IRenderContext* image)
{
	VkDeviceSize offsets[] = { dataStorage.offset };
	VkDeviceSize indexOffset = dataStorage.offset + vertices.getByteSize();
	vkCmdBindVertexBuffers(((CommandBufferRecord*)image->getContext())->getCommandBuffer(), 0, 1, &buf->getInterface()->getBufferHandle(dataStorage), offsets);
	vkCmdBindIndexBuffer(((CommandBufferRecord*)image->getContext())->getCommandBuffer(), buf->getInterface()->getBufferHandle(dataStorage), indexOffset, VK_INDEX_TYPE_UINT32);
}


Mesh::Vertices::Vertices(uint8_t size) : vertexSize(size)
{
}

uint8_t Mesh::Vertices::getVertexSize()
{
	return vertexSize;
}

void Mesh::Vertices::setVertexSize(uint8_t size)
{
	if (values.empty())
		vertexSize = size;
	else
		throw AlignmentError();
}

size_t Mesh::Vertices::size()
{
	return size_t(values.size() / vertexSize);
}

size_t Mesh::Vertices::getByteSize()
{
	return size_t(values.size() * sizeof(float));
}

void Mesh::Vertices::push(float * v, size_t length)
{
	if (length % vertexSize != 0) //protects from bad Memory Alignment
		throw AlignmentError();
	size_t end = values.size();
	extend(length);
	memcpy(values.data() + end, v, length * sizeof(float));
}
void Mesh::Vertices::push(const Vertices& other)
{
	values.insert(values.end(), other.values.begin(), other.values.end());
}

void Mesh::Vertices::erase(size_t pos)
{
	std::swap_ranges(values.begin() + pos * vertexSize, values.begin() + pos * vertexSize, values.end() - vertexSize);
	pop();
}
void Mesh::Vertices::erase(size_t start, size_t end)
{
	std::swap_ranges(values.begin() + start * vertexSize, values.begin() + end * vertexSize, values.end() - end + start);
	pop(end - start);
}

void Mesh::Vertices::pop()
{
	pop(1);
}
void Mesh::Vertices::pop(size_t amount)
{
	shrink(amount);
}

void Mesh::Vertices::clear()
{
	values.clear();
}

float Mesh::Vertices::operator[](size_t pos)
{
	return values[pos];
}

float * Mesh::Vertices::get()
{
	return values.data();
}

void Mesh::Vertices::shrink(size_t amt)
{
	values.resize(values.size() - (amt * vertexSize));
}

void Mesh::Vertices::extend(size_t amt)
{
	values.resize(values.size() + amt);
}

#include <vulkan\vulkan.h>
#include <vector>
#include <functional>

#include "CoreEngine/IHandle.h"
#include "PipelineFactory.h"
#include "CoreEngine/Event.h"
#include "GraphicsEvents.h"
#include "Subpass.h"
#include "RenderContext.h"

class Device;
class SwapChain;
class SwapChainImage;
class RenderPass;
class PipelineLayout;
class ComponentInterface;
class Surface;

#pragma once
class Attachment : public IRenderComponent
{
public:
	Attachment() {}
	Attachment(VkAttachmentDescription desc, VkClearValue clearVal, VkImageView view, VkExtent3D viewExtent) : attachment(desc), clearValue(clearVal), imageView(view), extent(viewExtent) {}
	
	VkAttachmentDescription& getAttachmentHandle() { return attachment; }
	VkClearValue& getClearValue() { return clearValue; }

	const VkExtent3D& getExtent() const { return extent; }
	VkImageView getImageView() const { return imageView; }

	virtual void* getComponentData() { return this; }
private:
	VkAttachmentDescription attachment;
	VkClearValue clearValue;
	VkImageView imageView;
	VkExtent3D extent;
};

class FramebufferAttachmentProvider
{
public:
	virtual Attachment getFramebufferAttachment(uint32_t framebufferCnt) const = 0;
};

struct Framebuffer {
	VkFramebuffer framebuffer;
	VkExtent2D extent;
};

struct FramebufferAttachmentProviderAttachment {
	FramebufferAttachmentProvider* provider;
	bool shouldClear = true;
};

#pragma once
class RenderPass :
	public EventHandler,
	public IRenderSystem,
	public IHandle<VkRenderPass>
{
public:
	RenderPass(ComponentInterface*, bool = true);
	
	virtual void construct(Surface*);

	const VkRenderPass& getHandle() const;
	PipelineFactory& getFactory();

	void operator()(ComponentInterface*, const EventBase* evt);

	void addProvider(FramebufferAttachmentProviderAttachment& attachment) { providers.push_back(attachment); }

	void registerDependency(Dependency& src, Dependency& dst);

	Framebuffer getFramebuffer(uint32_t id);
	inline const size_t getCurrentFramebufferID() const { return framebuffers.getIdx(); }

	void recordSubpasses();
	void render(CommandBufferRecord&, uint32_t);

	void destroy();

	~RenderPass();
protected:
	virtual VkSubpassContents getRenderMethod() { return VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS; }

private:
	ComponentInterface* interface;

	VkRenderPass pass;

	std::atomic<uint32_t> idCtr;
	std::vector<VkSubpassDependency> dependencies;
	ISwappable<Framebuffer> framebuffers;
	std::vector<FramebufferAttachmentProviderAttachment> providers;

	PipelineFactory pipelineProducer;
};


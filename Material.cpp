#include "Material.h"

#include "Device.h"
#include "CommandBuffer.h"
#include "Pipeline.h"
#include "GraphicsEngine.h"


Material::Material(ComponentInterface* core, float exponent, float intensity) :
	IObjectComponent(core),
	data({ exponent, intensity })
{

}

void Material::operator()(ComponentInterface* core, const EventBase* handle)
{
	if (handle->is<DescriptorRegistrationEvent>())
	{
		const DescriptorRegistrationEvent* evt = (const DescriptorRegistrationEvent*)handle;
		evt->getDescriptorFactory()->registerBinding(core, 3, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 0, sizeof(SpecularData));
		set = evt->getDescriptorFactory()->getDescriptorSet(core, 3);
	}
	else if(handle->is<DescriptorAllocationEvent>())
	{
		descriptorID = set->allocate(sizeof(SpecularData));
		set->set(descriptorID, &data);
	}
	else if(handle->is<RecordEvent>())
	{
		const RecordEvent* evt = (const RecordEvent*)handle;
		
		set->bind((CommandBufferRecord*)evt->getRenderContext()->getContext(), descriptorID);
	}
}

inline void Material::listRequirements(std::set<uint64_t>& req)
{
	req.insert(DescriptorRegistrationEvent::getClassID());
	req.insert(DescriptorAllocationEvent::getClassID());
	req.insert(RecordEvent::getClassID());
}


#include "Image.h"

#include "Device.h"


Image::Image()
{
}

Image::Image(VkImage& img, uint32_t width, uint32_t height, VkFormat format)
{
	ownsImage = false;
	tex = img; //is this not viable?

	setWidth(width);
	setHeight(height);
	setFormat(format);
	texMem = VK_NULL_HANDLE;
}

void Image::construct(Device &dev, uint32_t width, uint32_t height, VkFormat format)
{
	setWidth(width);
	setHeight(height);
	setFormat(format);

	ownsImage = tex == VK_NULL_HANDLE;
	if (ownsImage)
		createImage(dev);
	if (ownsImage && tex != VK_NULL_HANDLE && texMem == VK_NULL_HANDLE)
		allocateMemory(dev);

	if (ownsImage)
		vkBindImageMemory(dev.getHandle(), tex, texMem, 0); //potentially allocate 64 MB chunks and map Images to that
}

void Image::init(Device& dev)
{
	if (tex != VK_NULL_HANDLE)
		createImageView(dev);
}

void Image::destroy(Device& dev)
{
	vkDestroyImageView(dev.getHandle(), view, nullptr);
	if (ownsImage)
	{
		vkDestroyImage(dev.getHandle(), tex, VK_NULL_HANDLE);
		vkFreeMemory(dev.getHandle(), texMem, VK_NULL_HANDLE);
	}
}


Image::~Image()
{
}

void Image::allocateMemory(Device &dev)
{
	VkMemoryRequirements memRequirements;
	vkGetImageMemoryRequirements(dev.getHandle(), tex, &memRequirements);

	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = getMemoryType(dev, memRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	if (vkAllocateMemory(dev.getHandle(), &allocInfo, nullptr, &texMem) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate image memory!");
	}

}

void Image::createImage(Device &dev)
{
	VkImageCreateInfo imageInfo = {};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageInfo.imageType = VK_IMAGE_TYPE_2D;
	imageInfo.extent.width = width;
	imageInfo.extent.height = height;
	imageInfo.extent.depth = 1;
	imageInfo.mipLevels = 1;
	imageInfo.arrayLayers = 1;
	imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	imageInfo.format = format;
	imageInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
	imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;

	if (vkCreateImage(dev.getHandle(), &imageInfo, nullptr, &tex) != VK_SUCCESS) {
		throw std::runtime_error("failed to create image!");
	}
}

void Image::createImageView(Device &dev)
{
	VkImageViewCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	createInfo.image = tex;
	createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	createInfo.format = format;
	createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
	createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
	createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
	createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
	createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	createInfo.subresourceRange.baseMipLevel = 0;
	createInfo.subresourceRange.levelCount = 1;
	createInfo.subresourceRange.baseArrayLayer = 0;
	createInfo.subresourceRange.layerCount = 1;

	if (vkCreateImageView(dev.getHandle(), &createInfo, VK_NULL_HANDLE, &view) != VK_SUCCESS) {
		throw VulkanConstructionError("failed to create image views!");
	}
}

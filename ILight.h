#include "CoreEngine\IObjectComponent.h"
#include "GraphicsModuleAction.h"
#include "RenderContext.h"
#pragma once

class ILight;
class ILightRenderer
{
public:
	virtual void registerLight(ILight*) = 0;
	virtual void unregisterLight(ILight*) = 0;

	virtual void updateData(ILight* id) = 0;

};

struct LightTransform {
	glm::mat4 lightView;
	glm::vec3 lightPos;
};


//this is a LightRenderer proxy. it doesn't do a whole lot on its own
class ILight : public IObjectComponent, public Identifier
{
public:
	constexpr void assign(MemoryRange light, MemoryRange shadow) { lightData = light;  shadowData = shadow; }
	const MemoryRange& getAssignedData() const { return lightData; }
	const MemoryRange& getShadowData() const { return shadowData; }

	virtual void updateData(GraphicsModuleAction::BaseLightData* data) { renderer->updateData(this); }
	virtual void updateData(GraphicsModuleAction::DirectionalLightData* data) { renderer->updateData(this); }
	virtual void updateData(GraphicsModuleAction::PointLightData* data) { renderer->updateData(this); }
	virtual void updateData(GraphicsModuleAction::SpotLightData* data) { renderer->updateData(this); }

	virtual void operator()(ComponentInterface*, const EventBase*) {}
	virtual inline void listRequirements(std::set<uint64_t>& req) {}

protected:
	ILight(ComponentInterface* core, ILightRenderer* renderer) : IObjectComponent(core), Identifier(), renderer(renderer) { renderer->registerLight(this); }
	ILightRenderer* renderer;
	MemoryRange lightData;
	MemoryRange shadowData;
};

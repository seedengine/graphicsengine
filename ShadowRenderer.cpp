#include "ShadowRenderer.h"
#include "GraphicsEvents.h"
#include "CoreEngine/Event.hpp"


ShadowRenderer::ShadowRenderer(ComponentInterface * core) :
	Subpass(VK_PIPELINE_BIND_POINT_GRAPHICS),
	EventHandler(core)
{
}

void ShadowRenderer::construct(DepthImage * depthImage, ILightRenderer * lightProvider)
{
}

void ShadowRenderer::operator()(ComponentInterface* core, const EventBase* handle)
{
	if (handle->is<DescriptorRegistrationEvent>())
	{
		const DescriptorRegistrationEvent* evt = (const DescriptorRegistrationEvent*)handle;
		evt->getDescriptorFactory()->registerBinding(core, 6, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 0, sizeof(glm::mat4));
		evt->getDescriptorFactory()->registerBinding(core, 6, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1, sizeof(glm::mat4));
		evt->getDescriptorFactory()->registerBinding(core, 6, VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1, 0);
		depthMatrixSet = evt->getDescriptorFactory()->getDescriptorSet(core, 6);
		inputAttachment = evt->getDescriptorFactory()->getDescriptorSet(core, 6);
	}
	else if (handle->is<DescriptorAllocationEvent>())
	{
		depthMatrixAllocation = depthMatrixSet->allocate(sizeof(glm::mat4));
		depthMatrixSet->set(depthMatrixAllocation, &depthMat);
	}
	else if (handle->is<RecordEvent>())
	{
		const RecordEvent* evt = (const RecordEvent*)handle;
		if (evt->getRenderContext()->getPipelineID() != -1)
			depthMatrixSet->bind((CommandBufferRecord*)evt->getRenderContext()->getContext(), depthMatrixAllocation);
	}
}

void ShadowRenderer::render(IRenderContext *)
{
}

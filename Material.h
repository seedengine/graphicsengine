#include <stdint.h>

#include "CoreEngine/IObjectComponent.h"

class ComponentInterface;
class Device;
class RenderPass;
class CommandBufferRecord;
class DescriptorSet;

#pragma once
class Material : public IObjectComponent
{
	struct SpecularData
	{
		float specularExponent;
		float specularIntensity;
	};
public:
	Material(ComponentInterface* core, float exponent, float intensity);

	virtual void operator()(ComponentInterface*, const EventBase* handle);

	virtual inline void listRequirements(std::set<uint64_t>& req);
private:
	SpecularData data;
	DescriptorSet* set;
	MemoryRange descriptorID;
};
#include <atomic>
#include "CoreEngine\IHandle.h"
#include "RenderContext.h"

#include <map>
#include <vulkan\vulkan.h>

#ifndef CMD_BUF

class Device;
class Pipeline;
class CommandBuffer;

	enum BufferState
	{
		INVALID,
		READY,
		RECORDING
	};

class CommandBufferRecord
{
	friend class CommandBuffer;
public:
	CommandBufferRecord();

	VkCommandBuffer getCommandBuffer() const;
	Pipeline* getCurrentPipeline();
	void setCurrentPipeline(Pipeline*);

	void bindSet(uint8_t set, uint32_t binding) { boundDescriptors.insert(std::pair<uint8_t, uint32_t>(set, binding)); }
	bool isSetBound(uint8_t id) { return boundDescriptors.count(id) == 1; }
	void resetSets() { boundDescriptors.clear(); }

	~CommandBufferRecord();
private:
	CommandBufferRecord(CommandBuffer* buf, VkCommandBufferUsageFlags flags, const VkFramebuffer& frame, const VkRenderPass& pass, const uint32_t subpass);

	CommandBuffer* buf;
	Pipeline* pipeline;
	std::map<uint8_t, uint32_t> boundDescriptors;
};

class CommandBuffer : public IHandle<VkCommandBuffer>
{
	friend class CommandBufferRecord;
public:
	CommandBuffer();

	explicit CommandBuffer(const CommandBuffer& other) :
		primary(other.primary),
		bufferState(INVALID),
		recorded(false)
	{

	}

	void construct(Device&, bool, uint32_t);

	const VkCommandBuffer& getHandle() const
	{
		return cmdBuf;
	}

	void record(CommandBufferRecord* buf, VkCommandBufferUsageFlags flags, const VkFramebuffer& frame, const VkRenderPass& pass, const uint32_t subpass)
	{
		if (bufferState != READY)
			throw FrameworkException("Invalid State transfer of Command Buffer to RECORDING.");
		bufferState = RECORDING;
		recorded = true;
		new(buf) CommandBufferRecord( this, flags, frame, pass, subpass);
	}

	void destroy(Device&);

private:
	bool primary;

	bool recorded;

	std::atomic<BufferState> bufferState;
	VkCommandBuffer cmdBuf;
};



#pragma once
class RenderContext : public IRenderContext
{
public:
	RenderContext(CommandBufferRecord* record, uint32_t subpass, uint32_t pipeline) : record(record), subpassID(subpass), pipelineID(pipeline) {}

	virtual void* getContext()
	{
		return record;
	}
	virtual void* getExtendedData()
	{
		return nullptr;
	}
	virtual const uint32_t getSubpassID() const
	{
		return subpassID;
	}
	virtual const uint32_t getPipelineID() const
	{
		return pipelineID;
	}
private:
	CommandBufferRecord* record;
	uint32_t subpassID;
	uint32_t pipelineID;
};

#define CMD_BUF
#endif // !CMD_BUF

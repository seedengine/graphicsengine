#include "PipelineLayout.h"
#include "GraphicsExceptions.h"
#include "Device.h"
#include "PhysicalDevice.h"
#include "CoreEngine\IEngine.h"
#include <algorithm>


PipelineLayout::PipelineLayout()
{

}

void PipelineLayout::construct(ComponentInterface* core, Device& dev, std::set<uint32_t> sets, std::vector<VkPushConstantRange> pConstants)
{
	pushConstantLimit = dev.getPhysicalDevice()->getDeviceLimits().maxPushConstantsSize;
	this->sets = sets;

	//validate Push Constants
	if (pConstants.size() > 1 || (!pConstants.empty() && pConstants[0].size > 0))
		pushConstants = pConstants;


	//there has to be a more effective way

	std::vector<VkDescriptorSetLayout> tmpLayouts;
	for (uint32_t i : sets)
		tmpLayouts.push_back(*dev.getDescriptorFactory().getDescriptorSet(core, i));

	VkPipelineLayoutCreateInfo info{};
	info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	info.pNext = nullptr;
	info.flags = 0;
	info.pushConstantRangeCount = static_cast<uint32_t>(pushConstants.size());
	info.pPushConstantRanges = pushConstants.data();
	info.setLayoutCount = static_cast<uint32_t>(tmpLayouts.size());
	info.pSetLayouts = tmpLayouts.data();

	if (vkCreatePipelineLayout(dev.getHandle(), &info, VK_NULL_HANDLE, &layout) != VK_SUCCESS)
		throw VulkanConstructionError("failed to create pipeline layout!");
}

const VkPipelineLayout & PipelineLayout::getHandle() const
{
	return layout;
}

bool PipelineLayout::hasSet(uint32_t id)
{
	return sets.find(id) != sets.end();
}

void PipelineLayout::destroy(Device& dev)
{
	vkDestroyPipelineLayout(dev.getHandle(), layout, VK_NULL_HANDLE);
}

PipelineLayout::~PipelineLayout()
{
}
